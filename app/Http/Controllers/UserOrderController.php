<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Product;
use App\User;
use App\Debt;

class UserOrderController extends Controller
{
    private $_order, $_product, $_user, $_debt;

	public function __construct()
	{
		$this->middleware(['auth', 'wizard']);

        $this->middleware('seller')->except('showOrderToCustomer', 'cancelOrder', 'delivery_confirmation_from_customer', 'indexOrdersToAdmin', 'innerIndexOrdersToAdmin', 'showAdmin');

        $this->_order = new Order();

        $this->_product = new Product();

        $this->_user = new User();

        $this->_debt = new Debt();
	}

    public function index()
    {    	

        $current_user = auth()->user()->id;

        $orders = [];

        $users_products = $this->_product::where('user_id', $current_user)->get();

        foreach ($users_products as $products) {
            
            array_push($orders, $products->order()->orderBy('created_at', 'desc')->get());

        }

		return view('post-login.pages.product.order.index', compact('orders'));
    }

    public function indexOrdersToAdmin()
    {
        $vendors = $this->_user::where('role', 2)->get();

        return view('post-login.pages.product.admin-order.index', compact('vendors'));
    }

    public function innerIndexOrdersToAdmin($id)
    {
        $user_id = $this->_user::find($id)->id;

        $orders = $this->_order::with(['product'])
                     ->whereHas('product', function($query) use($user_id) {                    
                        $query->where('user_id', $user_id);
                    })->get();

        return view('post-login.pages.product.admin-order.inner-index', compact('orders', 'id'));
    }

    public function show($id)
    {
        $order = $this->_order::with(['product'])->find($id);

        $current_user = auth()->user();

        if(!is_null($order))
        {
            $customer = $order->user()->first();

            if($order->product()->first()->user_id == $current_user->id)
            {
        	   return view('post-login.pages.product.order.show', compact('order', 'customer'));
            }
        }

        return back();

    }

     public function showAdmin($vendor_id, $id)
    {
        $order = $this->_order::with(['product'])->find($id);

        $current_user = auth()->user();

        if(!is_null($order))
        {
            $customer = $order->user()->first();

            return view('post-login.pages.product.order.show', compact('order', 'customer'));
            
        }

        return back();

    }

    public function showOrderToCustomer(Request $request)
    {

        $orders = $this->_order::with(['user', 'product', 'productSize'])->where('user_id', auth()->user()->id)->latest()->get();

        if ($request->wantsJson()) {
            return response()->json(['orders', $orders], 200);
        }

        return view('pre-login.pages.my.orders', compact('orders'));
    }

    public function cancelOrder($order_id, Request $request)
    {
        $this->_order->destroyOrder($order_id);

        if($request->wantsJson())
        {
            return response()->json(['data' => 'Order is successfully canceled!'], 200);
        }

        return back()->with('success', 'Order is successfully canceled!');
    }

    public function vendor_delivery_confirmation(Request $request)
    {

        if($this->_user->isConnected() == false)
        {
            $status = $request->delivery_state;

            $id = $request->id;

            $this->_order->vendorDeliveryConfirmation($id, $status);

            $this->_order->updatePaymentStatus(1, $id);

            return response()->json(['status' => 1, 'message' => 'Delivery status is confirmed']);

        }
        else
        {
            return response()->json(['status' => 0, 'message' => 'Internet connection is required']);
        }

    }

    public function delivery_confirmation_from_customer($id, Request $request)
    {
        if($this->_user->isConnected() == false)
        {
            $this->validate(request(), [

                'delivery_status' => 'required'

            ]);

            $status = $request->delivery_status;

            $this->_order->customerDeliveryConfirmation($id, $status);

            if($request->wantsJson())
            {
                return response()->json(['status'=>'success', 'message'=>'Delivery status is confirmed'], 200);
            }

            return back()->with('success', 'Delivery status is confirmed');

        }
        else
        {

            if($request->wantsJson())
            {
                return response()->json(['status'=>'error', 'message'=>'Internet connection is required!'], 200);
            }

            return back()->withErrors('Internet connection is required!');
        }
    }
}
