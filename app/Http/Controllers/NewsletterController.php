<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Newsletter;

use App\NewsletterTheLetter;

class NewsletterController extends Controller
{
	private $_newsletter, $_newsletterTheLetter;

	public function __construct()
	{
		$this->middleware(['auth', 'admin'])->except('store');

		$this->_newsletter = new Newsletter();

		$this->_newsletterTheLetter = new NewsletterTheLetter();
	}

    public function store(Request $request)
    {
    	$this->validate(request(), [

    		'email' => 'required|unique:newsletters'

    	]);

    	$email = $request->email;

		$this->_newsletter->addUserForNewsLetter($email);

		return response()->json(['message'=>'you have successfully Subscribed for our newletter! Thank you!']);
    }

    public function create()
    {
    	$newsletters = $this->_newsletterTheLetter::latest()->get();

    	return view('post-login.pages.newsletter.index', compact('newsletters'));
    }

    public function storeLetter(Request $request)
    {        
    	$this->validate(request(), [

    		'news_title' => 'required',

    		'slug' => 'required'

    	]);


    	$this->_newsletterTheLetter->createTheLetter(request('news_title'), request('slug'));

    	return back()->with('success', 'Newsletter is successfully created!');

    }

    public function updateLetter($id)
    {
    	$this->validate(request(), [

    		'news_title' => 'required',

    		'slug' => 'required'

    	]);

    	$this->_newsletterTheLetter->updateTheLetter(request('news_title'), request('slug'), $id);

    	return back()->with('success', 'Newsletter us successfully updated!');

    }

    public function destroy($id)
    {
    	$this->_newsletterTheLetter->destroyTheLetter($id);

    	return back()->with('success', 'Newsletter is successfully deleted!');
    }

    public function sendNewsLetter($id)
    {
    	/*
    	|--------------------------------------------------------------------------------------------------
    	| App\NewsLetter or $this->_newsletter represents the subscribers or subscription data if i may say
    	|--------------------------------------------------------------------------------------------------
    	*/

        $users = $this->_newsletter::all();

        $this->_newsletterTheLetter->sendMail($users, $id);

        return redirect()->back()->with('success', 'News-Letter is Successfully Sent'); 
    }
}
