<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Address;

class AddressManagementController extends Controller
{
    private $_address;

	public function __construct()
	{
		$this->middleware('auth');
        $this->_address = new Address();
	}

	public function index()
	{
		$locations = auth()->user()->addresses()->get();

		
		if(auth()->user()->role == 2)
		{
			return view('post-login.pages.user.my-address', compact('locations'));
		}
		elseif(auth()->user()->role == 3)
		{
			return view('pre-login.pages.my.my-address', compact('locations'));
		}
	}

    public function store(Request $request)
    {
        $address = $request->address;
        $latitude = $request->lat;
        $longitude = $request->lng;

        $this->validate(request(), [
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ]);

        $this->_address->addAddress($address, null, $latitude, $longitude,  auth()->user()->id);

        return back()->with('success', 'Address is successfully saved');

    }

    public function update($id, Request $request)
    {

        $address = $request->address;
        $latitude = $request->lat;
        $longitude = $request->lng;

        $this->validate(request(), [
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ]);

        $this->_address->updateAddress($address, $latitude, $longitude, $id);

        return back()->with('success', 'Address is successfully updated!');

    }

    public function destroy($id)
    {
        $this->_address->destroyAddress($id);

        return back()->with('success', 'Address is successfully destroyed!');
    }
}
