<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Order;
use App\Product;
use App\Debt;
use App\CompanySettings;
use App\User;
use App\Address;
use Cartalyst\Stripe\Exception\CardErrorException;
use App\Mail\OrderMail;

class PaymentController extends Controller
{
    private $_order, $_product, $_debt, $_setting, $_user;

    public function __construct()
    {
        $this->middleware('auth');
        $this->_order = new Order();
        $this->_product = new Product();
        $this->_debt = new Debt();
        $this->_setting = new CompanySettings();
        $this->_user = new User();
        $this->_address = new Address();
    }

    public function pay(Request $request)
    {

        /*
        |---------------------
        |  -- Validation -- 
        |---------------
        */

        $this->validate(request(), [

            'billing_first_name' => 'required',
            'billing_last_name' =>'required',
            'billing_country' => 'required',
            'billing_address' => 'required',
            'billing_state' => 'required',
            'billing_city' => 'required',
            'billing_postcode' => 'required',
            'billing_phone' => 'required',
            'stripeToken' => 'required',
            'map_address' => 'required'

        ]);

        /*
        |------------------------------------------------------
        |  -- Assign variables to save new comers to user -- 
        |------------------------------------------------
        */

        $firstname = $request->billing_first_name;
        $lastname = $request->billing_last_name;
        $country = $request->billing_country;
        $address = $request->billing_address;
        $state = $request->billing_state;
        $city = $request->billing_city;
        $phone = $request->billing_phone;
        $company_name = $request->billing_company;
        $apartment = $request->billing_address_op;
        $postalcode = $request->billing_postcode;

        $map_address = $request->map_address;
        $lat = $request->lat;
        $lng = $request->lng;

        $buyerUserId = auth()->user()->id;

        /*
        |--------------------------------------------
        |  -- Save user Information and Address -- 
        |--------------------------------------
        */

        if(is_null(auth()->user()->country) && is_null(auth()->user()->address) && is_null(auth()->user()->state) && is_null(auth()->user()->city) && is_null(auth()->user()->phone_number) && is_null(auth()->user()->postcode))
        {
            $this->_user->saveUserInformationAfterBilling($firstname, $lastname, $country, $address, $state, $city, $phone, $company_name, $apartment, $postalcode);

        }

        if(is_null(auth()->user()->addresses()->first()))
        {
            $this->_address->addAddress($map_address, null, $lat, $lng,  $buyerUserId);
        }

        /*
        |--------------------------------
        |  -- update Address -- 
        |--------------------------
        */

        if(!is_null(auth()->user()->addresses()->first()))
        {
            if(auth()->user()->addresses()->first()->latitude != $lat || auth()->user()->addresses()->first()->longitude != $lng || auth()->user()->addresses()->first()->address != $map_address)
            {
                $this->_address->updateAddress($map_address, $lat, $lng, auth()->user()->addresses()->first()->id);
            }
        }

        /*
        |----------------------------------------------------------
        |  -- Assign Main Variables That are passed to stripe -- 
        |----------------------------------------------------
        */

        $totalPrice = $this->_product->cartItemsPrice();

        $contents = Cart::content()->map(function($item) {
            return 'Item: '.$item->model->product_name.' | Quantity of Item: '.$item->qty;
        })->values()->toJson();

        /*
        |----------------------------------
        |  -- Execute stripe payment -- 
        |--------------------------
        */

        try {

            $charge = \Stripe::charges()->create([

                'amount' => $totalPrice,
                'currency' => 'USD',
                'source' => $request->stripeToken,
                'description' => 'Order',
                'receipt_email' => auth()->user()->email,
                'metadata' => [
                    'contents' => $contents,
                    'quantity' => Cart::instance('default')->count(),
                ]

            ]);

            $shippingTo = [
                'country' => $country,
                'address' => $address,
                'state' => $state,
                'city' => $city,
                'apartment' => $apartment,
                'postal_code' => $postalcode,
                'map_address' => $map_address
            ];

            $mainOrder = [
                'email' => auth()->user()->email,
                'amount' => $totalPrice,
                'shipping' => $this->_product->cartItemFlatRate(),
                'currency' => 'USD',
                'name' => $firstname.' '.$lastname,
                'contents' => Cart::content(),
                'subtotal' => Cart::subtotal(),
                'date' => date('D, M d Y', strtotime(now())).' at '.date('h:i a', strtotime(now())),
                'shippingTo' => $shippingTo
            ];

            \Mail::send(new OrderMail($mainOrder, $this->_setting::first()));

            $items = [];

            foreach(Cart::content() as $item)
            {
                array_push($items,$item);                
            }

            for ($i=0; $i < Cart::content()->count(); $i++) { 

                /*
                |---------------------------------------------------------
                |  -- Assign Variables for Order &  available items -- 
                |---------------------------------------------------
                */
                $charge = $this->_setting::first()->charge;

                $productId = $items[$i]->model->id;
                $orderCode = $items[$i]->rowId.strtotime("now");
                $color = $items[$i]->options->color;
                $qty = $items[$i]->qty;
                $price = $items[$i]->model->current_price;
                $vendorUserId = $items[$i]->model->user()->first()->id;
                $sizeId = $items[$i]->options->size;
                $changePrice = $price * $qty;
                $habesha_lebz_charge_per_item_in_price = ($charge / 100) * $changePrice;  //The money they will take from the deal!
                $habesha_lebz_debt =  $changePrice - $habesha_lebz_charge_per_item_in_price; // The money they will give away to vendors
                $paymentStatus = 1;  // PaymentStatus == 1 ? status == 'Paid' : status == 'transaction failed'
                $adminUserId = $this->_user->getAdmin()->id;
                $paid_amount = $this->_product->cartItemFlatRate() + $price; //The amount the customer paid | current_item + total flat rate
                $shipping = $this->_product->cartItemFlatRate();
                $personal_statement = $request->personal_statement;

                $this->_order->createOrder($buyerUserId, $productId, $orderCode, $paymentStatus, $color, $qty, $vendorUserId, $habesha_lebz_debt, $adminUserId, $paid_amount, $shipping, $sizeId, $personal_statement[$i]);
                
                /*
                |----------------------------------------------------------------
                |  -- Create Order | Update the available items after order -- 
                |----------------------------------------------------------
                */

                
                $this->_product->updateAvailableItems($productId, $qty);

            }

            /*
            |-----------------------------------------------------------
            |  -- Destory The Cart | Redirect to a thank you page -- 
            |---------------------------------------------------
            */            

            Cart::instance('default')->destroy();
            return redirect()->route('thankyou.index')->with('success', 'successfully payed!');

        } 
        /*
        |---------------------------
        | -- Catch Stripe Erros --
        |----------------------
        */
        catch(CardErrorException $e) {
            return back()->withErrors('Error! '.$e->getMessage());
        }

        /*
        |--------------------------------------------
        */


    }



}
