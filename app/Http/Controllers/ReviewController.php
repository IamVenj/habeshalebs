<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;

use App\User;

use App\Product;

class ReviewController extends Controller
{
	private $_review, $_user, $_product;

    public function __construct()
    {
    	$this->middleware('auth');

    	$this->middleware('seller')->except(['store', 'update', 'destroy', 'showVendors', 'showVendorReviews']);

        $this->middleware('admin')->except(['store', 'update', 'destroy', 'myReview']);

    	$this->_review = new Review();

        $this->_user = new User();

        $this->_product = new Product();
    }

    public function index()
    {

    }

    public function store(Request $request)
    {
    	$this->validate(request(), [

    		'review' => 'required',

    		'rating' => 'required'

    	]);

    	$user = auth()->user()->id;

    	$check_existance = $this->_review::where('user_id', $user)->where('product_id', request('product_id'))->count();

    	if($check_existance > 0)
    	{
            if($request->wantsJson())
            {
                return response()->json(['status'=>'error', 'message'=>'You have already reviewed this product!'], 200);
            }

    		return back()->withErrors('You have already reviewed on this product!');
    	}
    	else
    	{
	    	$this->_review->postReview(request('review'), request('rating'), request('product_id'), $user);

            if($request->wantsJson())
            {
                return response()->json(['status'=>'success', 'message'=>'Successfully posted!'], 200);
            }

	    	return back()->with('success', 'Successfully posted!');
    	}

    }

    public function update($id, Request $request)
    {

    	$this->validate(request(), [

    		'review' => 'required',

    		'rating' => 'required'

    	]);    	

    	$rating = request('rating');

    	$review = request('review');


    	$this->_review->_updateReview($rating, $review, $id);

        if($request->wantsJson())
        {
            return response()->json(['message' => 'You have successfully updated your review'], 200);
        }

    	return back()->with('success', 'You have successfully updated your review');

    }

    public function destroy($id, Request $request)
    {
        $this->_review->destroyReview($id);

        if($request->wantsJson())
        {
            return response()->json(['message' => 'Review is successfully deleted!'], 200);
        }

        return back()->with('success', 'Review is successfully deleted!');
    }


    public function myReview($id)
    {
        $reviews = $this->_review::where('product_id', $id)->get();

        return view('post-login.pages.reviews.my-reviews', compact('reviews'));
    }

    public function showVendors()
    {
        $vendors = $this->_user::where('role', 2)->get();

        return view('post-login.pages.reviews.vendor-reviews', compact('vendors'));
    }

    public function showVendorReviews($shop_name, $id)
    {
        $vendor = $this->_user::find($id);

        $products = $vendor->products()->get();

        $reviews = [];

        foreach ($products as $product) 
        {
            array_push($reviews,  $this->_review::where('product_id', $product->id)->latest()->get());
        }

        return view('post-login.pages.reviews.show-vendor-reviews', compact('reviews', 'vendor'));
    }
}
