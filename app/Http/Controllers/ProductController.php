<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Colors;

use App\User;

use App\ProductColor;

use DB;

use Cloudder;

use App\Review;

class ProductController extends Controller
{

	private $_product, $_color, $_user, $_review;

	public function __construct()
	{
		$this->_product = new Product();

		$this->_color = new Colors();

		$this->_user = new User();

        $this->_review = new Review();
	}

    public function show($product_name, $product_id, Request $request)
    {
        $product = $this->_product::with(['review', 'review.user', 'productImage', 'productSize', 'productColors', 'productColors.colors', 'user'])
            ->find($product_id);
        
        $category_id = $product->category()->first()->id;
        
        $total_review_count = $this->_review::where('product_id', $product_id)
            ->count();
        
        $locations = $product->user()->first()->addresses()->get();
        
        $related_products = $this->_product::where('category_id', $category_id)
        ->where('id', '!=', $product_id)
        ->orderByRaw('product_name', '%'.strtolower($product_name).'%')
        ->inRandomOrder()
        ->take(6)
        ->get();

        
        $this->_product->visits($product_id);
        

        if(!is_null(auth()->user()))
        {
            $reviews = $this->_review::where('product_id', $product_id)
            ->where('user_id', '!=', auth()->user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(3);
            
            $current_user_review = $this->_review::where('product_id', $product_id)
            ->where('user_id', auth()->user()->id)
            ->first();
        }
        else
        {
            $reviews = $this->_review::where('product_id', $product_id)->orderBy('created_at', 'desc')->paginate(3);

            $current_user_review = null;            
        }

        if($request->wantsJson())
        {
            return response()->json(['product'=>$product], 200);
        }


    	return view('pre-login.pages.product-page', compact('product', 'product_id', 'reviews', 'current_user_review', 'related_products', 'total_review_count', 'locations'));
        
    }

    public function showBasedOnCategory(Request $request, $category_name, $category_id)
    {
        $products =  $this->_product::where('category_id', $category_id)->orderBy('created_at', 'desc')->get();

        $all_colors = $this->_color::all();

        $vendors = $this->_user::where('role', 2)->whereNotNull('shop_name')->get();

        return view('pre-login.pages.products', compact('products', 'all_colors', 'vendors', 'category_name', 'category_id'));
    }

    public function productsBasedOnCategory(Request $request)
    {


        $minPrice = explode(" Birr", $request->min_price)[0];

        $maxPrice = explode(" Birr", $request->max_price)[0];
        // productsize
        $multiple_size_filter = $request->multiple_size_filter;
        //productColor  
        $multiple_colors_filters = $request->multiple_colors_filter;
        // user
        $vendor_filter = $request->vendor_filter;

        $sort_by = $request->sort_by;

        $design = $request->design;


        /*
        |-------------------------------------------------------------------------------
        | Variables for pagination
        |-------------------------------------------------------------------------------
        */

         $records_per_page = $request->records_per_page;

         $page = '';

         

         if($request->page != '')
         {
            $page = $request->page;
         }
         else
         {
            $page = 1;
         }


        /*
        |--------------------------------------------------------------------------------
        */


    
        if($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            
            
            if($sort_by == '0')
            {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }
            elseif($sort_by == '1')
            {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }

            elseif($sort_by == '2')
            {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
                      
            
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            
            if($sort_by == '0')
            {

                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '1')
            {

                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
                        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '2')
            {

                $products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0')
            {

                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }


            elseif($sort_by == '1')
            {

                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();



                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif($sort_by == '2')
            {

                $products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['productSize', 'user'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            if($sort_by == '0')
            {
                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }
            elseif($sort_by == '1')
            {

                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->forPage($page, $records_per_page)

                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '2')
            {

                $products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'user'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
                        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        elseif($design == null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            if($sort_by == '0')
            {
                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }
            elseif($sort_by == '1')
            {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->forPage($page, $records_per_page)

                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '2')
            {

                $products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors', 'productSize', 'user'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
                        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0'){

                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {
                
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '2') {
                
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        elseif($design == null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0'){

                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {
                
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '2') {
                
                $products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['user'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        elseif($design == null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            if($sort_by == '0')
            {
                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }
            elseif ($sort_by == '1') {

                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            if($sort_by == '0')
            {
                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();
            }
            elseif ($sort_by == '1') {

                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productColors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design == null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
    
            }

            elseif ($sort_by == '2') {

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
    
            }

            elseif ($sort_by == '2') {

                $products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();


                $unlimited_products =  $this->_product::with(['productSize'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($design != null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)
        
                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->forPage($page, $records_per_page)

                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)

                        ->where('current_price', '<=', $maxPrice)
        
                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                 $products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)

                        ->where('current_price', '<=', $maxPrice)
        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)

                        ->where('current_price', '<=', $maxPrice)
        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                 $products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)

                        ->where('current_price', '<=', $maxPrice)
        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->whereIn('styling', $design)

                        ->where('current_price', '<=', $maxPrice)
        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }

        }
        elseif($design == null && $minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $products =  $this->_product::where('current_price', '>=', $minPrice)

        
                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->forPage($page, $records_per_page)

                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

        
                        ->where('category_id', $request->category_id)

                        ->orderBy('created_at', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                 $products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                 $products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
                        
                        ->get();

                $unlimited_products =  $this->_product::where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

        
                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }

        }
        else
        {
            if($sort_by == '0'){

                $products =  $this->_product::where('category_id', $request->category_id)

                                        
                        ->orderBy('created_at', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('category_id', $request->category_id)

                                        
                        ->orderBy('created_at', 'desc')
        
                        ->get();



            }
            elseif ($sort_by == '1') {

                $products =  $this->_product::where('category_id', $request->category_id)

                                        
                        ->orderBy('current_price', 'desc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('category_id', $request->category_id)

                                        
                        ->orderBy('current_price', 'desc')
        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $products =  $this->_product::where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'asc')

                        ->forPage($page, $records_per_page)
        
                        ->get();

                $unlimited_products =  $this->_product::where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'asc')
        
                        ->get();
                
            }

        }

        $pagination_output = '';

        $total_pages = ceil($unlimited_products->count()/$records_per_page);

        $pagination_output .= '<div class="pagination-wraper">
                    <div class="pagination">
                        <ul class="pagination-numbers">';

        for ($i=1; $i <= $total_pages; $i++) { 
            
            $pagination_output .= '
            <li>
                <a href="#" class="pagination_link page-number" id="'.$i.'">'.$i.'</a>
            </li>';

        }
        $pagination_output .= '</ul>
                    </div>
                </div>';



        $output = "";


        if(count($products) == 0)
        {
            $output .= '<section class="">
                        <div class="home-about-blocks">
                            <div class="col-12 about-blocks-wrap2">
                                <div class="row">
                                    <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                        <div class="about-box-inner2">
                                            <h4 class="mb-25">Sorry!</h4><span class="fa fa-shopping-bag text-center" style="font-size: 50px;"></span>  
                                            <h5 class="mb-20 mt-25">We couldn\'t find what you were looking for</h5>
                                            <p>Keep calm and search again</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
               ';
        }


        foreach ($products as $product) 
        {

            $output .= '<div class="product-item-element col-sm-6 col-md-6 col-lg-4">
                    <div class="product-item">
                        <div class="product-item-inner">
                            <div class="product-img-wrap">
                                <img src="'.Cloudder::show($product->productImage()->first()->image_public_id, array("quality" => "auto", "height" => 800, "width"=>800)).'" alt="'.$product->productImage()->first()->image_public_id.'" style="height: 300px; width: 300px; object-fit:cover;">
                            </div>
                            <div class="product-button">';
                                if(is_null(auth()->user()))
                                {
                                $output .= '<a href="/login" class="js_tooltip" data-mode="top" data-tip="Add To Whishlist"><i class="fa fa-heart"></i></a>';
                                }
                                else
                                {
                                $output .= '<a class="js_tooltip" onclick="add_wish('.$product->id.')" data-mode="top" data-tip="Add To Wishlist"><i class="fa fa-heart"></i></a>';
                                }
                                $output .= '<a href="/'.$product->product_name.'/detail/'.$product->id.'" class="js_tooltip" data-mode="top" data-tip="Quick&nbsp;View"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <a class="tag" href="/'.$product->category()->first()->category_name.'/products/'.$product->category()->first()->id.'">'.$product->category()->first()->category_name.'</a>
                            <p class="product-title"><a href="/'.$product->product_name.'/detail/'.$product->id.'">'.$product->product_name.'</a></p>
                            <div class="product-rating">
                                <p>Sold by:</p>
                                <p class="product-rating-count"><span style="font-weight: bold;">'.$product->user()->first()->shop_name.'</span></a>
                            </div>
                            <p class="product-description">
                                '.$product->slug.'
                            </p>
                            <h5 class="item-price">
                            <del>';
                            if(!is_null($product->old_price))
                            {

                                $output .=  '$'.$product->old_price;

                            }
                            $output .= '</del>$'.$product->current_price.'</h5>
                        </div>
                    </div>
                </div>';

        }

        if($request->wantsJson())
        {
            return response()->json(['products'=>$products], 200);
        }
        else
        {
            return response()->json(['data'=>$output, 'pagination'=>$pagination_output]);
        }

    }

    
}
