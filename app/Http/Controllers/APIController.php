<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\SpecialOffers;

use App\HomeCarousel;

use App\Product;

use App\Category;

class APIController extends Controller
{

	private $_user, $_token, $_offer, $_carousel, $_product, $_category;

    /*
    |---------------------------------------------------------------------
    | Constructor for ApiController Class
    |---------------------------------------------------------------------
    */
    
    public function __construct()
    {
        $this->_user = new User();

        $this->_token = null;

        $this->_offer = new SpecialOffers();

        $this->_carousel = new HomeCarousel();

        $this->_product = new Product();

        $this->_category = new Category();
    }


    /*
    |---------------------------------------------------------------------
    | A user registers & gets his/her access token
    |---------------------------------------------------------------------
    */
    
    public function register(Request $request)
    {
        $this::validate(request(),[

            'full_name' => 'required',

            'email' => 'required|email|unique:users',

            'phone_number' => 'required',

            'gender' => 'required',

            'password' => 'required|confirmed|min:4',

        ]);

        $name = request('full_name');

        $password = bcrypt(request('password'));

        $email = request('email');

        $mobile_number = request('phone_number');

        $gender = request('gender');
        
        $role = 2;
 
        $created_user = User::create([

            'name' => $name,

            'email' => $email,

            'phone_number' => $mobile_number,

            'password' => $password,

            'role_id' => $role,

            'gender' => $gender

        ]);

        $token = $created_user->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;
 
        return response()->json(["token" => $token], 200);
        
    }

    /*
    |---------------------------------------------------------------------
    | A user logs in & gets his/her access token
    |---------------------------------------------------------------------
    */

    public function login(Request $request)
    {
    	$credentials = ['email' => $request->email, "password" => $request->password];

    	if(auth()->attempt($credentials))
    	{
            $this->_token = auth()->user()->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;

            $request->headers->set('authorization', 'Bearer ' . $this->_token);

        	return response()->json(["token" => $this->_token], 200);
    	}
    	else
    	{
    		return response()->json(["error" => "unauthorized"], 401);
    	}
    }

    /*
    |--------------------------------------------------------------------------
    | Latest Products
    |--------------------------------------------------------------------------|
    */

    public function latestProduct()
    {
        $latestP = $this->_product::latest()->take(10)->get();

        return response()->json(['products' => $latestP]);
    }

    /*
    |--------------------------------------------------------------------------
    | Best Sold Item
    |--------------------------------------------------------------------------|
    */

    public function bestSold()
    {
        $best_products = $this->_product::where('orders', '!=', 0)->orderBy('orders', 'desc')->get();

        return response()->json(['products'=>$best_products], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Special Offers
    |--------------------------------------------------------------------------|
    */

    public function specialOffers()
    {
        $offers = $this->_offer->getLatestOffers();

        return response()->json(['offers'=>$offers], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Home Carousel
    |--------------------------------------------------------------------------|
    */

    public function homeCarousel()
    {
        $carousel = $this->_carousel::latest()->with('category')->get();

        return response()->json(['carousel'=>$carousel], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Categories
    |--------------------------------------------------------------------------|
    */


    public function getCategory()
    {
        $categories = $this->_category::with(['category'])->doesntHave('category')->whereNotNull('parent_id')->get();

        return response()->json(['categories'=>$categories], 200);    
    }

    /*
    |--------------------------------------------------------------------------
    | productsBasedOnCategory
    |--------------------------------------------------------------------------|
    */

    public function productsBasedOnCategory(Request $request)
    {


        $minPrice = explode(" Birr", $request->min_price)[0];

        $maxPrice = explode(" Birr", $request->max_price)[0];
        // productsize
        $multiple_size_filter = $request->multiple_size_filter;
        //productColor  
        $multiple_colors_filters = $request->multiple_colors_filter;
        // user
        $vendor_filter = $request->vendor_filter;

        $sort_by = $request->sort_by;

        $this->validate(request(), [

            'category_id' => 'required',
        
            'sort_by' => 'required'

        ]);


    
        if($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            
            
            if($sort_by == '0')
            {


                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();
            }
            elseif($sort_by == '1')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }

            elseif($sort_by == '2')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
                      
            
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            
            if($sort_by == '0')
            {


                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();

            }
            elseif($sort_by == '1')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '2')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])
                        
                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();
            }


            elseif($sort_by == '1')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif($sort_by == '2')
            {


                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter != null)
        {
            if($sort_by == '0')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();
            }
            elseif($sort_by == '1')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif($sort_by == '2')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter != null)
        {

            if($sort_by == '0'){

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();

            }
            elseif ($sort_by == '2') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('user', function($query) use($vendor_filter) {

                            $query->whereIn('id', $vendor_filter);

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();

            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters != null && $vendor_filter == null)
        {
            if($sort_by == '0')
            {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();
            }
            elseif ($sort_by == '1') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productColors', function($query) use($multiple_colors_filters) {
                                                
                            $query->whereIn('color_id', $multiple_colors_filters);
                        
                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter != null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
    
            }

            elseif ($sort_by == '2') {


                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])

                        ->whereHas('productSize', function($query) use($multiple_size_filter) {

                            $query->whereIn('size', $multiple_size_filter);    

                        })

                        ->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }
        }
        elseif($minPrice != "" && $maxPrice != "" && $multiple_size_filter == null && $multiple_colors_filters == null && $vendor_filter == null)
        {
            if($sort_by == '0'){

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->latest()
                        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'desc')
                        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('current_price', '>=', $minPrice)

                        ->where('current_price', '<=', $maxPrice)

                        ->where('category_id', $request->category_id)

                        ->orderBy('current_price', 'asc')
                        
                        ->get();
                
            }

        }
        else
        {
            if($sort_by == '0'){

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('category_id', $request->category_id)
                                        
                        ->latest()
        
                        ->get();

            }
            elseif ($sort_by == '1') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'desc')
        
                        ->get();
                
            }
            elseif ($sort_by == '2') {

                $unlimited_products =  $this->_product::with(['category', 'productColors', 'productSize', 'user', 'productImage', 'productColors.colors'])->where('category_id', $request->category_id)
                                        
                        ->orderBy('current_price', 'asc')
        
                        ->get();
                
            }

        }
    
        return response()->json(['products'=>$unlimited_products], 200);
    
    }
}
