<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use App\HomeCarousel;

use App\Product;

use App\Order;

use App\SpecialOffer;

use App\CompanySettings;

class HomeController extends Controller
{

	private $_carousel, $_product, $_order, $_category, $_settings;

	public function __construct()
	{
		$this->middleware('wizard');

		$this->_carousel = new HomeCarousel();

		$this->_product = new Product();

		$this->_order = new Order();

		$this->_category = new Category();

        $this->_offer = new SpecialOffer();

        $this->_settings = new CompanySettings();
	}

    public function index()
    {
    	$carousels = $this->_carousel::latest()->with(['category'])->get();

    	$latestP = $this->_product::latest()->take(30)->get();

    	$bestP = $this->_product::where('orders', '!=', 0)->orderBy('orders')->first();

        $best_products = $this->_product::where('orders', '>', 0)->orderBy('orders', 'desc')->with(['category', 'productImage', 'user'])->get();

    	$categories = $this->_category::latest()->get();

        $offers = $this->_offer->getLatestOffers();

        $settings = $this->_settings::first();

    	return view('pre-login.pages.home', compact('carousels', 'latestP', 'bestP', 'best_products', 'categories', 'offers', 'settings'));
    }
}
