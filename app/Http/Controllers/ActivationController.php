<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySettings;
use App\User;

class ActivationController extends Controller
{
	private $_setting, $_user;

	public function __construct() {
		$this->_setting = new CompanySettings();
        $this->_user = new User();
	}

    public function index()
    {
    	$settings = $this->_setting::first();

    	if(!is_null(auth()->user()))
    		if(auth()->user()->activation == 1)
    			return view('pre-login.pages.activation', compact('settings'));

    	return redirect('/');
    }

    public function update(Request $request)
    {
        $this->_user->activate($request->id);
        return response()->json(['message' => 'user is successfully activated!']);
    }
}
