<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\CompanySettings;

class CheckoutController extends Controller
{
	private $_setting;

	public function __construct()
	{
		$this->_setting = new CompanySettings();

		$this->middleware('auth');

		$this->middleware('wizard');
	}

    public function index()
    {
    	if(Cart::instance('default')->count() > 0) {
	    	$settings = $this->_setting::first();
	    	return view('pre-login.pages.checkout', compact('settings'));
    	}
    	else {
    		return redirect("/");
    	}

    }

    public function orderComplete()
    {
    	if(!session()->has('success')) {
    		return redirect('/');
    	}
    	return view('pre-login.pages.thankyou');
    }
}
