<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Colors;
use Cloudder;
use App\CompanySettings;
use App\Product;

class CartController extends Controller
{
	private $_setting, $_product;

	public function __construct()
	{
		$this->_setting = new CompanySettings();
		$this->_product = new Product();
		$this->middleware(['auth', 'wizard']);
	}

	/*
	|-----------------------------------------------------------
	| Cart Content
	|-----------------------------------------------------------
	*/

    public function index()
    {
    	$setting = $this->_setting::first();
    	return view('pre-login.pages.cart', compact('setting'));
    }

    /*
	|-----------------------------------------------------------
	| Create cart item
	|-----------------------------------------------------------
	*/

    public function store(Request $request)
    {

		$product_id = $request->product_id;
		$product_name = $request->product_name;
		$product_price = $request->product_price;
		$product_quantity = $request->product_quantity;
		$product_weight = $request->product_weight;
		$product_color = $request->product_color;
		$product_size = $request->product_size;
		$product_availability = $request->product_availability - $product_quantity;

		if(!is_null($product_color))
		{
			$this->validate(request(), [

				'product_size' => 'required',
				'product_color' => 'required',
				'product_quantity' => 'required'

			]);
		}
		else
		{

			$this->validate(request(), [

				'product_size' => 'required',
				'product_quantity' => 'required'

			]);

		}

		$duplicates = Cart::search(function($cartItem, $rowId) use ($request) {

			if ($cartItem->id == $request->product_id)
				return true;

			return false;

		});



		if($duplicates->isNotEmpty()) {
			return response()->json(['status'=>0, 'message'=>'Item is already in your cart!'], 200);
		}

		else
		{

			if($product_availability > 0)
			{
				if($product_quantity > 0)
				{
					if(!is_null($product_color))
					{
					Cart::add(['id'=>$product_id, 'name'=>$product_name, 'qty'=>$product_quantity, 'price'=>$product_price, 'weight' => $product_weight, 'options'=>['size'=>$product_size, 'color'=>$product_color]])->associate('App\Product');	
					}
					else
					{
					Cart::add(['id'=>$product_id, 'name'=>$product_name, 'qty'=>$product_quantity, 'price'=>$product_price, 'weight' => $product_weight, 'options'=>['size'=>$product_size]])->associate('App\Product');	

					}

					return response()->json(['status' => 1, 'product_name' => $product_name], 200);
				}
				else
				{
					return response()->json(['status'=>0, 'message'=>'Quantity should be greater than 0'], 200);
				}

			}
			else
			{
				return response()->json(['status'=>0, 'message'=>'Sorry! Product is not available!'], 200);
			}

		}


    }


    /*
	|-----------------------------------------------------------
	| Get cart - ON created()
	|-----------------------------------------------------------
	*/


    public function get_cart_list()
    {
    	$output = '';
    	$total_price = '';
		$i = 0;
		$setting = $this->_setting::first();


		if (Cart::instance('default')->count() > 0) {

			foreach (Cart::content() as $item) {

		$output .= 	'<li>
                   		<div class="product-image">
                            <img src="'. Cloudder::show($item->model->productImage()->first()->image_public_id, array("version" => $item->model->productImage()->first()->image_version, "quality" => "auto", "height" => 200, "width"=>200)).'" alt="'.$item->model->productImage()->first()->image_public_id.'" style="height: 50px; width: 50px; object-fit:cover;"/>
                        </div>


                        <div class="product-content">

                            <a class="product-link" href="#">'.$item->model->product_name.'</a>


                            <div class="cart-collateral">
                                <span class="qty-cart">'.$item->qty.'</span>&nbsp;<span>&#215;</span>&nbsp;<span class="product-price-amount"><span class="currency-sign">$</span>'.$item->model->current_price.'</span>
                            </div>

                            <input type="hidden" id="rowId'.$i.'" value="'.$item->rowId.'">
                            <a class="product-remove" onclick="removeFromCart('.$i.')">
                                <i class="fa fa-times-circle" aria-hidden="true"></i></a>
                        </div>
                    </li>';
			
			$i++;
			}

			$total_price = Cart::subtotal();


		}

		else
		{
			$total_price = "Empty";
		}

		$total_price_main = $this->_product->cartItemsPrice();
		$flat_rate = $this->_product->cartItemFlatRate();

		return response()->json(['status' => 1,  'output' => $output, 'total_price' => explode('.00', $total_price), 'cart_count'=>Cart::instance('default')->count(), 'total_price_main'=>$total_price_main, 'flat_rate' => $flat_rate], 200);
    }


    /*
	|-----------------------------------------------------------
	| Update care using the rowId of gloudmenns cart
	|-----------------------------------------------------------
	*/


    public function update(Request $request)
    {
    	$qty = $request->qty;
    	$rowId = $request->rowId;

    	Cart::update($rowId, $qty)->associate('App\Product');
    	return response()->json(['status' => 1], 200);
    }


    /*
	|-----------------------------------------------------------
	| Empty Cart
	|-----------------------------------------------------------
	*/


    public function empty()
    {
    	Cart::destroy();
    	return back()->with('success', 'Cart is emptied!');
    }


    /*
	|-----------------------------------------------------------
	| Remove the items from cart
	|-----------------------------------------------------------
	*/


    public function  removeFromCart(Request $request)
    {
    	$rowId = $request->rowId;
    	Cart::remove($rowId);
    	return response()->json(['status' => 1, 'message' => 'item is successfully removed'], 200);
    }
}
