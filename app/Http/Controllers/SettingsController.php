<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompanySettings;

class SettingsController extends Controller
{
	private $_setting;

    public function __construct()
    {
    	$this->middleware(['auth', 'admin']);
    	$this->_setting = new CompanySettings();
    }

    public function index()
    {
        $setting = $this->_setting::first();
    	return view('post-login.pages.settings.index', compact('setting'));
    }

    public function store()
    {
    	/*
    	/--------------------------------------------
    	/ variables sent from form ---> request() -->
    	/--------------------------------------------
    	*/

    	$location = request('location');
    	$slug = request('slug');
    	$company_name = request('company_name');
    	$email = request('email');
    	$phone_number = request('phone_number');
    	$facebook = request('facebook');
    	$twitter = request('twitter');
    	$google_plus = request('google_plus');
    	$instagram = request('instagram');
    	$logo = request('logo');
    	$favicon = request('favicon');
        $charge = request('charge');
        $delivery_price = request('delivery_price');
        $item_amount_flat_rate = request('item_amount_flat_rate');
        $customer_support = request('customer_support');
        $delivery_time = request('delivery_time');
        $delivery_email = request('delivery_email');



    	/*
    	/------------------------
    	/ validating the form
    	/------------------------
    	*/

    	if(!is_null($logo) && !is_null($favicon))
    	{

	    	$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'logo' => 'required|mimes:png,svg|max:7000',
                'charge' => 'required',
	    		'favicon' => 'required|mimes:png,svg|max:7000'

	    	]);

    	}

    	elseif(!is_null($favicon) && is_null($logo))
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'favicon' => 'required|mimes:png,svg|max:7000',
                'charge' => 'required',

	    	]);

    	}

    	elseif(is_null($favicon) && !is_null($logo))
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'phone_number' => 'required',
	    		'company_name' => 'required',
	    		'logo' => 'required|mimes:png,svg|max:7000',
                'charge' => 'required',

	    	]);

    	}

    	else
    	{

    		$this->validate(request(),[

	    		'location' => 'required',
	    		'slug' => 'required',
	    		'email' => 'required',
	    		'company_name' => 'required',
	    		'phone_number' => 'required',
                'charge' => 'required',

	    	]);

    	}

    	/*
    	/--------------------
    	/ Updating Settings
    	/--------------------
    	*/

    	$this->_setting->update_settings($company_name, $slug, $location, $phone_number, $email, $facebook, $twitter, $google_plus, $instagram, $charge, $logo, $favicon, $delivery_price, $item_amount_flat_rate, $customer_support, $delivery_email, $delivery_time);

    	return back()->with('success', 'Setting is successfully updated');
    }
}
