<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Address;

// use App\Delivery;

use Cloudinary;

// use App\paymentInfo;

use Illuminate\Support\Facades\Input;

class WizardController extends Controller
{
	private $_user, $_delivery, $_address, $_bankStatement;

	public function __construct()
	{
		$this->middleware(['auth', 'authenticatedWizard']);

		$this->_user = new User();

		$this->_address = new Address();

		// $this->_delivery = new Delivery();

		// $this->_bankStatement = new paymentInfo();
	}

    public function index()
    {
    	return view('post-login.pages.wizard.wizard');
    }

    public function store(Request $request)
    {

    	if($this->_user->isConnected() == true)
    	{

	    	$newsletter_register = null;

	    	$string_address = null;

	    	$array_address = null;

	    	$name = null;

	    	$shop_name = null;

	    	$about_shop = null;

	    	$current_user_id = auth()->user()->id;

	    	$current_user_role = auth()->user()->role;

	    	$current_user_email = auth()->user()->email;

	    	if(auth()->user()->role == 2)
	    	{
		    	$this->validate(request(), [

		    		'image' => 'required|max:6000',

		    		'shop_name' => 'required',

		    		'shop_slug' => 'required',

		    		'phone_number' => 'required',

		    		'agreement'=>'required',

		    	]);

		    	$shop_name = $request->shop_name;

		    	$about_shop = $request->shop_slug;
	    	}
	    	elseif(auth()->user()->role == 3)
	    	{
		    	$this->validate(request(), [

		    		'image' => 'required|max:6000',

		    		'fullname' => 'required',

		    		'phone_number' => 'required',

		    		'agreement'=>'required',

		    	]);

		    	$name = $request->fullname;
	    	}


	    	if(is_null(request('address1')))
	    	{
	    		$this->validate(request(), ['address'=>'required|array', 'address.*'=>'required']);
	    	
	    		$array_address = $request->address;

	    		$lat = $request->lat;

	    		$lng = $request->lng;
	    	}

	    	if(is_null(request('address')))
	    	{
				$this->validate(request(), ['address1'=>'required']);
		    	
	    		$string_address = $request->address1;

	    		$lat = $request->lat;

	    		$lng = $request->lng;
	    	}


	    	if(request('newsletters') == 'on')
	    	{
	    		$newletter_register = 1;
	    	}



	    	$image = $request->file('image');

	    	$phone_number = $request->phone_number;   	


	    	$image_name = $image->getRealPath();

	    	$_image_ = \Cloudinary\Uploader::upload($image_name, array("folder"=>"profile"));

	    	/*
	    	| -------------------------------------------------- |
	    	| list($width, $height) = getimagesize($image_name); |
	    	| -------------------------------------------------- |
	    	*/

	    	$image_public_id = $_image_['public_id'];

	    	$image_version = $_image_['version'];

	    	// $image_version = "static_url";

	    	// $image_public_id = "Static public id";

	    	$this->_address->addAddress($string_address, $array_address, $lat, $lng, $current_user_id);

	    	$this->_user->updateAccount($image_public_id, $phone_number, $shop_name, $about_shop, $current_user_id, $current_user_role, $image_version);


	    	// $pdt_token = $request->pdt_token;

	    	// $merchant_code = $request->merchant_code;

	    	// if($current_user_role == 2)
	    	// {
	    	// 	$this->_bankStatement->createMerchantYenePayBankIdentity($current_user_id, $pdt_token, $merchant_code);
	    	// }

	    	if ($newsletter_register == 1) {
	    		
	    		$this->_newsletter->addUserForNewsLetter($current_user_email);

	    	}

	    	return response()->json(['status' => 'success', "role" => $current_user_role]);
    	}
    	else
    	{
    		return response()->json(['status'=>'error', "message" => "Internet Connection is required!"]);
    	}

    }

}
