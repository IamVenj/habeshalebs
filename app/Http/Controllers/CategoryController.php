<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use App\User;

use Cloudinary;

use Cloudder;

class CategoryController extends Controller
{
	private $_category, $_user;

	public function __construct()
	{
		$this->middleware(['auth', 'admin'])->except('show');

		$this->_category = new Category();

        $this->_user = new User();
	}

    public function index()
    {
    	$categories = $this->_category::all();
        
		return view('post-login.pages.category.index', compact('categories'));
    }



    /*
    |----------------------------------------------------------------------
    | Show id a function that is independant of all authorization and role 
    |---------------------------------------------------------------------
    \ Its used to show the prodicts based on the category_name & category_id
    \ The route is {category_name}/products/{category_id}
    |----------------------------------------------------------------------
    */

    public function show($category_name, $category_id)
    {

    }




    public function store(Request $request)
    {
        if($this->_user->isConnected() == true)
        {

        	$this->validate(request(), [

        		"image" => 'required|mimes:jpeg|max:5000',

        		"categoryName" => 'required'

        	]);

        	$image = $request->file('image');

            $image_name = $image->getRealPath();

            $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"category"));

            $image_public_id = $image_result['public_id'];

            $image_url = $image_result['secure_url'];

            $image_version = $image_result['version'];

            $category_name = $request->categoryName;

            $this->_category->createCategory($category_name, $image_public_id, $image_url, $image_version);

            $message = "Category ".$category_name." is successfully created";

        	return response()->json(["status" => 1, "message" => $message]);

        }
        else
        {
            return response()->json(['status' => 0, "message" => "Internet Connection is required!"]);
        }

    }


    public function update(Request $request)
    {

        if($this->_user->isConnected() == true)
        {

            $this->validate(request(), [

                'categoryName' => 'required',

            ]);

            $category_id = $request->category_id;

            $category_name = $request->categoryName;

            $parent_id = $request->mainCategory;


            $this->_category->updateCategory($category_name, $parent_id, $category_id);

            return response()->json(['status' => 1, "message" => "Category is successfully updated!"]);

        }
        else
        {
            return response()->json(['status' => 0, "message" => "Internet Connection is required!"]);
        }


    }


    public function destroy(Request $request)
    {

        if($this->_user->isConnected() == true)
        {

            $id = $request->category_id;

            $this->_category->destroyCategory($id);

            return response()->json(['status' => 1, "message" => "Category is successfully deleted!"]);

        }
        else
        {
            return response()->json(['success' => 0, 'message' => "internet Connection is required!"]);
        }

    }

    public function updateImage(Request $request)
    {

        if($this->_user->isConnected() == true)
        {

            $this->validate(request(), ['image_update'=>'required|max:6000|mimes:jpeg,png']);

            $image = $request->file('image_update');

            $image_name = $image->getRealPath();

            $id = $request->category_id;

            $curretImage = $this->category->currentImage($id);

            \Cloudder::destroy($currentImage);

            $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"category"));

            $image_public_id = $image_result['public_id'];

            $image_url = $image_result['secure_url'];

            $image_version = $image_result['version']; 

            $this->_category->updateCategoryImage($id, $image_public_id, $image_url, $image_version);

            return response()->json(['status' => 1, 'message' => "Image is successfully updated!"]);

        }
        else
        {
            return response()->json(['status' => 0, 'message' => 'Internet Connection is required!']);
        }


    }


    // --------------------------------------------------------------------------------- //
    // AJAX REQUESTS
    // --------------------------------------------------------------------------------- //




    public function postview()
    {
        $categories = $this->_category::orderBy('created_at', 'desc')->get();

        $output = '<table class="order-listing table myDataTable">

            <thead>

              <tr>

                  <th>#</th>

                  <th>Category Name</th>

                  <th>Category Image</th>

                  <th>Actions</th>

              </tr>

            </thead>

            <tbody>';

        if($categories->count() > 0)
        {
            $count = 0;

            foreach ($categories as $category) {
                
                $count = $count + 1;

                $output .= '<tr>


                <td>'.$count.'</td>

                <td>'.$category->category_name.'</td>

                  <td><img src="'.Cloudder::show($category->image_public_id, ['width'=>50, 'height'=>50, 'crop'=>'fill', 'radius'=>'max']).'"></td>

                  <td>

                    <div class="dropdown">

                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Manage

                      </button>

                      <div class="dropdown-menu">

                        <button class="dropdown-item edit_category" id="'.$category->id.'">

                            <i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>Edit</button>

                        <div class="dropdown-divider"></div>

                        <button class="dropdown-item edit_category_image" id="'.$category->id.'">

                            <i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>Update Image</button>

                        <div class="dropdown-divider"></div>

                        <button class="dropdown-item delete_category" id="'.$category->id.'">

                            <i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>Delete</button>

                      </div>

                    </div>

                  </td>

                </tr>';

            }
        }

          $output .= '</tbody>

          </table>';

        echo $output;
    }

    public function show_categories_edit_modal(Request $request)
    {
        $get_category = $this->_category::find($request->category_id);

        return response()->json(['category_name'=>$get_category->category_name, "parent" => $get_category->parent_id]);
    }

    public function show_categories_edit_image_modal(Request $request)
    {
        $get_category = $this->_category::find($request->category_id);

        return response()->json(['image_url' => $get_category->image_url]);
    }

}
