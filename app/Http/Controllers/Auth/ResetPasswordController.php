<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Support\Str;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Password;

use Illuminate\Auth\Events\PasswordReset;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
        $this->middleware('guest');
    
    }

    public function showResetForm(Request $request, $token = null)
    {

        return view('pre-login.auth.reset-password')->with(

            ['token' => $token, 'reset-email' => $request->email]

        );

    }
    
    public function reset(Request $request)
    {
    
        $request->validate($this->rules(), $this->validationErrorMessages());


        $response = $this->broker()->reset(
    
            $this->credentials($request), function ($user, $password) {
    
                $this->resetPassword($user, $password);
    
            }
    
        );


        return $response == Password::PASSWORD_RESET
    
                    ? $this->sendResetResponse($request, $response)
    
                    : $this->sendResetFailedResponse($request, $response);
    
    }


    protected function rules()
    {
    
        return [
    
            'token' => 'required',
    
            'reset-email' => 'required|email',
    
            'password' => 'required|confirmed|min:6',
    
        ];
    
    }

    protected function validationErrorMessages()
    
    {
    
        return [];
    
    }


    protected function credentials(Request $request)
    
    {
    
        return $request->only(
    
            'reset-email', 'password', 'password_confirmation', 'token'
    
        );
    
    }


    protected function resetPassword($user, $password)
    {
    
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

    }


    protected function sendResetResponse(Request $request, $response)
    {

        return redirect($this->redirectPath())

                            ->with('success', trans($response));

    }


    protected function sendResetFailedResponse(Request $request, $response)
    {

        return redirect()->back()

                    ->withInput($request->only('reset-email'))

                    ->withErrors(['reset-email' => trans($response)]);

    }

    public function broker()
    {

        return Password::broker();

    }


    protected function guard()
    {

        return Auth::guard();

    }
}
