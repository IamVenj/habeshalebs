<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Password;

use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('pre-login.auth.forgot-password');
    }

    public function sendResetLinkEmail(Request $request)
    {

        $this->validateEmail($request);

        $user_m = new User();

        $check_for_connection = $user_m->isConnected();

        if($check_for_connection == true)
        {

            $response = $this->broker()->sendResetLink(

                $request->only('forgot-email')

            );

            return $response == Password::RESET_LINK_SENT

                        ? $this->sendResetLinkResponse($request, $response)

                        : $this->sendResetLinkFailedResponse($request, $response);

        }
        else
        {

            return redirect()->back()->withErrors('Please Check Your Internet Connection!');

        }

    }
    protected function validateEmail(Request $request)
    {

        $request->validate(['forgot-email' => 'required|email']);

    }


    protected function sendResetLinkResponse(Request $request, $response)
    {

        return back()->with('success', trans($response));

    }


    protected function sendResetLinkFailedResponse(Request $request, $response)
    {

        return back()

                ->withInput($request->only('forgot-email'))

                ->withErrors(['forgot-email' => trans($response)]);

    }


    public function broker()
    {

        return Password::broker();

    }
}
