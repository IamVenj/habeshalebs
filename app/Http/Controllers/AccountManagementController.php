<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

// use App\Delivery;

class AccountManagementController extends Controller
{
	private $_user, $_delivery;

    public function __construct()
    {
    	$this->middleware('auth');

    	$this->_user = new User();

        // $this->_delivery = new Delivery();
    }

    public function index()
    {
        if(auth()->user()->role != 3)
        {

        	$user = $this->_user::find(auth()->user()->id);

        	return view('post-login.pages.user.my-account', compact('user'));
        }
        else
        {
            return back();
        }
    }

    public function customerAccountView()
    {
        $user = $this->_user::find(auth()->user()->id);
    
    	return view('pre-login.pages.my.my-account', compact('user'));
    }

    public function updateAccount(Request $request)
    {

        $email = $request->email;

        $currentUserId = auth()->user()->id;

        if(auth()->user()->role == 1)
        {

            $this->validate(request(), [

                'name' => 'required',

                'email' => 'required'

            ]);
            
            $name = $request->name;

            $this->_user->updateAdmin($currentUserId, $name, $email);

        }


        if(auth()->user()->role == 2)
        {

            $this->validate(request(), [

                'shop_name' => 'required',

                'email' => 'required',

                'about_shop' => 'required',

                'phone_number' => 'required',

            ]);

            $shop_name = $request->shop_name;

            $about_shop = $request->about_shop;

            $phone_number = $request->phone_number;

            $this->_user->updateSeller($currentUserId, $shop_name, $about_shop, $phone_number, $email);

        }

        return back()->with('success', 'Account is successfully updated!');

    }

    public function updateAccountForCustomer(Request $request)
    {

        $this->validate(request(), [

            'name' => 'required',

            'email' => 'required',

            'phone_number' => 'required'

        ]);

        $id = auth()->user()->id;

        $email = $request->email;

        $phone_number = $request->phone_number;

        $firstname = $request->firstname;
        
        $lastname = $request->lastname;

        $this->_user->updateCustomer($firstname, $lastname, $email, $phone_number, $id);

        return back()->with('success', 'Account is successfully updated!');

    }

    public function updateProfilePic(Request $request)
    {

        $this->validate(request(), [

            'image' => 'required|mimes:png,jpeg|max:6000'

        ]);

        $image = $request->file('image');

        $this->_user->updateProfilePicture($image, auth()->user()->id);

        return back()->with('success', 'Your profile picture is successfully updated');

    }

    public function changePassword(Request $request)
    {
        $this->validate(request(), [

            'current_password' => 'required',

            'new_password' => 'required',

        ]);

        $current_password = $request->current_password;

        $new_password = $request->new_password;

        if($this->_user->checkIfCurrentPasswordIsCorrect($current_password) == true)
        {
            $this->_user->changePassword($new_password);

            return back()->with('success', 'Password is successfully changed!');
        }
        else
        {
            return back()->withErrors('Your current password is incorrect!');
        }

    }

    // public function updateDelivery()
    // {

    //     $delivery = request('m-delivery');

    //     if($delivery == "on")
    //     {
    //         $this->validate(request(), ['price' => 'required']);

    //         $status = 1;

    //         $price = request('price');

    //         if($this->_delivery->get_option() == true)
    //         {
    //             $this->_delivery->updateDeliveryState($status, $price);
    //         }
    //         else
    //         {
    //             $this->_delivery->addDeliveryStatus($price, $status, auth()->user()->id);
    //         }
    //     }
    //     elseif(is_null($delivery))
    //     {
    //         $status = 0;

    //         $this->_delivery->removeDeliveryState();
    //     }

    //     $this->_user->updateDeliveryStatus($status);

    //     return back()->with('success', 'Delivery option is successfully updated!');

    // }
}
