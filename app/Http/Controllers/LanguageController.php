<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

use URL;

class LanguageController extends Controller
{
    public function update()
    {
        request()->validate([

            'lang' => 'required'

        ]);

        $locale = request('lang');

        if (in_array($locale, \Config::get('app.locales'))) {

            Session::put('locale', $locale);

        }


        return response()->json(['url'=>URL::previous()], 200);
    }
}
