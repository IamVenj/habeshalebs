<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserListController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth', 'admin']);
	}

    public function index()
    {	
		return view('post-login.pages.registered-users.index');	
    }

    public function showUsers()
    {
    	$users = User::where('role', request('user_role'))->get();

        $address = "";

        foreach ($users as $user) {
            
            $address = $user->addresses()->get();

        }

    	return response()->json(["user"=>$users, "address"=>$address], 200);
    }
}
