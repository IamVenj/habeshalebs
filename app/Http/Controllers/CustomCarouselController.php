<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HomeCarousel;

use App\User;

class CustomCarouselController extends Controller
{
	private $_carousel, $_user;

    public function __construct()
    {
    	$this->middleware(['auth', 'admin']);

    	$this->_carousel = new HomeCarousel();

    	$this->_user = new User();
    }

    public function index()
    {
        $carousels = $this->_carousel::paginate(12);

    	return view('post-login.pages.custom-pages.home.home-carousel.index', compact('carousels'));
    }

    public function store(Request $request)
    {
    	$this->validate(request() ,[

    		'category' => 'required',

    		'image' => 'required|mimes:jpeg,png|max:6000'

    	]);

    	if($this->_user->isConnected() == true)
    	{

	    	$category_id = $request->category;

	    	$image = $request->file('image');

	    	$image_name = $image->getRealPath();



	        $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"home_carousel"));
	        
	        $image_public_id = $image_result['public_id'];

	        $image_version = $image_result['version'];

	    	
	    	$this->_carousel->createCarousel($category_id, $image_public_id, $image_version);

	    	return back()->with('success', 'Carousel is successfully created!');
    	}

    	return back()->withErrors('Internet Connection is required');

    }

    public function update($id, Request $request)
    {
    	$this->validate(request(), [

    		'category' => 'required'

    	]);

    	$category_id = $request->category;

    	$this->_carousel->updateCarouselCategory($category_id, $id);

    	return back()->with('success', 'Carousel is successfully updated!');

    }

    public function updateImage($id, Request $request)
    {
    	$this->validate(request(), [

    		'image' => 'required|mimes:jpeg,png|max:6000'

    	]);


    	if($this->_user->isConnected() == true)
    	{
	    	$image = $request->file('image');

	    	$image_name = $image->getRealPath();



	        $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"home_carousel"));
	        
	        $image_public_id = $image_result['public_id'];

	        $image_version = $image_result['version'];

	        

	        $this->_carousel->updateCarouselImage($id, $image_public_id, $image_version);

	        return back()->with('success', 'Image is successfully updated!');

    	}

    	return back()->withErrors('Internet Connection is required');

    }

    public function destroy($id)
    {
    	if($this->_user->isConnected() == true)
    	{
	    	$this->_carousel->destroyCarousel($id);

	    	return back()->with('success', 'Carousel is successfully deleted!');
	    }
    	return back()->withErrors('Internet Connection is required');
    }
}
