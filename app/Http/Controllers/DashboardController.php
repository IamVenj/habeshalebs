<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;

use App\Dashboard;

use App\User;

class DashboardController extends Controller
{
    private $_order, $_dashboard, $_user;

	public function __construct()
	{
		$this->middleware(['auth', 'wizard']);
	
        $this->_order = new Order();
        $this->_dashboard = new Dashboard();
        $this->_user = new User();
    }

    

    public function index()
    {
    	if(auth()->user()->role != 3)
    	{
            
            $monthlyIncome = $this->_dashboard->monthlyIncome(null);

            $yearlyIncome = $this->_dashboard->yearlyIncome();

            $dailyIncome = $this->_dashboard->dailyIncome();

            $cashSales = $this->_dashboard->monthlyIncome('sales');

            $cashSalesPercentage = $this->_dashboard->getCashSales()['percentage'];

            $cashSalesStatus = $this->_dashboard->getCashSales()['status'];

            $total_customers = $this->_user->role_customer()->count();

            $total_vendors = $this->_user->role_vendor()->count();

            $total_price = $this->_dashboard->allIncome();


    		return view('post-login.pages.dashboard.dashboard', compact('monthlyIncome', 'yearlyIncome', 'dailyIncome', 'cashSales', 'cashSalesPercentage', 'cashSalesStatus', 'total_customers', 'total_vendors', 'total_price'));
    	
        }
    	else
    	{
    		return back();
    	}
    }

    public function getMonthlyIncome()
    {
        return $this->_dashboard->getMonthlyIncome();
    }

    public function getDailyIncome()
    {
        return $this->_dashboard->getDailyIncome();
    }

    public function getCashIncome()
    {
        return $this->_dashboard->getCashSales();
    }

    public function getYearlyIncome()
    {
        return $this->_dashboard->getYearlyIncome();
    }

    public function getAdminAllIncome()
    {
        $all_income = [];

        array_push($all_income, $this->_dashboard->getDataOfThisYr());
        array_push($all_income, $this->_dashboard->getDataOfLastYr());
        array_push($all_income, $this->_dashboard->getDataBeforeLastYr());

        return $all_income;
    }
}
