<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Debt;

class DebtController extends Controller
{
	private $_debt;

    public function __construct()
    {
    	$this->middleware('auth');

        $this->middleware('seller')->except(['index', 'show', 'update', 'updateEach']);

    	$this->middleware('admin')->except('seller_index');

    	$this->_debt = new Debt();
    }

    public function index()
    {
    	$debts = $this->_debt::distinct()->get()->groupBy('vendor_id');

    	return view('post-login.pages.debt.index', compact('debts'));
    }

    public function show($id)
    {
    	$debts = $this->_debt::where('vendor_id', $id)->get();
    	$debts_sum = $this->_debt::where('vendor_id', $id)->where('debt_status', 0)->sum('debt');
    	$debtStatus = $this->_debt::where('vendor_id', $id)->where('debt_status', 0)->count();

    	return view('post-login.pages.debt.show', compact('debts', 'debts_sum', 'id', 'debtStatus'));
    }

    public function update($id)
    {
    	$this->_debt->changeDebtStatus($id, 1);

    	return back()->with('success', 'Debt payment is successfully updated!');
    }

    public function updateEach($debt_id)
    {
        $this->_debt->changeDebtStatusForEach(1, $debt_id);

        return back()->with('success', 'Debt payment is successfully updated!');
    }

    public function seller_index()
    {
    	$debts = $this->_debt::where('vendor_id', auth()->user()->id)->where('debt_status', 0)->get();

        $debts_sum = $this->_debt::where('vendor_id', auth()->user()->id)->where('debt_status', 0)->sum('debt');

        auth()->user()->unreadNotifications()->where('type', 'App\Notifications\NotifyMerchantOnDebt')->get()->MarkAsRead();

        return view('post-login.pages.debt.seller-index', compact('debts', 'debts_sum'));
    }
}
