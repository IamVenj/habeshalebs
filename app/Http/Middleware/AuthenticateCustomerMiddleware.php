<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateCustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null(auth()->user()))
        {
            if(auth()->user()->role == 3)
            {
                return $next($request);
            }
            else
            {
                return redirect('/dashboard');
            }
        }
    }
}
