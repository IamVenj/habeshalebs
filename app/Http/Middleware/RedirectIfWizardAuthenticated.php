<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfWizardAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth()->user()->role == 2)
        {
            if(!is_null(auth()->user()->shop_name) && !is_null(auth()->user()->phone_number) && !is_null(auth()->user()->about_shop) && auth()->user()->addresses()->count() > 0)
            {
                return redirect('/dashboard');
            }
            else
            {
                return $next($request);
            }
        }

        return $next($request);
    }
}
