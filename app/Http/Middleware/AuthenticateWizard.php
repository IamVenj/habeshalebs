<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateWizard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!is_null(auth()->user()))
        {
            if(auth()->user()->role == 3)
            {
                return $next($request);
            }
            elseif(auth()->user()->role == 2)
            {
                if(auth()->user()->activation == 1) {
                    return redirect('/unactivated');
                } elseif(is_null(auth()->user()->shop_name) || is_null(auth()->user()->phone_number) || is_null(auth()->user()->about_shop) || auth()->user()->addresses()->count() == 0) {
                    return redirect(route('wizard-view'));
                } else {
                    return $next($request);
                }
            }
            
        }

    
        return $next($request);
        
    
    }

}
