<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null(auth()->user()))
        {
            if(auth()->user()->role == 2 && auth()->user()->activation == 0)
            {
                return $next($request);
            }
            elseif(auth()->user()->role == 2 && auth()->user()->activation == 1)
            {
                return redirect('/unactivated');
            }
            else
            {
                return redirect('/dashboard');
            }
        }
    }
}
