<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;

use App\CompanySettings;

class Dashboard extends Model
{
	private $_order, $_settings;

	public function __construct()
	{
		$this->_order = new Order();
		$this->_settings = new CompanySettings();
	}

    	public function monthlyIncome($cashsales)
    	{
    		$PM = [];
    		$newPM = [];

    		if(is_null($cashsales))
    		{
        		$monthlyIncome = $this->getMonthlyIncome();
    		}
    		else
    		{
    			$monthlyIncome = $this->getCashSales();
    		}

        	$monthlyIncomePrice = $monthlyIncome['total_product_price'];
        	$monthlyIncomeMonth = $monthlyIncome['months'];

        	foreach ($monthlyIncomeMonth as $key => $value) {
            	foreach ($monthlyIncomePrice as $key => $value) {
                		$PM[$monthlyIncomeMonth[$key]] = $monthlyIncomePrice[$key];
            	}
        	}

        	return $PM;
    	}

    	public function yearlyIncome()
    	{
        	$PY = [];

        	$yearlyIncome = $this->getYearlyIncome();

        	$yearlyIncomePrice = $yearlyIncome['total_product_price'];
        	$yearlyIncomeYrs = $yearlyIncome['yrs'];

        	foreach ($yearlyIncomeYrs as $key => $value) {
            	foreach ($yearlyIncomePrice as $key => $value) {
                		$PY[$yearlyIncomeYrs[$key]] = $yearlyIncomePrice[$key];
            	}
        	}

        	return $PY;
    	}

    	public function dailyIncome()
    	{
        	$PD = [];

        	$dailyIncome = $this->getdailyIncome();

        	$dailyIncomePrice = $dailyIncome['total_product_price'];
        	$dailyIncomeDays = $dailyIncome['days'];

        	foreach ($dailyIncomeDays as $key => $value) {
            	foreach ($dailyIncomePrice as $key => $value) {
                		$PD[$dailyIncomeDays[$key]] = $dailyIncomePrice[$key];
            	}
        	}

        	return $PD;
    	}

    	public function allIncome()
    	{
    		$AI = [];
            $orders = $this->_order->all();
            foreach ($orders as $order) {
            	array_push($AI, $order->product()->first()->current_price);
            }
            $product_price = array_sum($AI) * ($this->_settings::first()->charge/100);
            return $product_price;
    	}

    	public function getMonthlyIncome()
    	{
        	$monthly_product_price_array = array();
        	$month_array = $this->_order->getAllMonthsOrDaysOrYrs('m');
        	$month_name_array = [];

        	if(!empty($month_array))
        	{
            	foreach ($month_array as $month_no => $month_name) 
            	{
                		$monthly_product_price = $this->_order->getProductPrice($month_no, 'm', 'vendor');
                		array_push($monthly_product_price_array, $monthly_product_price);
                		array_push($month_name_array, $month_name);
            	}
        	}

        	$max = null;

        	if(!empty($monthly_product_price_array))
        	{
	        	$max_no = max($monthly_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
		}
        	
        	$monthlyIncomeArray = [

	            'months' => $month_name_array,
	            'total_product_price' => $monthly_product_price_array,
	            'max' => $max

        	];

        	return $monthlyIncomeArray;

    	}

    	public function getYearlyIncome()
    	{
        	$yearly_product_price_array = array();
        	$year_array = $this->_order->getAllMonthsOrDaysOrYrs('yr');
        	$year_name_array = [];

        	if(!empty($year_array))
        	{
            	foreach ($year_array as $year_no => $year_name) 
            	{
                		$yearly_product_price = $this->_order->getProductPrice($year_no, 'yr', 'vendor');
                		array_push($yearly_product_price_array, $yearly_product_price);
                		array_push($year_name_array, $year_name);
            	}
        	}

        	$max = null;

        	if(!empty($yearly_product_price_array))
        	{
	        	$max_no = max($yearly_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
        	}

        	$yearlyIncomeArray = [

	            'yrs' => $year_name_array,
	            'total_product_price' => $yearly_product_price_array,
	            'max' => $max

        	];

        	return $yearlyIncomeArray;

    	}


    	public function getDailyIncome()
    	{
    		$daily_product_price_array = array();
        	$day_array = $this->_order->getAllMonthsOrDaysOrYrs('d');
        	$day_name_array = [];

        	if(!empty($day_array))
        	{
            	foreach ($day_array as $day_no => $day_name) 
            	{
                		$daily_product_price = $this->_order->getProductPrice($day_no, 'd', 'vendor');
                		array_push($daily_product_price_array, $daily_product_price);
                		array_push($day_name_array, $day_name);
            	}
        	}

        	$max = null;

        	if(!empty($daily_product_price_array))
        	{
	        	$max_no = max($daily_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;	
        	}

        	$dailyIncomeArray = [

	            'days' => $day_name_array,
	            'total_product_price' => $daily_product_price_array,
	            'max' => $max

        	];

        	return $dailyIncomeArray;
    	}

    	public function getCashSales()
    	{
    		$comparison_product_price_array = array();
        	$month_array = $this->_order->getAllMonthsOrDaysOrYrs('comparison');
        	$month_name_array = [];

        	if(!empty($month_array))
        	{
            	foreach ($month_array as $month_no => $month_name) 
            	{
                		$monthly_product_price = $this->_order->getProductPrice($month_no, 'm', 'vendor');
                		array_push($comparison_product_price_array, $monthly_product_price);
                		array_push($month_name_array, $month_name);
            	}
        	}

        	$max = null;
        	$percentage = null;
        	$status = null;

        	if(!empty($comparison_product_price_array))
        	{
	        	$max_no = max($comparison_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
	        	
	        	$percentage = round((min($comparison_product_price_array) / max($comparison_product_price_array)) * 100);
	        	
                if(!is_null($comparison_product_price_array) && array_key_exists(1, $comparison_product_price_array)) 
                {
    	        	if($comparison_product_price_array[0] > $comparison_product_price_array[1])
    	        		$status = 'decrease';
                }
		}
        	


        	$monthlyIncomeArray = [

	            'months' => $month_name_array,
	            'total_product_price' => $comparison_product_price_array,
	            'max' => $max,
	            'percentage' => $percentage,
	            'status' => $status

        	];

        	return $monthlyIncomeArray;
    	}


    	public function getDataOfThisYr()
    	{

        	$monthly_product_price_array = array();
        	$month_array = $this->_order->getAllMonthsOrDaysOrYrs('this_yr');
        	$month_name_array = [];

        	if(!empty($month_array))
        	{
            	foreach ($month_array as $month_no => $month_name) 
            	{
                		$monthly_product_price = $this->_order->getProductPrice($month_no, 'm', 'admin');
                		array_push($monthly_product_price_array, $monthly_product_price);
                		array_push($month_name_array, $month_name);
            	}
        	}

        	$max = null;

        	if(!empty($monthly_product_price_array))
        	{
	        	$max_no = max($monthly_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
	     	}

        	$monthlyIncomeArray = [

	            'months' => $month_name_array,
	            'total_product_price' => $monthly_product_price_array,
	            'max' => $max

        	];

        	return $monthlyIncomeArray;
    	}

    	public function getDataOfLastYr()
    	{

        	$monthly_product_price_array = array();
        	$month_array = $this->_order->getAllMonthsOrDaysOrYrs('last_yr');
        	$month_name_array = [];

        	if(!empty($month_array))
        	{
            	foreach ($month_array as $month_no => $month_name) 
            	{
                		$monthly_product_price = $this->_order->getProductPrice($month_no, 'm', 'admin');
                		array_push($monthly_product_price_array, $monthly_product_price);
                		array_push($month_name_array, $month_name);
            	}
        	}

        	$max = null;

        	if(!empty($monthly_product_price_array))
        	{
	        	$max_no = max($monthly_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
	     	}

        	$monthlyIncomeArray = [

	            'months' => $month_name_array,
	            'total_product_price' => $monthly_product_price_array,
	            'max' => $max

        	];

        	return $monthlyIncomeArray;
    	}

    	public function getDataBeforeLastYr()
    	{

        	$monthly_product_price_array = array();
        	$month_array = $this->_order->getAllMonthsOrDaysOrYrs('before_yr');
        	$month_name_array = [];

        	if(!empty($month_array))
        	{
            	foreach ($month_array as $month_no => $month_name) 
            	{
                		$monthly_product_price = $this->_order->getProductPrice($month_no, 'm', 'admin');
                		array_push($monthly_product_price_array, $monthly_product_price);
                		array_push($month_name_array, $month_name);
            	}
        	}

        	$max = null;

        	if(!empty($monthly_product_price_array))
        	{
	        	$max_no = max($monthly_product_price_array);
	        	$max = round(($max_no + 10/2) / 10) * 10;
	     	}

        	$monthlyIncomeArray = [

	            'months' => $month_name_array,
	            'total_product_price' => $monthly_product_price_array,
	            'max' => $max

        	];

        	return $monthlyIncomeArray;
    	}
}
