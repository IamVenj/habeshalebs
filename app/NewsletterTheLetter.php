<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Mail\NewsLetter;

use App\CompanySettings;

class NewsletterTheLetter extends Model
{
    protected $fillable = ['title', 'slug'];

    public function createTheLetter($title, $slug)
    {
    	$this::create([

    		'title' => $title,

    		'slug' => $slug,

    	]);
    }

    public function updateTheLetter($title, $slug, $id)
    {
    	$theLetter = $this::find($id);

    	$theLetter->title = $title;

    	$theLetter->slug = $slug;

    	$theLetter->save();
    }

    public function destroyTheLetter($id)
    {
    	$this::find($id)->delete();
    }

    public function sendMail($users, $id)
    {
    	$theLetter = $this::find($id);

        $setting = CompanySettings::first();

    	foreach ($users as $user) {

            \Mail::to($user)->send(new NewsLetter($theLetter, $setting));

        }
    }
}
