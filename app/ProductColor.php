<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;

class ProductColor extends Model
{
    protected $fillable = ['color_id', 'product_id'];

    public function products()
    {
    	return $this->belongsToMany(Product::class);
    }

    public function colors()
    {
        return $this->hasMany('App\Colors', 'id', 'color_id');
    }

    public function addColorsForaSingleProduct($color_id, $product_id)
    {
    	$this::create([

    		'color_id' => $color_id,

    		'product_id' => $product_id

    	]);
    }

    public function removeProductColor($productColor_id)
    {
        $productColor = $this::find($productColor_id);

        $productColor->delete();
    }

    public function updateProductColor($productColor_id, $color_id)
    {
        $productColor = $this::find($productColor_id);

        $productColor->color_id = $color_id;

        $productColor->save();
    }

}