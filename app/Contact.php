<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['fullname', 'email', 'message'];

    public function storeContactMessage($fullname, $email, $message)
    {
    	$this::create([

    		'fullname' => $fullname, 
    		'email' => $email,
    		'message' => $message

    	]);
    }

    public function checkIfContactExists($fullname, $email, $message)
    {
    	$count = $this::where('fullname', $fullname)->where('email', $email)->where('message', $message)->count();
    	if($count > 0)
    		return true;
    	return false;
    }
}
