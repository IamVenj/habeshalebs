<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyMerchantOnDebt extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $debt_sum, $created_at;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($debt_sum, $created_at)
    {
        $this->debt_sum = $debt_sum;

        $this->created_at = $created_at;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [

            'DebtSum' => $this->debt_sum,

            'created_at' => $this->created_at

        ];
    }

    public function toBroadcast($notifiable)
    {
        return [

            'DebtSum' => $this->debt_sum,

            'created_at' => $this->created_at

        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
