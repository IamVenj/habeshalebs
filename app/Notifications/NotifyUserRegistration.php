<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class NotifyUserRegistration extends Notification
{
    use Queueable;

    public $_user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->_user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {

        if($this->_user->role == 2)
        {
            return [

                'name' => $this->_user->shop_name,

                'role' => $this->_user->role

            ];
        }
        elseif ($this->_user->role == 3) 
        {
            return [

                'name' => $this->_user->firstname.' '.$this->_user->lastname,

                'role' => $this->_user->role

            ];
        }

    }

    public function toBroadcast($notifiable)
    {

        if($this->_user->role == 2)
        {
            return [

                'name' => $this->_user->shop_name,

                'role' => $this->_user->role

            ];
        }
        elseif ($this->_user->role == 3) 
        {
            return [

                'name' => $this->_user->firstname.' '.$this->_user->lastname,

                'role' => $this->_user->role

            ];
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
