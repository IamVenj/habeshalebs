<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeCarousel extends Model
{
    protected $fillable = ['category_id', 'image_public_id', 'image_version'];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function createCarousel($category_id, $image_public_id, $version)
    {
    	$this::create([

    		'category_id' => $category_id,

    		'image_public_id' => $image_public_id,

    		'image_version' => $version

    	]);
    }

    public function updateCarouselCategory($category_id, $id)
    {

    	$carousel = $this::find($id);

    	$carousel->category_id = $category_id;

    	$carousel->save();

    }

    public function deletePreviousCarouselImage($id)
    {

    	$carousel = $this::find($id);

    	\Cloudder::destroy($carousel->image_public_id);

    }

    public function updateCarouselImage($id, $image_public_id, $version)
    {

    	$this->deletePreviousCarouselImage($id);

    	$carousel = $this::find($id);

    	$carousel->image_public_id = $image_public_id;

    	$carousel->image_version = $version;

    	$carousel->save();

    }

    public function destroyCarousel($id)
    {
    	$this->deletePreviousCarouselImage($id);

    	$this::find($id)->delete();
    }
}
