<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySettings extends Model
{
    protected $fillable = ['company_name', 'slug', 'location', 'phone_number', 'email', 'facebook', 'twitter', 'google_plus', 'instagram', 'charge', 'delivery_price', 'item_amount_flat_rate', 'customer_support', 'delivery_email', 'delivery_time'];

     public function update_settings($company_name, $slug, $location, $phone_number, $email, $facebook, $twitter, $google_plus, $instagram, $charge, $logo, $favicon, $delivery_price, $item_amount_flat_rate, $customer_support, $delivery_email, $delivery_time)
    {

    	$settings = $this::first();

    	/*
        /------------------------------------------------------------
        / If logo and favicon request is not null get its extension,
        / and Delete the existing file then update the new file.
        / while saving, move the new image in the public folder.
        /------------------------------------------------------------
        */

        /*
        /--------------------------------------------------------------------------
        / even If logo and favicon request is null just update the rest
        /--------------------------------------------------------------------------
        */

    	if (!is_null($logo) && !is_null($favicon)) 
    	{
    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();
    		Storage::delete(['public/uploads/'.$logo_update, 'public/uploads/'.$favicon_update]);
			Storage::putFileAs('public/uploads', $logo, $logo_update);
			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}

    	elseif(!is_null($logo) && is_null($favicon))

    	{

    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		Storage::delete('public/uploads/'.$logo_update);
			Storage::putFileAs('public/uploads', $logo, $logo_update);

    	}

    	elseif(is_null($logo) && !is_null($favicon))

    	{

    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();
    		Storage::delete('public/uploads/'.$favicon_update);
			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}


    	$settings->location = $location;
    	$settings->slug = $slug;
    	$settings->email = $email;
    	$settings->company_name = $company_name;
    	$settings->phone_number = $phone_number;
    	$settings->facebook = $facebook;
    	$settings->twitter = $twitter;
    	$settings->google_plus = $google_plus;
    	$settings->instagram = $instagram;
        $settings->charge = $charge;
        $settings->delivery_price = $delivery_price;
        $settings->item_amount_flat_rate = $item_amount_flat_rate;
        $settings->customer_support = $customer_support;
        $settings->delivery_time = $delivery_time;
        $settings->delivery_email = $delivery_email;

    	$settings->save();

    }
}
