<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    protected $fillable = ['product_id', 'size'];

    protected $hidden = ['product_id'];

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }


    public function addProductSize($product_id, $sizes)
    {
        if(!is_null($sizes))
        {

        	foreach ($sizes as $size) {
        		
        		$this::create([

        			'product_id' => $product_id,

        			'size' => $size

        		]);

        	}

            
        }

    	
    }


    public function updateProductSize($product_id, $sizes)
    {

        $this::where('product_id', $product_id)->delete();

    	foreach ($sizes as $size) {
            	
	    	if($this::where('product_id', $product_id)->where('size', $size)->count() == 0)
	
	    	{
	
	    		$this::create([

	    			'product_id' => $product_id,

	    			'size' => $size

	    		]);

	    	}
    
    	}
    
    }
}
