<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    protected $fillable = ['colour_name', 'hex'];

    public function products()
    {
    	return $this->belongsToMany(Product::class);
    }
}
