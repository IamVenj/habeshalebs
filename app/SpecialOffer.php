<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialOffer extends Model
{
   protected $fillable = ['product_id'];
   protected $hidden = ['product_id'];

   public function product()
   {
   	return $this->belongsTo('App\Product');
   }

   public function getLatestOffers()
   {
   	return $this::latest()->with(['product'])->get();
   }

   public function addAsOffer($product_id)
   {
   	$this::create([
   		'product_id' => $product_id
   	]);
   }

   public function destroyOffer($id)
   {
   	$this::find($id)->delete();
   }

}
