<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['address', 'user_id', 'longitude', 'latitude'];

	protected $hidden = ['user_id'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function addAddress($string_address, $array_address, $lat, $lng,  $current_user_id)
	{

    	$address_count = $this->user()->count();

    	if($address_count == 0)
    	{

	    	if(!is_null($string_address))
	    	{

		    	$this::create([

		    		"address" => $string_address,

		    		"user_id" => $current_user_id,

		    		"latitude" => $lat, 

		    		"longitude" => $lng

		    	]);

	    	}
	    	elseif(!is_null($array_address))
	    	{
	    		foreach ($array_address as $key => $value) {
	    			
	    			$this::create([

	    				"address" => $array_address[$key],

	    				"user_id" => $current_user_id,

	    				"latitude" => $lat[$key], 

		    			"longitude" => $lng[$key]

	    			]);

	    		}

	    	}

    	}

	}

	public function updateAddress($address, $latitude, $longitude, $id)
	{
		$_address = $this::find($id);

		$_address->address = $address;

		$_address->latitude = $latitude;

		$_address->longitude = $longitude;

		$_address->save();
	}

	public function destroyAddress($id)
	{
		$this::find($id)->delete();
	}
}
