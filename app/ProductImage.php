<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['product_id', 'image_public_id', 'image_version'];

    protected $hidden = ['product_id'];

    public function products()
    {
    	return $this->belongsTo("App\Products");
    }

    public function addProductImageForASingleProduct($product_id, $image_public_id, $image_version)
    {
    	$this::create([

    		'product_id' => $product_id,

    		'image_public_id' => $image_public_id,

            'image_version' => $image_version

    	]);
    }


    public function updateProductImageForASingleProduct($product_id, $image_public_id, $image_version, $id)
    {
        $productImage = $this::find($id);

        $productImage->product_id = $product_id;

        $productImage->image_public_id = $image_public_id;

        $productImage->image_version = $image_version;

        $productImage->save();
    }

    public function destroySelectedImage($productImage_id)
    {
        $productImage = $this->_product_image::find($productImage_id);

        $productImage->delete();
    }
}
