<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Debt;
use App\User;
use App\Notifications\NotifyMerchantOnOrder;
use App\Notifications\NotifyMerchantOnDebt;
use App\CompanySettings;
use Carbon\Carbon;
use App\Product;

class Order extends Model
{
    protected $fillable = ['user_id', 'product_id', 'order_code', 'payment_status', 'vendor_delivery_confirmation', 'customer_delivery_confirmation', 'quantity', 'color_id', 'paid_amount', 'shipping', 'size_id', 'personal_statement'];
    protected $hidden = ['user_id', 'product_id', 'order_code', 'payment_status'];

    public function createOrder($buyer_user_id, $productId, $orderCode, $paymentStatus, $color, $qty, $vendorUserId, $habesha_lebz_debt, $adminUserId, $paid_amount, $shipping, $sizeId, $personal_statement)
    {
        /*
        |------------------------
        |  -- Create Order  -- 
        |--------------------
        */

        $list = [
        	'user_id' => $buyer_user_id,
    		'product_id' => $productId,
    		'order_code' => $orderCode,
    		'payment_status' => $paymentStatus,
            'color_id' => $color,
            'quantity' => $qty,
            'paid_amount' => $paid_amount,
            'shipping' => $shipping,
            'size_id' => $sizeId, 
            'personal_statement' => $personal_statement
        ];

    	$order = $this::create($list);

        /*
        |------------------------
        |  -- Create Debt  -- 
        |-------------------
        */

        $debt = new Debt();
        $debt->addDebt($vendorUserId, $habesha_lebz_debt, $order->id);
        
        /*
        |------------------------------------------------
        |  -- Send Notification for admin & vendor  -- 
        |-------------------------------------------
        */

        $_order = $this::find($order->id);
        User::find($vendorUserId)->notify(new NotifyMerchantOnOrder($_order));
        User::find($adminUserId)->notify(new NotifyMerchantOnOrder($_order));

    }

    public function updatePaymentStatus($paymentStatus, $id)
    {
    	$order = $this::find($id);
    	$order->payment_status = $paymentStatus;
    	$order->save();
    }


    public function customerDeliveryConfirmation($id, $status)
    {
        $order = $this::find($id);
        $order->customer_delivery_confirmation = $status;
        $order->save();
    }


    public function vendorDeliveryConfirmation($id, $status)
    {
        $order = $this::find($id);
        $order->vendor_delivery_confirmation = $status;
        $order->save();
    }

    public function order_count($userId, $productId, $quantity, $color)
    {
        $product_count = $this::where('user_id', $userId)->where('product_id', $productId)->where('quantity', $quantity)->where('color_id', $color)->count();
        return $product_count;
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function destroyOrder($id)
    {
        $debts = Debt::where('order_id', $id)->get();
        foreach ($debts as $debt) {
        	$debt->delete();
        }
        $this::find($id)->delete();
    }

    public function color()
    {
        return $this->hasMany('App\Color');
    }

    public function productSize()
    {
    	return $this->belongsTo('App\productSize', 'size_id', 'id');
    }


    public function getAllMonthsOrDaysOrYrs($type)
    {
        $date_array = [];

        if ($type == 'd')
        {

                $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                        ->pluck('created_at');

        }
        elseif ($type == 'comparison')
        {

                $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->whereBetween('created_at', [Carbon::now()->startOfMonth()->subMonth(), Carbon::now()->endOfMonth()])
                                        ->pluck('created_at');

        }
        elseif ($type == 'this_yr')
        {
                $orders_dates = $this::whereBetween('created_at', [Carbon::now()->startOfYear(), Carbon::now()->endOfYear()])
                                        ->pluck('created_at');
        }

        elseif ($type == 'last_yr')
        {
                $orders_dates = $this::whereBetween('created_at', [Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()->subYear()])
                                        ->pluck('created_at');
        }

        elseif ($type == 'before_yr')
        {
                $orders_dates = $this::whereNotBetween('created_at', [Carbon::now()->startOfYear()->subYear(), Carbon::now()->endOfYear()])
                                        ->pluck('created_at');
        }
        else
        {
            $orders_dates = $this::with(['product'])
                                        ->whereHas('product', function($query) {
                                            $query->where('user_id', auth()->user()->id);    
                                        })
                                        ->oldest()->pluck('created_at');
        }

        $orders_dates = json_decode($orders_dates);
        
        if(!empty($orders_dates))
        {
      
            foreach ($orders_dates as $unformatted_date) {
      
                $date = new \DateTime($unformatted_date);

                if($type == 'm' || $type == 'comparison' || $type == 'this_yr' || $type == 'last_yr' || $type == 'before_yr') 
                {

                    $month_no = $date->format('m');
                    $month_name = $date->format('M');
                    $date_array[$month_no] = $month_name;

                }

                elseif($type == 'd')
                {

                    $day_no = $date->format('d');
                    $day_name = $date->format('D');
                    $date_array[$day_no] = $day_name;

                }

                elseif($type == 'yr')
                {

                    $year = $date->format('Y');
                    $date_array[$year] = $year;

                }

            }
      
        }

        return $date_array;
    }



    public function getProductPrice($date, $type, $role)
    {
        $yr = date('Y');
        $month = date('m');
        $_settings = new CompanySettings();

        if($type == 'm')
            $orders = $this::whereMonth('created_at', $date)->get();

        if($type == 'yr')
            $orders = $this::whereYear('created_at', $date)->get();

        if($type == 'd' )
            $orders = $this::whereDate('created_at', $yr.'-'.$month.'-'.$date)->get();

        $price = [];

        foreach ($orders as $order) {
            
             if($role == 'admin')
                $product = $order->product()->sum('current_price') * ($_settings::first()->charge/100);

            if($role == 'vendor')
                $product = $order->product()->sum('current_price') * (1 - $_settings::first()->charge/100);
            
            array_push($price, $product);
        
        }

        return array_sum($price);
    
    }


}
