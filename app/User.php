<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Address;

use App\Delivery;
use Laravel\Passport\HasApiTokens;

use App\Notifications\NotifyUserRegistration;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
    seller == 2
    admin == 1
    customer == 3
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'role', 'shop_name', 'phone_number', 'about_shop', 'photo_url','image_version', 'company_name', 'country', 'address', 'apartment', 'city', 'state', 'postcode', 'activation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class, 'user_id', 'id');
    }

    public function wish()
    {
        return $this->hasMany('App\WishList');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function createAccount($firstname, $lastname, $shopname, $email, $password)
    {
        if(is_null($firstname) && is_null($lastname))
        {

            $this::create([

                'email' => $email,

                'password' => $password,

                'role' => 2,

                'activation' => 1,

                'shop_name' => $shopname

            ]);

        }

        else
        {

            $this::create([

                'email' => $email,

                'password' => $password,

                'role' => 3,

                'firstname' => $firstname,

                'lastname' => $lastname

            ]);

        }

    }

    public function updateAccount($image_public_id, $phone_number, $shop_name, $about_shop, $current_user_id, $current_user_role, $image_version)
    {
        $current_user = $this::find($current_user_id);

   
        $current_user->photo_url = $image_public_id;

        $current_user->phone_number = $phone_number;

        $current_user->shop_name = $shop_name;

        $current_user->about_shop = $about_shop;

        $current_user->image_version = $image_version;

        $current_user->save();

        
        $this::find($this->getAdmin()->id)->notify(new NotifyUserRegistration($current_user));

    }

    public function getAdmin()
    {
        $admin = $this::where('role', 1)->first();

        return $admin;
    }

    public function updateCustomer($firstname, $lastname, $email, $phone_number, $id)
    {
        
        $user = $this::find($id);

        $user->firstname = $firstname;

        $user->lastname = $lastname;

        $user->email = $email;

        $user->phone_number = $phone_number;

        $user->save();

    }

    public function deletePreviousPhotoURL($id)
    {

        $profile_img = $this::find($id);

        if(!is_null($profile_img->photo_url))
        {
            \Cloudder::destroy($profile_img->photo_url);
        }

    }

    public function updateProfilePicture($image, $id)
    {

        $image_name = $image->getRealPath();
        
        $image_result = \Cloudinary\Uploader::upload($image_name, array("folder"=>"profile"));

        $this->deletePreviousPhotoURL($id);

        $user = $this::find($id);

        $user->photo_url = $image_result['public_id'];

        $user->image_version = $image_result['version'];

        $user->save();

    }

    public function checkIfCurrentPasswordIsCorrect($current_password)
    {
        if(\Hash::check($current_password, auth()->user()->password))
        {
            return true;
        }
        return false;
    }

    public function changePassword($new_password)
    {
        $user = $this::find(auth()->user()->id);

        $user->password = bcrypt($new_password);

        $user->save();
    }

    public function updateSeller($currentUserId, $shop_name, $about_shop, $phone_number, $email)
    {
        $user = $this::find($currentUserId);

        $user->shop_name = $shop_name;

        $user->about_shop = $about_shop;

        $user->phone_number = $phone_number;

        $user->email = $email;

        $user->save();
    }

    public function updateAdmin($currentUserId, $firstname, $email)
    {
        $user = $this::find($currentUserId);

        $user->firstname = $firstname;

        $user->email = $email;

        $user->save();
    }

    public function updateDeliveryStatus($status)
    {
        $user = $this::find(auth()->user()->id);

        $user->delivery = $status;

        $user->save();

    }

    public function isConnected()
    {
        $connected = @fsockopen("www.google.com", 80);

        if($connected)
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function role_vendor()
    {
        return $this::where('role', 2);
    }

    public function role_customer()
    {
        return $this::where('role', 3);
    }

    public function getVendors()
    {
        return $this::where('role', 2)->whereNotNull('shop_name')->get();
    }

    public function saveUserInformationAfterBilling($firstname, $lastname, $country, $address, $state, $city, $phone, $company_name, $apartment, $postalcode)
    {
        $user = $this::find(auth()->user()->id);

        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->country = $country;
        $user->address = $address;
        $user->state = $state;
        $user->city = $city;
        $user->phone_number = $phone;
        $user->company_name = $company_name;
        $user->apartment = $apartment;
        $user->postcode = $postalcode;

        $user->save();
    }

    public function activate($id)
    {
        $user = $this::find($id);
        $user->activation = 0;
        $user->save();
    }

}
