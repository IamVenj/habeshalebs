<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Category extends Model
{
    protected $fillable = ['category_name', 'image_public_id', 'image_url', 'image_version'];

    public function products()
    {
    	return $this->hasMany(Product::class);
    }

    public function parent()
    {
    	return $this->hasMany("App\Category", "parent_id", "id");
    }

    public function category()
    {
    	return $this->belongsTo("App\Category", "id", "parent_id");
    }

    public function createCategory($category_name, $image, $image_url, $image_version)
    {

    	$this::create([

    		'category_name' => $category_name,

    		'image_public_id' => $image,

    		'image_url' => $image_url,

            'image_version' => $image_version

    	]);

    }

    public function updateCategory($category_name, $parent_id, $category_id)
    {
        $category = $this::find($category_id);

        $category->category_name = $category_name;

        $category->parent_id = $parent_id;

        $category->save();
    }

    public function currentImage($id)
    {
        $category = $this::find($id);

        return $category->image_public_id;

    }

    public function updateCategoryImage($id, $image_public_id, $image_url, $image_version)
    {
        $categorImage = $this::find($id);

        $categorImage->image_public_id = $image_public_id;

        $categoryImage->image_url = $image_url;

        $categoryImage->image_version = $image_version;

        $categoryImage->save();
    }

    public function destroyCategory($id)
    {
        $category = $this::find($id);

        $category->delete();
    }
}
