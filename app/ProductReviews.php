<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReviews extends Model
{
    protected $fillable = ['review_id', 'product_id'];
    
	protected $hidden = ['review_id', 'product_id'];

	public function reviews()
	{
		return $this->hasMany('App\Review');
	}

	public function products()
	{
		return $this->hasMany('App\Product');
	}
}
