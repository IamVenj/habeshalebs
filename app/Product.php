<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CompanySettings;

class Product extends Model
{
    protected $fillable = ['product_name', 'slug', 'visits', 'category_id', 'brand', 'current_price', 'product_location', 'availability', 'old_price', 'user_id', 'sku', 'orders', 'weight', 'washcare', 'composition', 'lining_composition', 'styling'];
    
    protected $hidden = ['user_id', 'category_id'];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function productColors()
    {
    	return $this->hasMany("App\ProductColor");
    }

    public function productImage()
    {
    	return $this->hasMany('App\ProductImage');
    }

    public function productSize()
    {
        return $this->hasMany('App\ProductSize');
    }

    public function Review()
    {
        return $this->hasMany('App\Review');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function updateProduct($product_name, $slug, $brand, $current_price, $category, $product_location, $old_price, $availability, $id, $sku)
    {

        $product = $this::find($id);

        $product->product_name = $product_name;
        $product->slug = $slug;
        $product->category_id = $category;
        $product->brand = $brand;
        $product->current_price = $current_price;
        $product->old_price = $old_price;
        $product->product_location = $product_location;
        $product->availability = $availability;
        $product->user_id = auth()->user()->id;
        $product->sku = $sku;

        $product->save();

    }

    public function destroyProduct($id)
    {
        $this::find($id)->delete();
    }

    public function updateAvailableItems($productId, $qty)
    {
        $product = $this::find($productId);

        $product->availability = $product->availability - $qty;
        $product->orders = $product->orders + $qty;

        $product->save();
    }

    public function getProductsOfThisUser($vendor_id)
    {
        return $this::where('user_id', $vendor_id)->get();
    }

    public function visits($id) {
        $product = $this::find($id);
        $product->visits = $product->visits + 1;
        $product->save();
    }

    public function cartItemsPrice() {
        $qty = \Cart::content()->pluck('qty')->toArray();
        $setting = CompanySettings::first();
        $total_price = \Cart::subtotal();

        $filtered_qty = array_filter($qty, function($value) use ($setting) {
            return $value >= $setting->item_amount_flat_rate;
        });

        if($filtered_qty == true)
            $flat_rate = (max($qty) / $setting->item_amount_flat_rate) * $setting->delivery_price;
        else
            $flat_rate = $setting->delivery_price;

        return $total_price_main = $total_price + $flat_rate;
    }

    public function cartItemFlatRate() {
        $qty = \Cart::content()->pluck('qty')->toArray();
        $setting = CompanySettings::first();
        $total_price = \Cart::subtotal();

        $filtered_qty = array_filter($qty, function($value) use ($setting) {
            return $value >= $setting->item_amount_flat_rate;
        });

        if($filtered_qty == true)
            $flat_rate = (max($qty) / $setting->item_amount_flat_rate) * $setting->delivery_price;
        else
            $flat_rate = $setting->delivery_price;

        return $flat_rate;
    }

}
