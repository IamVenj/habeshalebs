<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\NewsletterTheLetter;

use App\CompanySettings;

class NewsLetter extends Mailable
{
    use Queueable, SerializesModels;

    public $_newsletterTheLetter, $_settings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(NewsletterTheLetter $_newsletterTheLetter,CompanySettings $_settings)
    {
        $this->_newsletterTheLetter = $_newsletterTheLetter;

        $this->_settings = $_settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('post-login.pages.newsletter.email.index')->with('_settings', '_newsletterTheLetter');
    }
}
