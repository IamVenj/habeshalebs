<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotifyMerchantOnDebt;

class Debt extends Model
{
    protected $fillable = ['vendor_id', 'order_id', 'debt', 'debt_status'];

    protected $hidden = ['vendor_id', 'order_id'];

    public function vendor()
    {
        return $this->belongsTo('App\User', 'vendor_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function addDebt($vendor_id, $debt, $order)
    {
        /*
        |------------------------
        |  -- Create Debt  -- 
        |--------------------
        */

        $debt = $this::create([

            'vendor_id' => $vendor_id,
            'debt' => $debt,
            'order_id' => $order,
            'debt_status' => 0

        ]);

        /*
        |---------------------------------
        |  -- Notify Debt to vendors -- 
        |---------------------------
        */
/*
        $debt_sum = $this::where('vendor_id', $vendor_id)->where('debt_status', 0)->sum('debt');
        User::find($vendor_id)->notify(new NotifyMerchantOnDebt($debt_sum, $debt->created_at->diffForHumans()));*/

    }

    public function changeDebtStatus($id, $debtStatus)
    {
        $debtuser = $this::where('vendor_id', $id)->where('debt_status', 0)->get();
        foreach ($debtuser as $debt) 
        {
            
            $debt->debt_status = $debtStatus;
            $debt->save();

        }

    }

    public function changeDebtStatusForEach($debtStatus, $debt_id)
    {
        $debt = $this::find($debt_id);
        $debt->debt_status = $debtStatus;
        $debt->save();
    }

    /*
    |----------------------------------------------------------------
    | In case they want to remove the debt 
    |----------------------------------------------------------------
    */

    public function destroyDebt($id)
    {
        $this::find($id)->delete();
    }
}
