$(document).ready(function(){

	$('#color_length').on('change', function(){
		
		var inputs = $(this).val();

		$('.color-loop').remove();

		for (var i = 0; i < inputs; i++) {

			$('#InputAreaForColors').append('<div class="color-loop"><div class="form-group"><label>Color '+i+'   '+'</label><select class="form-control form-control-lg" name="color[]"><option selected disabled>Choose Color</option>@foreach($all_colors as $colors)<option style="font-family: Montserrat; background:{{$colors->hex}};">{{$colors->colour_name}}</option>@endforeach</select></div></div>');
		 
		}

	}).change();

});