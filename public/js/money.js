(function($) {
  "use strict"

  var slider = document.getElementById('price-slider');
  if (slider) {
    var xl = noUiSlider.create(slider, {
      start: [1, 50000],
      connect: true,
      tooltips: [true, true],
      format: {
        to: function(value) {
          return value.toFixed(0) + ' Birr';
        },
        from: function(value) {
          return value
        }
      },
      range: {
        'min': 1,
        'max': 50000
      }
    });

    var price_range = [document.getElementById("min-price"), document.getElementById("max-price")];
    xl.on('update', function(values) {
      price_range[0].value = values[0];
      price_range[1].value = values[1];
    });
  }

})(jQuery)
