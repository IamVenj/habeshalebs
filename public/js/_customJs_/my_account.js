var delivery = $('#material-delivery');

var append_delivery = $('#delivery');

var price_per_km = $('input[name=price_per_km]').val();

delivery.change(function(){

    var checked = $(this).is(":checked");

    var appended_delivery = "";

    if(checked == true)
    {
      appended_delivery += "<div class='form-group'>";
          
      appended_delivery += "<label for='price'>Price per Kilometer</label>";
          
      appended_delivery += "<input id='price' name='price' type='text' class='required form-control' value='"+price_per_km+"' placeholder='Fill in the price in birr per Kilometer'>";
          
      appended_delivery += "</div>";

      append_delivery.html(appended_delivery);
    }
    else
    {
      append_delivery.html("");
    }

  }).change();


$(document).ready(function() {

		var image_select = $(".image");

		image_select.change(function(){

    if (this.files && this.files[0]) {

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.picture-src').attr('src', e.target.result).fadeIn('slow');

      }

      reader.readAsDataURL(this.files[0]);

    }
  
  });

});