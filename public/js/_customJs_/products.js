// ---------------- //
// Global Variables //
// ---------------- //

var error_images = "";

var img = [];

var form = $("#productsForm");

var error_array = [];

var token = $("#csrf-token");

// var status = $('#status');


 

// --------------- //
// Form Submission //
// --------------- //

form.submit(function(e){

  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': token.attr('content')

    }

  });



  if(img.length == 0)
  {
    error_images += "Image is required!"; 
  }   
  else if(error_array.length != 0)
  {
    if(jQuery.inArray(error_array, ['png', 'jpg', 'jpeg']) == -1)
    {
      error_images += "Only png, jpg or jpeg files are allowed! ";
    }
  }
  else
  {
    error_images = "";
  }



  e.preventDefault();


  if(error_images == "")
  {

   document.getElementById("progress").style.display = 'block';

    $("#error_multiple_files").html("");

    $.ajax({

      type: form.attr("method"),

      url: form.attr("action"),

      data: new FormData(this),

      contentType: false,

      processData: false,

      cache: false,

      // timeout: 10000,

      beforeSend: function(){

        $(".btn-product-create").attr("disabled", "disabled");

        $("#pending_status").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");
      
        $('.progress-bar').css('width', '0%');  
      
      },

      uploadProgress:function(event, position, total, percentComplete) {
              
        $('.progress-bar').css('width', percentComplete + '%');
      
      },

      success: function(response){

        $("#pending_status").html("");

        $(".btn-product-create").removeAttr("disabled", "disabled");

        var message = $('.message').fadeIn('fast');


        if(response.status == 1)
        {
          
          $('.progress-bar').css('width', '100%');

          $('.progress-bar').text('Upload 100%');


          var successMessage = '<div class="form-group mt-4">';

                successMessage += '<div class="alert golden2">';

                successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                
                successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                + response.message + '</p>';

                  successMessage +=  '</div></div>';

            $(message).html(successMessage).delay(4500).fadeOut('slow');

            location.replace('/create-product');

        }

        else if(response.status == 0)
        {

          var errorMessage = '<div class="form-group mt-4">';

              errorMessage += '<div class="alert golden">';

              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
              
              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

              + response.message + '</p>';
            
              errorMessage +=  '</div></div>';

              $(message).html(errorMessage).delay(4500).fadeOut('slow');

        }

      },

      error: function(error){

        console.log(error.responseJSON);

        $("#pending_status").html("");

        $(".btn-product-create").removeAttr("disabled", "disabled");

        var message = $('.message').fadeIn('fast');
        
        if(error.responseJSON['errors'] != null)
        {
                
          var errorMessage = '<div class="form-group mt-4">';

              errorMessage += '<div class="alert golden">';

              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

              $.each(error.responseJSON['errors'], function(key, errors)
              {
              
              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

              + errors + '</p>';

              });

              errorMessage +=  '</div></div>';


              $(message).html(errorMessage).delay(4500).fadeOut('slow');
                
                
        }

        else if(error.responseJSON['exception'] == "Symfony\\Component\\Debug\\Exception\\FatalErrorException")
        {
          var errorMessage = '<div class="form-group mt-4">';

              errorMessage += '<div class="alert golden">';

              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
              
              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

              + error.responseJSON['message'] + '! Please try again</p>';

              errorMessage +=  '</div></div>';


              $(message).html(errorMessage).delay(4500).fadeOut('slow');
        }

        else
        {

          $(message).html('').fadeIn('slow');

        }

      }

    });

  }
  else
  {
    $("#image").val('');
  
    $("#error_multiple_files").html("<span class='text-danger'>"+error_images+"</span><br/><br/>");
  }

});

$(".btn-select-colors-ok").click(function(){

  var all_vals = [];

  var check = $("#multiple_colors");

  $("input[name='colors[]']:checked").each(function(){
     
      all_vals.push($(this).val());
 
  });

  $("#selected-colors").html(all_vals.length+" colors selected");

});