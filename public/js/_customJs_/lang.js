function change_language(value)
{
	var token = $("meta[name=csrf-token]").attr('content');

	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/lang',

		type: 'post',

		data: {lang: value},

		success: function(response)
		{
			location.replace(response.url);
		},

		error: function(err)
		{
			console.log(err);
		}

	});

}