function Newsletter(form) {
    var token = $("meta[name='csrf-token']").attr('content');
    var email = form.email.value;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
    $.ajax({
        url: form.action,
        type: form.method,
        data: {email:email},
        beforeSend: function() {
            $(".btn-subscribe").attr("disabled", "disabled");
        },
        success: function(response) {
            $(".btn-subscribe").removeAttr("disabled", "disabled");
            var message = $('.message').fadeIn('fast');

            var successMessage = '<div class="form-group mt-4">';
                successMessage += '<div class="alert golden2">';
                successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                successMessage += '<p class="mr-5 ml-5" style="font-family: "Montserrat", sans-serif; color: #fff; font-size: 16px; cursor: default;">'+ response.message + '</p>';
                successMessage +=  '</div></div>';

                $(message).html(successMessage).delay(2500).fadeOut('slow');

            form.email.value = "";
            
        },
        error: function(error) {
            $(".btn-subscribe").removeAttr("disabled", "disabled");
            var message = $('.message').fadeIn('fast');
            if(error.responseJSON['errors'] != null)
            {
                  
                var errorMessage = '<div class="form-group mt-4">';
                    errorMessage += '<div class="alert golden">';
                    errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                    $.each(error.responseJSON['errors'], function(key, errors){
                        errorMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ errors + '</p>';
                    });
                    
                    errorMessage +=  '</div></div>';
                    $(message).html(errorMessage).delay(4500).fadeOut('slow');      
            }
            else
            {
                $(message).html('').fadeIn('slow');
            }
        }
    })
}


    //Contact Form Validation
    if($('#contact-form').length){
        $('#contact-form').validate({
            rules: {
                fullname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                }
            },
            submitHandler: function(form, e){
                e.preventDefault();
                var token = $("meta[name=csrf-token]").attr("content");
                var form = $("#contact-form");
                var message = $("#message-contact").val();
                var email = $("input[name=contact-email]").val();
                var fullname = $("input[name=fullname]").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': token
                    }
                });

                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: {fullname: fullname, email: email, message: message},
                    beforeSend: function(){
                        $("#send_contact").attr('disabled', "disabled");
                    },
                    success: function(response){
                        $("#send_contact").removeAttr('disabled', "disabled");

                        if(response.status == 'success')
                        {
                            var message = $('.message').fadeIn('fast');

                           var successMessage = '<div class="form-group mt-4">';
                            successMessage += '<div class="alert golden2">';
                            successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                            successMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ response.message + '</p>';
                            successMessage +=  '</div></div>';

                            $(message).html(successMessage).delay(4500).fadeOut('slow');

                            $("#message-contact").val("");
                            $("input[name=contact-email]").val("");
                            $("input[name=fullname]").val("");
                        }
                        else if(response.status == "error")
                        {
                            var message = $('.message').fadeIn('fast');

                            var errorMessage = '<div class="form-group mt-4">';
                            errorMessage += '<div class="alert golden">';
                            errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
                            errorMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ response.message + '</p>';
                            errorMessage +=  '</div></div>';
                            
                            $(message).html(errorMessage).delay(4500).fadeOut('slow');
                        }
                    },
                    
                    error: function(error){
                        $("#send_contact").removeAttr('disabled', "disabled");

                        var message = $('.message').fadeIn('fast');
                        if(error.responseJSON['errors'] != null)
                        {
                            
                            var errorMessage = '<div class="form-group mt-4">';
                            errorMessage += '<div class="alert golden">';
                            errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                            $.each(error.responseJSON['errors'], function(key, errors){
                                errorMessage += '<p class="mr-5 ml-5" style="color: #fff;">'+ errors + '</p>';
                            });
                            
                            errorMessage +=  '</div></div>';
                            $(message).html(errorMessage).delay(4500).fadeOut('slow');                   
                        }
                        else
                        {
                            $(message).html('').fadeIn('slow');
                        }

                    }
                });
            }
        });
    }