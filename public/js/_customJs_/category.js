load_table();

var token = $("input[name=csrf-token]");


function load_table(token)
{

  var token = $("input[name=csrf-token]");

  $.ajaxSetup({

    headers: {
  
      'X-CSRF-TOKEN': token.val()
  
    }

  });

  $.ajax({
    
    url: '/get-categories',

    method: "POST",

    success: function(response) {

      $("#dtable").html(response);


      $('.myDataTable').DataTable().destroy();
     
      $('.myDataTable').DataTable({

        "bDestroy": true

      });

      

      // $("").dataTable({

      //   "bDestroy": true
      
      // });

    },
    error: function(error) {
    
      // console.log(error.responseJSON);
    
    }

  });

}


var form1 = $("#categoryForm");

form1.submit(function(e){

  $.ajaxSetup({

    headers: {
  
      'X-CSRF-TOKEN': token.val()
  
    }

  });

  e.preventDefault();

  $.ajax({

    url: form1.attr("action"),

    type: form1.attr("method"),

    data: new FormData(this),

    contentType: false,

    processData: false,

    cache: false,

    beforeSend: function(){

      $(".button-create-category").attr("disabled", "disabled");

      $("#pending_status").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

    },

    success: function(response){


      $(".button-create-category").removeAttr("disabled", "disabled");

      $("#pending_status").html("");

      

      var message = $('.message').fadeIn('fast');

      if(response.status == 1)
      {
        var successMessage = '<div class="form-group mt-4">';

            successMessage += '<div class="alert golden2">';

            successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
            
            successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

            + response.message + '</p>';

              successMessage +=  '</div></div>';

        $(message).html(successMessage).delay(2500).fadeOut('slow');

        // ---------------------------------- //

        $("input[name=categoryName]").val("");

        $("input[name=mainCategory]").removeAttr("selected");
        
        document.getElementById("image-category-upload").value = "";

        document.getElementById("file-upload-info").value = "";

        // ---------------------------------- //


        load_table();

        location.replace('/category');


      }
      else if(response.status == 0)
      {
        
        var errorMessage = '<div class="form-group mt-4">';

              errorMessage += '<div class="alert golden">';

              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

              
              
              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

              + response.message + '</p>';


              errorMessage +=  '</div></div>';


              $(message).html(errorMessage).delay(4500).fadeOut('slow'); 
      }

      // console.log(response);

    },

    error: function(error){

      // console.log(error.responseJSON);

      $("#pending_status").html("");

      $(".button-create-category").removeAttr("disabled", "disabled");

      var message = $('.message').fadeIn('fast');
      
      if(error.responseJSON['errors'] != null)
      {
              
        var errorMessage = '<div class="form-group mt-4">';

            errorMessage += '<div class="alert golden">';

            errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

            $.each(error.responseJSON['errors'], function(key, errors)
            {
            
            errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

            + errors + '</p>';

            });

            errorMessage +=  '</div></div>';


            $(message).html(errorMessage).delay(4500).fadeOut('slow');
              
              
      }

      else
      {

        $(message).html('').fadeIn('slow');

      }

    }

  });

});


// --------------------------------------------------- //
// EDIT CATEGORY FORM SUBMISSION
// --------------------------------------------------- //


var edit_category_form = $("#edit_category_form");

var csrf_edit_token = $("input[name=csrf-edit-token]");

edit_category_form.submit(function(e){


  console.log(edit_category_form.attr('action'));

    $.ajaxSetup({

      headers: {
    
        'X-CSRF-TOKEN': csrf_edit_token.val()
    
      }

    });

    e.preventDefault();

    var category_id = $("input[name=category_id]");

    var mainCategory = $("input[name=mainCategory]");

    var categoryName = $("input[name=categoryName]");

    $.ajax({

        url: edit_category_form.attr("action"),

        type: edit_category_form.attr("method"),

        data: new FormData(this),

        contentType: false,

        processData: false,

        cache: false,

        beforeSend: function(){

            $(".button-edit-category").attr("disabled", "disabled");

            $("#pending_status_edit").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

        },

        success: function(response) {

            $("#pending_status_edit").html("");

            $(".button-edit-category").removeAttr("disabled", "disabled");

            $("#categoryModal").modal("hide");

            load_table();

            var message = $('.message').fadeIn('fast');

            if(response.status == 1)
            {
              var successMessage = '<div class="form-group mt-4">';

                  successMessage += '<div class="alert golden2">';

                  successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                  
                  successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                  + response.message + '</p>';

                    successMessage +=  '</div></div>';

              $(message).html(successMessage).delay(2500).fadeOut('slow');


              load_table();


            }
            else if(response.status == 0)
            {
              
              var errorMessage = '<div class="form-group mt-4">';

                    errorMessage += '<div class="alert golden">';

                    errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                    
                    
                    errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                    + response.message + '</p>';


                    errorMessage +=  '</div></div>';


                    $(message).html(errorMessage).delay(4500).fadeOut('slow'); 

            }

        },

        error: function(error) {

            // console.log(error.responseJSON);

        }


    });


});

$(document).on('click', '.edit_category', function(){

  // console.log('clicked');

  var category_id = $(this).attr('id');

  $.ajaxSetup({

    headers: {
  
      'X-CSRF-TOKEN': token.val()
  
    }

  });

  $.ajax({

    url: "/show-categories-edit-modal",

    method: "POST",

    data: {category_id: category_id},

    dataType: "json",

    success: function(response) {

      $("#categoryModal").modal("show");

      $("#category_id").val(category_id);

      $("#category_name").val(response.category_name);

      $("#mainCategory").val(response.parent);

    }

  });

});


// --------------------------------------------------- //
// EDIT CATEGORY IMAGE FORM SUBMISSION
// --------------------------------------------------- //


$(document).on('click', '.edit_category_image', function(){

  var category_id = $(this).attr('id');

  $.ajaxSetup({

    headers: {
  
      'X-CSRF-TOKEN': token.val()
  
    }

  });

  $.ajax({

    url: "/show-categories-edit-image-modal",

    method: "POST",

    data: {category_id: category_id},

    dataType: "json",

    success: function(response) {

      $("#categoryImageModal").modal("show");

      $("#category_id").val(category_id);

      $("#current_image").attr('src', response.image_url);

    }

  });

});

var edit_image_category_form = $("#edit_image_category_form");

var csrf_edit_image_token = $("input[name=csrf-edit-image-token]");

edit_image_category_form.submit(function(e){


    $.ajaxSetup({

      headers: {
    
        'X-CSRF-TOKEN': csrf_edit_image_token.val()
    
      }

    });

    e.preventDefault();

    $.ajax({

        url: edit_image_category_form.attr("action"),

        type: edit_image_category_form.attr("method"),

        data: new FormData(this),

        contentType: false,

        processData: false,

        cache: false,

        beforeSend: function(){

            $(".button-edit-image-category").attr("disabled", "disabled");

            $("#pending_status_image").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

        },

        success: function(response) {

            $("#pending_status_image").html("");

            $(".button-edit-image-category").removeAttr("disabled", "disabled");

            $("#categoryImageModal").modal("hide");

            load_table();

            var message = $('.message').fadeIn('fast');

            if(response.status == 1)
            {
              var successMessage = '<div class="form-group mt-4">';

                  successMessage += '<div class="alert golden2">';

                  successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                  
                  successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                  + response.message + '</p>';

                    successMessage +=  '</div></div>';

              $(message).html(successMessage).delay(2500).fadeOut('slow');


              load_table();


            }
            else if(response.status == 0)
            {
              
              var errorMessage = '<div class="form-group mt-4">';

                    errorMessage += '<div class="alert golden">';

                    errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                    
                    
                    errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                    + response.message + '</p>';


                    errorMessage +=  '</div></div>';


                    $(message).html(errorMessage).delay(4500).fadeOut('slow'); 

            }

        },

        error: function(error) {

            $("#pending_status_image").html("");

            $(".button-edit-image-category").removeAttr("disabled", "disabled");

            // console.log(error.responseJSON);

        }


    });


});


// --------------------------------------------------- //
// DELETE CATEGORY IMAGE FORM SUBMISSION
// --------------------------------------------------- //


$(document).on('click', '.delete_category', function(){

  var category_id = $(this).attr('id');

  $("#categoryDeleteModal").modal("show");

  $("#category_id").val(category_id);

});


var delete_category_form = $("#delete_category_form");

var csrf_delete_token = $("input[name=csrf-delete-token]");

delete_category_form.submit(function(e){


    $.ajaxSetup({

      headers: {
    
        'X-CSRF-TOKEN': csrf_delete_token.val()
    
      }

    });

    e.preventDefault();

    $.ajax({

        url: delete_category_form.attr("action"),

        type: delete_category_form.attr("method"),

        data: new FormData(this),

        beforeSend: function(){

            $(".button-delete-category").attr("disabled", "disabled");

            $("#pending_status_delete").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

        },

        success: function(response) {

            $("#pending_status_delete").html("");

            $(".button-delete-category").removeAttr("disabled", "disabled");

            $("#categoryDeleteModal").modal("hide");

            load_table();

            var message = $('.message').fadeIn('fast');

            if(response.status == 1)
            {
              var successMessage = '<div class="form-group mt-4">';

                  successMessage += '<div class="alert golden2">';

                  successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                  
                  successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                  + response.message + '</p>';

                    successMessage +=  '</div></div>';

              $(message).html(successMessage).delay(2500).fadeOut('slow');


              load_table();


            }
            else if(response.status == 0)
            {
              
              var errorMessage = '<div class="form-group mt-4">';

                    errorMessage += '<div class="alert golden">';

                    errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                    
                    
                    errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                    + response.message + '</p>';


                    errorMessage +=  '</div></div>';


                    $(message).html(errorMessage).delay(4500).fadeOut('slow'); 

            }

        },

        error: function(error) {

            $("#pending_status_delete").html("");

            $(".button-delete-category").removeAttr("disabled", "disabled");

            // console.log(error.responseJSON);

        }


    });


});