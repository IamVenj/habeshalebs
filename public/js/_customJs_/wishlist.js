function add_wish(i)
{
	var token = $("meta[name=csrf-token]").attr('content');

	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/add-wish',

		type: 'POST',

		data: {product_id: i},

		beforeSend: function() {



		},

		success: function(response) {

			swal(response.productName, "is added to wishlist !", "success");

			$(".heart-comp").addClass('text-danger');

		},

		error: function(error) {

			if(error.responseJSON['message'] == 'Unauthenticated.')
			{
				location.replace('/login');
			}

		},

	});


}