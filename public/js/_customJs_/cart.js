// $(document).ready(function() {

get_cart();

function get_cart() {

	var token = $("meta[name=csrf-token]").attr("content");

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/get-cart',

		type: 'get',

		data: {},

		success: function(response) {

			$("#cartItem").html(response.output);

			$("#total-cart").html(response.total_price);

			$(".cart-total-price").html('$'+response.total_price);

			$(".cartCount").html(response.cart_count);

			$("#total_price_main").html('$'+response.total_price_main);

			$(".flat_rate").html(response.flat_rate);

		},

		error: function(error) {

			// console.log(error);

        }

	});
}


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------


var cart_form = $(".cart-form");

cart_form.submit(function(e){

	var token = $("meta[name=csrf-token]").attr("content");

	var product_id = $("input[name=cart-product-id]").val();

	var product_name = $("input[name=product_name]").val();

	var product_price = $("input[name=product_price]").val();

	var product_weight = $("input[name=product_weight]").val();

	var product_availability = $("input[name=product_availability]").val();

	var product_quantity = $("#qty").val();

	var product_color = $("input[name=color]:checked").val();

	var product_size = $("input[name=size]:checked").val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	e.preventDefault();

	$.ajax({

		url: cart_form.attr('action'),

		type: cart_form.attr('method'),

		data: {product_id: product_id, product_name: product_name, product_price: product_price, product_weight: product_weight, product_color: product_color, product_size: product_size, product_quantity: product_quantity, product_availability: product_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");

		},

		success: function(response) {

			$("#add_cart").removeAttr("disabled", "disabled");

			get_cart();

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';

	              successMessage += '<div class="alert golden">';

	              successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              
	              
	              successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + response.message + '</p>';

	              successMessage +=  '</div></div>';


	              $(message).html(successMessage).delay(4500).fadeOut('slow');
		        
			}
			else if(response.status == 1)
			{
				swal(response.product_name,"is added to cart !", "success");
			}
			
		},

		error: function(error) {

			console.log(error);

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});

});


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------

function cart_submit(i){

	var token = $("meta[name=csrf-token]").attr("content");

	var product_id = $("input[name=cart-product-id"+i+"]").val();

	var product_name = $("input[name=product_name"+i+"]").val();

	var product_price = $("input[name=product_price"+i+"]").val();

	var product_availability = $("input[name=product_availability"+i+"]").val();

	var product_quantity = $("#qty"+i).val();

	var product_color = $("input[name=color"+i+"]:checked").val();

	var product_size = $("input[name=size"+i+"]:checked").val();

	console.log(product_price);

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/cart',

		type: 'pOST',

		data: {product_id: product_id, product_name: product_name, product_price: product_price, product_color: product_color, product_size: product_size, product_quantity: product_quantity, product_availability: product_availability},

		beforeSend: function() {

			$("#add_cart").attr("disabled", "disabled");

		},

		success: function(response) {

			$("#add_cart").removeAttr("disabled", "disabled");

			$("#cart_count").html(response.cart_count);

			var message = $('.message').fadeIn('fast');
		
			if(response.status == 0)
			{
				var message = $('.message').fadeIn('fast');			       
	                
	          	var successMessage = '<div class="form-group mt-4">';

	              successMessage += '<div class="alert golden">';

	              successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              
	              
	              successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + response.message + '</p>';

	              successMessage +=  '</div></div>';


	              $(message).html(successMessage).delay(4500).fadeOut('slow');
		        
			}
			else if(response.status == 1)
			{
				$(message).html("");

				get_cart();

				swal(response.product_name,"is added to cart !", "success");
			}
			
		},

		error: function(error) {

			console.log(error);

			$("#add_cart").removeAttr("disabled", "disabled");

	        var message = $('.message').fadeIn('fast');
	        
	        if(error.responseJSON['errors'] != null)
	        {
	                
	          var errorMessage = '<div class="form-group mt-4">';

	              errorMessage += '<div class="alert golden">';

	              errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

	              $.each(error.responseJSON['errors'], function(key, errors)
	              {
	              
	              errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

	              + errors + '</p>';

	              });

	              errorMessage +=  '</div></div>';


	              $(message).html(errorMessage).delay(4500).fadeOut('slow');
	                
	                
	        }

	        else
	        {

	          $(message).html('').fadeIn('slow');

	        }
		}

	});

};



//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------

function removeFromCart2(i)
{
	var token = $("meta[name=csrf-token]").attr("content");

	var rowId = $("#rowId"+i).val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/remove-cartitem',

		type: 'POST',

		data: {rowId: rowId},

		beforeSend: function() {

		},

		success: function(response) {

			get_cart();

			location.replace('/cart');

			swal(response.message, '','success');

		},

		error: function(error) {

			console.log(error);

		}

	});
}

function removeFromCart(i)
{
	var token = $("meta[name=csrf-token]").attr("content");

	var rowId = $("#rowId"+i).val();

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/remove-cartitem',

		type: 'POST',

		data: {rowId: rowId},

		beforeSend: function() {

		},

		success: function(response) {

			get_cart();

			// location.replace(response.back);

			swal(response.message, '','success');

		},

		error: function(error) {

			console.log(error);

		}

	});
}


//  --------------------------------------------------------------------------------------------------
//
//  --------------------------------------------------------------------------------------------------


function update_cart(i)
{
	var token = $("meta[name=csrf-token]").attr("content");

	var rowId = $("#Item-rowId"+i).val();

	console.log(rowId);

	var qty = $("#qty"+i).val();

	var current_price = $("#current_price"+i).val();
	

	$.ajaxSetup({

		headers:{

			'X-CSRF-TOKEN': token

		}

	});

	$.ajax({

		url: '/update-cart',

		type: 'POST',

		data: {rowId: rowId, qty: qty},

		beforeSend: function() {

		},

		success: function(response) {

			get_cart();

			$(".total-price-of-one-item"+i).html('$'+ qty * current_price);

		},

		error: function(error) {

			console.log(error);

		}

	});
}