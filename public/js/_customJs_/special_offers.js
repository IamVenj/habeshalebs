function getVendors()
{

	var token = $("meta[name=csrf-token]").attr('content');

	var vendor_id = $("#vendor").val();

	$.ajaxSetup({

		headers: {
			'X-CSRF-TOKEN': token
		}

	});

	$.ajax({

		url: '/get-vendor-special-offer',
		
		type: 'POST',
		
		data: {id: vendor_id},
		
		success: function(response) {
			$("#products").html(response.output);
		},
		
		error: function(error) {
			console.log(error);
		}


	});

}