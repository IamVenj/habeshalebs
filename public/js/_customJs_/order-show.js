map();

  function map()
  {
    var address_count = $("input[name=add_count]").val();

    for (var i = 1; i < address_count; i++) {
    
        var v_lat = $("#vendor_lat"+i).val();
        var v_lng = $("#vendor_lng"+i).val();

        var coordinates = new google.maps.LatLng(parseFloat(v_lat), parseFloat(v_lng));

        var m_a_p = new google.maps.Map(document.getElementById('zmapz'+i), {

          zoom: 15,
          center: coordinates

        });

        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer({
          preserveViewport: true
        });

        directionsDisplay.setMap(m_a_p);

        calcRoute(m_a_p, directionsService, directionsDisplay, i);

    }
  }

  


  function calcRoute(map, directionsService, directionsDisplay, index)
  {
    var c_lat = $("#customer_lat").val();
    var c_lng = $("#customer_lng").val();

    var startingLocation = new google.maps.LatLng(parseFloat(c_lat), parseFloat(c_lng));

     var request = {
        origin: startingLocation,
        destination: map.getCenter(),
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC
    };

    directionsService.route(request, function(response, status){
        if(status == google.maps.DirectionsStatus.OK){
            directionsDisplay.setDirections(response); 

            for (var i = response.routes.length - 1; i >= 0; i--) {
              for (var i = response.routes[i].legs.length - 1; i >= 0; i--) {
                $("#km"+index).html(response.routes[i].legs[i].distance.text);
              }
            }

        } 
    });
  }


  function vendor_delivery_confirmation() {
    
      var token = $("input[name=csrf-token-for-delivery]").val();
      var delivery_state = $("#delivery_status_confirm").val();
      var id = $("input[name=confirmation_order_id]").val();
      
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': token
        }
      });

      $.ajax({
        url: '/vendor-delivery-confirmation',
        type: 'post',
        data: {delivery_state: delivery_state, id: id},
        success: function(response) {
          var message = $('.message').fadeIn('fast');

          if(response.status == 1)
          {          
            var successMessage = '<div class="form-group mt-4">';
                  successMessage += '<div class="alert golden2">';
                  successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
                  successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
                  + response.message + '</p>';
                    successMessage +=  '</div></div>';

              $(message).html(successMessage).delay(4500).fadeOut('slow');
          }
          else if(response.status == 0)
          {
            var errorMessage = '<div class="form-group mt-4">';
                errorMessage += '<div class="alert golden">';
                errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';
                errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
                + response.message + '</p>';
                errorMessage +=  '</div></div>';

                $(message).html(errorMessage).delay(4500).fadeOut('slow');
          }

        },
        error: function(err) {
          console.log(err);
        }
    });
  }