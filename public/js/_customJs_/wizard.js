$(document).ready(function(){

  // ---------------- //
  // Global Variables //
  // ---------------- //

  var image_select = $("input[name=image]");

  var add_more = $("#more_address");

  var append_address = $("#address2");

  var delivery = $("#material-delivery");

  var append_delivery = $("#delivery");

  // ------------------------------ //
  // Avatar selection & replacement //
  // ------------------------------ //
  
  image_select.change(function(){
      
    if (this.files && this.files[0]) {

      var reader = new FileReader();

      reader.onload = function (e) {

          $('#profilePicturePreview').attr('src', e.target.result).fadeIn('slow');

      }

      reader.readAsDataURL(this.files[0]);

    }
  
  });


  add_more.click(function(){

    document.getElementById("address_inputs").style.display = 'block';

    add_more.attr("disabled", "disabled");

  });


  var inputAddress = 1;

  $("#address_input").on('change', function(){

    var inputAddress = $(this).val();

    $(".address-remove").remove();

    for (var i = 1; i < inputAddress; i++) {
      
      append_address.append('<div class="form-group address-remove"><label for="name">Address '+i+'</label><input id="addresses'+i+'" name="address[]" type="text" class="required form-control address" placeholder="Address '+i+'" ><div id="_address_error"></div><div id="maps'+i+'" class="show-map2"></div><input type="hidden" name="lat[]" id="lat'+i+'"><input type="hidden" name="lng[]" id="lng'+i+'"></div>');

      var map = new google.maps.Map(document.getElementById('maps'+i), {
      center: {
            lat: 9.024666568,
            lng: 38.737330384
      },
      zoom: 12
    });

    var marker = new google.maps.Marker({
      position: {
        lat: 9.024666568,
        lng: 38.737330384
      },
      map: map,
      draggable: true
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('addresses'+i));

    (function (marker, i) {

          google.maps.event.addListener(marker, 'position_changed', function() {

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat'+i).val(lat);
            $('#lng'+i).val(lng);

          });

      })(marker, i);

      (function (searchBox, map, marker) {

          google.maps.event.addListener(searchBox, 'places_changed', function() {

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for (i = 0; place=places[i]; i++) {
              bounds.extend(place.geometry.location);
              marker.setPosition(place.geometry.location);
            }
            map.fitBounds(bounds);
            map.setZoom(18);

          }); 

      })(searchBox, map, marker);   

    }

    document.getElementById("address_main").style.display = 'none';

  });


  
  delivery.change(function(){

    var checked = $(this).is(":checked");

    var appended_delivery = "";

    if(checked == true)
    {
      appended_delivery += "<div class='form-group'>";
          
      appended_delivery += "<label for='price'>Price per Kilometer</label>";
          
      appended_delivery += "<input id='price' name='price' type='text' class='required form-control' placeholder='Fill in the price in birr per Kilometer'>";
          
      appended_delivery += "</div>";

      append_delivery.html(appended_delivery);
    }
    else
    {
      append_delivery.html("");
    }

  });


    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
            lat: 9.024666568,
            lng: 38.737330384
      },
      zoom: 12
    });

    var marker = new google.maps.Marker({
      position: {
        lat: 9.024666568,
        lng: 38.737330384
      },
      map: map,
      draggable: true
    });

    var searchBox = new google.maps.places.SearchBox(document.getElementById('address'));
// }
  google.maps.event.addListener(searchBox, 'places_changed', function() {

    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i, place;

    for (i = 0; place=places[i]; i++) {
      bounds.extend(place.geometry.location);
      marker.setPosition(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(18);

  });


    google.maps.event.addListener(marker, 'position_changed', function() {

      var lat = marker.getPosition().lat();
      var lng = marker.getPosition().lng();

      $('#lat').val(lat);
      $('#lng').val(lng);

    });

  $("#search_map1").click(function() {

    google.maps.event.addListener(marker, 'position_changed', function() {

      var lat = marker.getPosition().lat();
      var lng = marker.getPosition().lng();

      $('#lat').val(lat);
      $('#lng').val(lng);

    });
  });


});

// }

 


var axxountForm = $("#accountForm");

var token = $("input[name=csrf-token]");

var agreement_error = "";

var address_error = "";

var _address_error = "";

var image_error = "";

var role = $("input[name=role]").val();


axxountForm.submit(function(e){

  if($("input[name=agreement]").is(':checked') != true)
  {
    agreement_error += "<p class='text-danger'>Agreement is required!</p>";

    $("#agreement_error").html(agreement_error);
  }
  else
  {
    $("#agreement_error").html("");

    agreement_error = "";
  }


  if($("input[name=address1]").val() == "")
  {
    address_error += "<p class='text-danger'>Address is required!</p>";

    $("#address_error").html(address_error);
  }
  else
  {
    $("#address_error").html("");
  }

  if($("#address").val() == "")
  {
    _address_error += "<p class='text-danger'>Address is required!</p>";

    $("#_address_error").html(_address_error);
  }
  else
  {
    $("#_address_error").html("");
  }


  if($("input[name=image]").val() == "")
  {
    image_error += "<p class='text-danger'>Image is required!</p>";

    $("#image_error").html(image_error);
  }
  else
  {
    $("#image_error").html("");

    image_error = "";
  }

});



axxountForm.children("div").steps({

  headerTag: "h3",

  bodyTag: "section",

  transitionEffect: "slideLeft",

  stepsOrientation: "vertical",

  enableFinishButton: false,

  onInit: function(event, currentIndex) {

    if(agreement_error == "")
    {

      axxountForm.submit(function(e){

        $.ajaxSetup({

          headers: {

            'X-CSRF-TOKEN': token.val()

          }

        });

        e.preventDefault();

        $.ajax({

          url: axxountForm.attr('action'),

          type: axxountForm.attr('method'),

          data: new FormData(this),

          contentType: false,

          processData: false,

          cache: false,

          beforeSend: function(){

            $("#submit-wizard-btn").attr('disabled', "disabled");

            $("#pending_status").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

          },

          success: function(response){

            $("#pending_status").html("");

            $("#submit-wizard-btn").removeAttr('disabled', "disabled");

            if(response.status == 'success')
            {

              if(response.role == 2)
              {
                location.replace('/dashboard');
              }
              else if(response.role == 3)
              {
                location.replace('/');
              }

            }
            else if(response.status == "error")
            {
              var message = $('.message').fadeIn('fast');

              var errorMessage = '<div class="form-group mt-4">';

                  errorMessage += '<div class="alert golden">';

                  errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                  
                  
                  errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                  + response.message + '</p>';


                  errorMessage +=  '</div></div>';


                  $(message).html(errorMessage).delay(4500).fadeOut('slow');
            }


          },

          error: function(error){


            $("#pending_status").html("");

            $("#submit-wizard-btn").removeAttr('disabled', "disabled");

            console.log(error);

            var message = $('.message').fadeIn('fast');
            
            if(error.responseJSON['errors'] != null)
            {
                    
              var errorMessage = '<div class="form-group mt-4">';

                  errorMessage += '<div class="alert golden">';

                  errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';

                  $.each(error.responseJSON['errors'], function(key, errors)
                  {
                  
                  errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'

                  + errors + '</p>';

                  });

                  errorMessage +=  '</div></div>';

                  $(message).html(errorMessage).delay(4500).fadeOut('slow');
                    
                    
            }

            else
            {

              $(message).html('').fadeIn('slow');

            }

          }

        });        

      });

    }

  }



});