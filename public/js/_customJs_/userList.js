
getUsers();

function getUsers() {
  var user = $("#user_list");
  var token = $("meta[name=csrf-token]");
  var form = $("#userForm");


  user.change(function(e){

    var table_data = $(".table-data");
    var _data_ = "";


    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token.attr('content')
      }
    });


    e.preventDefault();

    $.ajax({

      url: form.attr("action"),
      type: form.attr("method"),
      data: {user_role: user.val()},

      beforeSend: function(){

        $("#pending-status").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");

      },

      success: function(response){

        $("#pending-status").html("");

        if(response.user.length > 0)
        {

          _data_+= "<table class='order-listing table myDataTable'>";
            _data_+= "<thead>";
              _data_+= "<tr>";

              if(user.val() == 2)
              {
                _data_+= "<th></th>";
                _data_+= "<th>#</th>";
                _data_+= "<th>Shop Name</th>"
                _data_+= "<th>Email</th>";
                _data_+= "<th>Phone Number</th>";
                _data_+= "<th>About Shop</th>";
                _data_+= "<th>Address</th>";

              }

              else if(user.val() == 3)

              {

                _data_+= "<th>#</th>";
                _data_+= "<th>Name</th>"
                _data_+= "<th>Email</th>";
                _data_+= "<th>Phone Number</th>";
                _data_+= "<th>Address</th>";

              }

              _data_+= "</tr>";
            _data_+= "</thead>";
            _data_+= "<tbody>";

              var count = 0;

              for (var i = response.user.length - 1; i >= 0; i--) {

              _data_+= '<div class="modal modal-edu-general fade" role="dialog" id="activate'+response.user[i].id+'">';
                  _data_+= '<div class="modal-dialog">';
                     _data_+= '<div class="modal-content">';
                          _data_+= '<div class="modal-header header-color-modal bg-color-3">';
                              _data_+= '<h4 class="modal-title"><i class="fa fa-trash"></i>Activate '+response.user[i].email+'</h4>';
                              _data_+= '<hr>';
                          _data_+= '</div>';
                              _data_+= '<div class="modal-footer">';
                                  _data_+= '<button id="btn_activate" class="btn btn-success btn-activate" onclick="submitActivation('+response.user[i].id+')" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Activate</button>';
                              _data_+= '</div>';
                      _data_+= '</div>';
                   _data_+= '</div>';
               _data_+= '</div>';

              _data_+= "<tr>";

                count = count + 1;

                if(user.val() == 2)
                {

                  _data_+= "<td>";
                  if(response.user[i].activation === 1) {
                    _data_+= "<a data-toggle='modal' href='#' data-target='#activate"+response.user[i].id+"'><button class='main-btn' style='border-radius: 50%;'><i class='mdi mdi-check'></i></button></a>";
                  }
                  _data_+= "</td>";
                  _data_+= "<td>"+count+"</td>";
                  _data_+= "<td>";
                  if(response.user[i].shop_name != null) {
                    _data_+= response.user[i].shop_name;
                  }
                  _data_+=  "</td>";

                  _data_+= "<td>";
                  if(response.user[i].email != null) {
                    _data_+= response.user[i].email;
                  }
                  _data_+= "</td>";
                  _data_+= "<td>"+response.user[i].phone_number+"</td>";
                  _data_+= "<td>";
                  if(response.user[i].about_shop != null) {
                    _data_+= response.user[i].about_shop;
                  }
                  _data_+= "</td>";
                  _data_+= "<td>";

                  for (var i = response.address.length - 1; i >= 0; i--) {
                    _data_ += " | " + response.address[i].address + " | ";
                  }

                  _data_+= "</td>";

                  

                }

                else if(user.val() == 3)
                {

                  _data_+= "<td>"+count+"</td>";
                  _data_+= "<td>"+response.user[i].firstname+" "+response.user[i].lastname+"</td>";
                  _data_+= "<td>"+response.user[i].email+"</td>";
                  _data_+= "<td>"+response.user[i].phone_number+"</td>";
                  _data_+= "<td>";

                  for (var i = response.address.length - 1; i >= 0; i--) {
                    _data_ +=  " | " + response.address[i].address + " | ";
                  }

                }


              _data_+= "</tr>";
              }

            _data_+= "</tbody>";
          _data_+= "</table>";
          table_data.html(_data_);

          $(".myDataTable").dataTable({"bDestroy": true});

        }

      },

      error: function(error){

        $("#pending-status").html("");
      
      }

    });


  }).change();

}


function submitActivation(id) {
  $("#btn_activate").on('click', function() {
    var token = $("meta[name=csrf-token]");
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': token.attr('content')
        }
    });
    $.ajax({
        url: '/activate',
        type: "post",
        data: {id: id},

        beforeSend: function(){
          $("#btn_activate").attr("disabled", "disabled");
          $("#pending-status").html("<div class='dot-opacity-loader'><span></span><span></span><span></span></div>");
        },

        success: function(response){
          $("#pending-status").html("");
          $("#btn_activate").removeAttr("disabled", "disabled");
          var message = $('.message').fadeIn('fast');            
          var successMessage = '<div class="form-group mt-4">';
            successMessage += '<div class="alert golden2">';
            successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer; color:#fff;"><strong>&times;</strong></small>';          
            successMessage += '<p class="mr-5 ml-5" style="font-family: Montserrat; color: #fff; font-size: 16px; cursor: default;">'
            + response.message + '</p>';
            successMessage +=  '</div></div>';

            $(message).html(successMessage).delay(4500).fadeOut('slow');

            $("#activate"+id).modal("hide");
        },

        error: function(error){
          $("#pending-status").html("");
          $("#btn_activate").removeAttr("disabled", "disabled");
        }
      });
  });
}