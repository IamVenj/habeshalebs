<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|-----------------------------------------------------------------
| No - Internet Error
|-----------------------------------------------------------------
*/
Route::get('/clear-cache', function() {
   Artisan::call('config:cache');
   return 'linked';
});


/*
|--------------------------------------------------------------------
| The Above is a middleware view ||
|--------------------------------------------------------------------
*/

Route::get('/', 'HomeController@index');
Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@store')->name('contact.store');
Route::get('/admin/contact', 'ContactController@showToAdmin');

/*
|-----------------------------------------------------------------
| localization
|-----------------------------------------------------------------
*/

Route::post('/lang', 'LanguageController@update');

/*
|------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------
| CART
|--------------------------------------------------------------------
*/

Route::get('/cart', 'CartController@index');
Route::post('/cart', 'CartController@store');
Route::get('/empty-cart', 'CartController@empty');
Route::get('/get-cart', 'CartController@get_cart_list');
Route::get('/get-cart-main', 'CartController@cart_item');
Route::post('/update-cart', 'CartController@update');
Route::post('/remove-cartitem', 'CartController@removeFromCart');

/*
|------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------
| WISHLIST
|--------------------------------------------------------------------
*/

Route::get('/wishes', 'WishController@index');
Route::delete('/remove-wish/{id}', 'WishController@destroy');
Route::post('/add-wish', 'WishController@store');

/*
|------------------------------------------------------------------
*/


Route::get('/checkout', 'CheckoutController@index');
Route::post('/checkout', 'PaymentController@pay')->name('checkout.store');
Route::get('/checkout/complete', 'CheckoutController@orderComplete')->name('thankyou.index');



/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------
| Products
|-------------------------------------
|
| This is the route to products
|
*/

Route::get('/{category_name}/products/{category_id}', "ProductController@showBasedOnCategory");
Route::post('/category-products', "ProductController@productsBasedOnCategory");
Route::get('/{product_name}/detail/{product_id}', 'ProductController@show');

/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------
| Search
|-------------------------------------
|
| This is the route to autocomplete AJAX and search post request
|
*/


Route::post('/auto-complete', 'SearchController@autoComplete');
Route::get('/search', 'SearchController@search');
Route::post('/search-result', 'SearchController@searchProductsBasedOnCategory');

/*
|------------------------------------------------------------------
*/



/*
|--------------------------------------
| Authentication
|--------------------------------------
|
| This is the route for user login & register
| Login & Register Authentication
|
*/

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login', 'Auth\LoginController@store');
Route::get('/logout', 'Auth\LoginController@destroy');

/*
|------------------------------------------------
| -------------------------------------------------------------------------
|------------------------------------------------
*/

Route::get('/register', 'Auth\RegisterController@index');
Route::post('/customer/signup', 'Auth\RegisterController@storeCustomer');
Route::post('/seller/signup', 'Auth\RegisterController@storeSeller');
Route::get('/unactivated', 'ActivationController@index');
Route::post('/activate', 'ActivationController@update');

/*
|----------------------------------------------
| After Login / Post Login
|----------------------------------------------
|
| This is the route after a user logs in
| There are three types of users
| Admin | Seller | customer
|
*/

Route::get('/dashboard', 'DashboardController@index');
Route::get('/getYearlyIncome', 'DashboardController@getYearlyIncome');
Route::get('/getMonthlyIncome', 'DashboardController@getMonthlyIncome');
Route::get('/getDailyIncome', 'DashboardController@getDailyIncome');
Route::get('/getCashIncome', 'DashboardController@getCashIncome');
Route::get('/getAllIncome', 'DashboardController@getAdminAllIncome');

/*
|-------------------------------------------------------------------------------------
| Admin Role
|-------------------------------------------------------------------------------------
|
| This role can:
|  - customize carousel
|  - create Category & Sub-category
|  - view List of vendors & customers registered
|  - 
|
*/

/*
|-------------------------------------------------------------------------------------
|  Home Carousel
|-------------------------------------------------------------------------------------
*/

Route::get('/custom-carousel', 'CustomCarouselController@index');
Route::post('/custom-carousel', 'CustomCarouselController@store');
Route::patch('/update-image-carousel/{carousel_id}', 'CustomCarouselController@updateImage');
Route::patch('/edit-carousel/{carousel_id}', 'CustomCarouselController@update');
Route::patch('/delete-carousel/{carousel_id}', 'CustomCarouselController@destroy');


/*
|-------------------------------------------------------------------------------------
|  Category
|-------------------------------------------------------------------------------------
*/


Route::get('/category', 'CategoryController@index');
Route::post('/create-category', 'CategoryController@store');

/*
|-------------------------------------------------------------------------------------
|  List of Users | vendor and customer alike
|-------------------------------------------------------------------------------------
*/


Route::get('/users', 'UserListController@index');
Route::post('/users', 'UserListController@showUsers');


/*
|-------------------------------------------------------------------------------------
|--------------------------------- SELLER ROLE --------------------------------
|-------------------------------------------------------------------------------------
|
| This role can:
|  - View Orders
|  - Create & view Products
|
*/

Route::get('/orders', 'UserOrderController@index');
Route::get('/admin/orders', 'UserOrderController@indexOrdersToAdmin')->middleware('admin');
Route::get('/admin/orders/{id}', 'UserOrderController@innerIndexOrdersToAdmin')->middleware('admin');
Route::get('/admin/orders/{vendor_id}/detail/{id}', 'UserOrderController@showAdmin')->middleware('admin');
Route::get('/orders/{id}', 'UserOrderController@show');

/*
|-------------------------------------------------------------------------------------
|  Orders for user
|-------------------------------------------------------------------------------------
*/

Route::get('/my-orders', 'UserOrderController@showOrderToCustomer');
Route::delete('/cancel-my-order/{order_id}', 'UserOrderController@cancelOrder');
Route::post('/vendor-delivery-confirmation', 'UserOrderController@vendor_delivery_confirmation');
Route::patch('/confirm-customer-delivery/{order_id}', 'UserOrderController@delivery_confirmation_from_customer');

/*
|------------------------------------------------------------------
*/

/*
|-------------------------------------------------------------------------------------
|  Create product & view products
|-------------------------------------------------------------------------------------
*/

Route::get('/create-product', 'SellerProductController@create');
Route::post('/create-product', 'SellerProductController@store');
Route::get('/view-products', 'SellerProductController@index');
Route::get('/{product_name}/images/{product_id}', 'SellerProductController@show_product_images');
Route::get('/{product_name}/colors/{product_id}', 'SellerProductController@show_product_colors');
Route::patch('/update-product-image/{productImage_id}', 'SellerProductController@update_selected_image');
Route::delete('/delete-product-image/{productImage_id}', 'SellerProductController@destroy_selected_image');
Route::post('/add-product-images', 'SellerProductController@add_image');
Route::delete('/delete-product-color/{productColor_id}', 'SellerProductController@destroy_selected_color');
Route::patch('/update-product-color/{productColor_id}', 'SellerProductController@update_selected_color');
Route::post('/add-selected-colors', 'SellerProductController@add_selected_color');
Route::delete('/destroy-product/{product_id}', 'SellerProductController@destroy');
Route::patch('/update-product/{product_id}', 'SellerProductController@update');

/*
|------------------------------------------------------------------
*/


/*
|-------------------------------------------------------------------------------------
|  First Time Login --> Wizard
|-------------------------------------------------------------------------------------
*/

Route::get('/wizard', 'WizardController@index')->name('wizard-view');
Route::post('/wizard', 'WizardController@store');


/*
|------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------
| AJAX - requests
|-----------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Categories
|-----------------------------------------------------------------
*/

Route::post('/get-categories', 'CategoryController@postview');
Route::post('/show-categories-edit-modal', 'CategoryController@show_categories_edit_modal');
Route::post('/show-categories-edit-image-modal', 'CategoryController@show_categories_edit_image_modal');
Route::post('/edit-category', 'CategoryController@update');
Route::post('/edit-category-image', 'CategoryController@updateImage');
Route::delete('/delete-category', 'CategoryController@destroy');

/*
|------------------------------------------------------------------
*/


/*
|-----------------------------------------------------------------
| Reviews
|-----------------------------------------------------------------
*/

Route::post('/review', 'ReviewController@store');
Route::patch('/update-review/{id}', 'ReviewController@update');
Route::delete('/delete-review/{id}', 'ReviewController@destroy');
Route::get('/my-reviews/{product_id}', 'ReviewController@myReview');
Route::get('/vendor-reviews', 'ReviewController@showVendors');
Route::get('{shop_name}/reviews/{id}', 'ReviewController@showVendorReviews');


/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Settings
|-----------------------------------------------------------------
*/


Route::get('/settings', 'SettingsController@index');
Route::patch('/settings', 'SettingsController@store');


/*
|-----------------------------------------------------------------
| Account
|-----------------------------------------------------------------
*/

Route::get('/my-account', 'AccountManagementController@index');
Route::patch('/my-account', 'AccountManagementController@updateAccount');
Route::get('/account', 'AccountManagementController@customerAccountView');
Route::patch('/account', 'AccountManagementController@updateAccountForCustomer');
Route::patch('/update-profile-pic', 'AccountManagementController@updateProfilePic');
Route::patch('/change-u-password', 'AccountManagementController@changePassword');
Route::patch('/update-delivery-option', 'AccountManagementController@updateDelivery');

/*
|------------------------------------------------------------------
*/



/*
|-----------------------------------------------------------------
| My Address
|-----------------------------------------------------------------
*/

Route::get('/my-address', 'AddressManagementController@index');
Route::patch('/update-address/{locations_id}', 'AddressManagementController@update');
Route::post('/add-address', 'AddressManagementController@store');
Route::delete('/remove-map/{locations_id}', 'AddressManagementController@destroy');

/*
|------------------------------------------------------------------
*/



/*
|-----------------------------------------------------------------
| Forgot-password & Reset Password
|-----------------------------------------------------------------
*/

Route::get('/forgot-password', 'Auth\ForgotPasswordController@index');
Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');


Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/reset-password', 'Auth\ResetPasswordController@reset');

/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Debt
|-----------------------------------------------------------------
*/

Route::get('/debts', 'DebtController@index');
Route::get('/debt/{id}', 'DebtController@show');
Route::patch('/debt/{id}', 'DebtController@update');
Route::patch('/debt/item/{debt_id}', 'DebtController@updateEach');
Route::get('/my-debt', 'DebtController@seller_index');

/*
|------------------------------------------------------------------
*/

/*
|-----------------------------------------------------------------
| Newsletter
|-----------------------------------------------------------------
*/

Route::post('/join-newsletter', 'NewsletterController@store');
Route::get('/newsletter', 'NewsletterController@create');
Route::post('/newsletter/store', 'NewsletterController@storeLetter');
Route::patch('/newsletter/{id}/edit', 'NewsletterController@updateLetter');
Route::delete('/newsletter/{id}/destroy', 'NewsletterController@destroy');
Route::post('/newsletter/{id}/send', 'NewsLetterController@sendNewsLetter');

/*
|-----------------------------------------------------------------
| Notifications
|-----------------------------------------------------------------
*/

Route::post('/notification/index', 'NotificationController@index');
Route::post('/notification/read', 'NotificationController@read');

/*
|-----------------------------------------------------------------
| Special Offerz
|-----------------------------------------------------------------
*/

Route::get('/special-offers', 'SpecialOfferController@index');
Route::post('/get-vendor-special-offer', 'SpecialOfferController@getVendorProducts');
Route::post('/special-offers', 'SpecialOfferController@store');
Route::delete('/special-offer/{id}', 'SpecialOfferController@destroy');