<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Auth API
|--------------------------------------------------------------------------|
*/

Route::post('/register', 'ApiController@register');

Route::post('/login', 'ApiController@login');

/*
|--------------------------------------------------------------------------
| Products
|--------------------------------------------------------------------------|
*/

Route::post('/products', 'ApiController@productsBasedOnCategory');

Route::get('/{product_name}/detail/{product_id}', 'ProductController@show');

/*
|--------------------------------------------------------------------------
| Category
|--------------------------------------------------------------------------|
*/

Route::get('/get-categories', 'ApiController@getCategory');

/*
|--------------------------------------------------------------------------
| Home
|--------------------------------------------------------------------------|
*/

Route::get('/home-carousel', 'ApiController@homeCarousel');

Route::get('/special-offer', 'ApiController@specialOffers');

Route::get('/latest-products', 'ApiController@latestProduct');

Route::get('/best-selling-products', 'ApiController@bestSold');

/*
|--------------------------------------------------------------------------
| All Authorized API routes
|--------------------------------------------------------------------------|
*/

Route::middleware('auth:api')->group(function(){

	/*
	|--------------------------------------------------------------------------
	| Orders
	|--------------------------------------------------------------------------|
	*/

	Route::get('/my-orders', 'UserOrderController@showOrderToCustomer');

	Route::delete('/cancel-my-order/{order_id}', 'UserOrderController@cancelOrder');

	Route::patch('/confirm-customer-delivery/{order_id}', 'UserOrderController@delivery_confirmation_from_customer');

	/*
	|--------------------------------------------------------------------------
	| Wish
	|--------------------------------------------------------------------------|
	*/

	Route::get('/wishlist', 'WishController@index');

	Route::delete('/remove-wish/{id}', 'WishController@destroy');

	Route::post('/add-wish', 'WishController@store');

	/*
	|--------------------------------------------------------------------------
	| Review
	|--------------------------------------------------------------------------|
	*/

	Route::post('/review', 'ReviewController@store');

	Route::patch('/update-review/{id}', 'ReviewController@update');

	Route::delete('/delete-review/{id}', 'ReviewController@destroy');

});