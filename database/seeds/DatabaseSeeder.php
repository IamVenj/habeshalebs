<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $colors = [

            [
                'colour_name' => 'Indian Red',
                'hex' => '#B0171F',
            ],

            [
                'colour_name' => 'Crimson',
                'hex' => '#DC143C',
            ],

            [
                'colour_name' => 'LightPink',
                'hex' => '#FFB6C1',
            ],

            [
                'colour_name' => 'Pink',
                'hex' => '#FFC0CB',
            ],

            [
                'colour_name' => 'Pale Violet Red',
                'hex' => '#DB7093',
            ],

            [
                'colour_name' => 'Lavendar Blush',
                'hex' => '#FFF0F5',
            ],

            [
                'colour_name' => 'Violet Red',
                'hex' => '#FF3E96',
            ],

            [
                'colour_name' => 'Hot Pink',
                'hex' => '#FF69B4',
            ],

            [
                'colour_name' => 'Raspberry',
                'hex' => '#872657',
            ],

            [
                'colour_name' => 'Deep Pink',
                'hex' => '#FF1493',
            ],

            [
                'colour_name' => 'Maroon',
                'hex' => '#FF34B3',
            ],

            [
                'colour_name' => 'Medium Violet Red',
                'hex' => '#C71585',
            ],

            [
                'colour_name' => 'Orchid',
                'hex' => '#DA70D6',
            ],

            [
                'colour_name' => 'Thistle',
                'hex' => '#D8BFD8',
            ],

            [
                'colour_name' => 'Plum',
                'hex' => '#DDA0DD',
            ],

            [
                'colour_name' => 'Magenta',
                'hex' => '#FF00FF',
            ],

            [
                'colour_name' => 'Purple',
                'hex' => '#800080',
            ],

            [
                'colour_name' => 'Dark Violet',
                'hex' => '#9400D3',
            ],

            [
                'colour_name' => 'Indigo',
                'hex' => '#480082',
            ],

            [
                'colour_name' => 'Dark Slate Blue',
                'hex' => '#483D8B',
            ],

            [
                'colour_name' => 'Light Slate Blue',
                'hex' => '#8470FF',
            ],

            [
                'colour_name' => 'Slate Blue',
                'hex' => '#6A5ACD',
            ],

            [
                'colour_name' => 'Ghost White',
                'hex' => '#F8F8FF',
            ],

            [
                'colour_name' => 'Blue',
                'hex' => '#0000FF',
            ],

            [
                'colour_name' => 'Navy',
                'hex' => '#000080',
            ],

            [
                'colour_name' => 'Cobalt',
                'hex' => '#3D59AB',
            ],

            [
                'colour_name' => 'Royal Blue',
                'hex' => '#4169E1',
            ],

            [
                'colour_name' => 'Corn Flower Blue',
                'hex' => '#6495ED',
            ],

            [
                'colour_name' => 'Light Steel Blue',
                'hex' => '#B0C4DE',
            ],

            [
                'colour_name' => 'Dodger Blue',
                'hex' => '#1E90FF',
            ],

            [
                'colour_name' => 'Steel Blue',
                'hex' => '#4682B4',
            ],

            [
                'colour_name' => 'Light Sky Blue',
                'hex' => '#87CEFA',
            ],

            [
                'colour_name' => 'Sky Blue',
                'hex' => '#87CEFF',
            ],

            [
                'colour_name' => 'Deep Sky Blue',
                'hex' => '#00BFFF',
            ],


            [
                'colour_name' => 'Peacock',
                'hex' => '#33A1C9',
            ],

            [
                'colour_name' => 'Light Blue',
                'hex' => '#ADD8E6',
            ],

            [
                'colour_name' => 'Powder Blue',
                'hex' => '#B0E0E6',
            ],

            [
                'colour_name' => 'Cadet Blue',
                'hex' => '#5F9EA0',
            ],

            [
                'colour_name' => 'Turquoise',
                'hex' => '#00868B',
            ],

            [
                'colour_name' => 'Azure',
                'hex' => '#F0FFFF',
            ],

            [
                'colour_name' => 'Light Cyan',
                'hex' => '#E0FFFF',
            ],

            [
                'colour_name' => 'Cyan/Aqua',
                'hex' => '#00FFFF',
            ],

            [
                'colour_name' => 'Teal',
                'hex' => '#008080',
            ],

            [
                'colour_name' => 'Aqua marine',
                'hex' => '#7FFFD4',
            ],

            [
                'colour_name' => 'Medium Spring Green',
                'hex' => '#00FA9A',
            ],

            [
                'colour_name' => 'Spring Green',
                'hex' => '#00FF7F',
            ],

            [
                'colour_name' => 'Sea Green',
                'hex' => '#54FF9F',
            ],

            [
                'colour_name' => 'Emerald Green',
                'hex' => '#00C957',
            ],

            [
                'colour_name' => 'Pale Green',
                'hex' => '#98FB98',
            ],

            [
                'colour_name' => 'Lime Green',
                'hex' => '#32CD32',
            ],

            [
                'colour_name' => 'Forest Green',
                'hex' => '#228B22',
            ],

            [
                'colour_name' => 'Green',
                'hex' => '#00FF00',
            ],

            [
                'colour_name' => 'Sap Green',
                'hex' => '#308014',
            ],

            [
                'colour_name' => 'Olive Drab',
                'hex' => '#C0FF3E',
            ],

            [
                'colour_name' => 'Ivory',
                'hex' => '#FFFFF0',
            ],

            [
                'colour_name' => 'Beige',
                'hex' => '#F5F5DC',
            ],

            [
                'colour_name' => 'light Yellow',
                'hex' => '#FFFFE0',
            ],

            [
                'colour_name' => 'Yellow',
                'hex' => '#FFFF00',
            ],

            [
                'colour_name' => 'Dark Khaki',
                'hex' => '#8DB76B',
            ],

            [
                'colour_name' => 'Khaki',
                'hex' => '#F0E68C',
            ],

            [
                'colour_name' => 'Banana',
                'hex' => '#E3CF57',
            ],

            [
                'colour_name' => 'Gold',
                'hex' => '#FFD700',
            ],

            [
                'colour_name' => 'Cornsilk',
                'hex' => '#FFF8DC',
            ],

            [
                'colour_name' => 'Golden Rod',
                'hex' => '#DAA520',
            ],

            [
                'colour_name' => 'Orange',
                'hex' => '#FFA500',
            ],

            [
                'colour_name' => 'Wheat',
                'hex' => '#F5DEB3',
            ],

            [
                'colour_name' => 'Navajo White',
                'hex' => '#FFDEAD',
            ],

            [
                'colour_name' => 'Tan',
                'hex' => '#D2B48C',
            ],

            [
                'colour_name' => 'Carrot',
                'hex' => '#ED9121',
            ],

            [
                'colour_name' => 'Dark Orange',
                'hex' => '#FF7F00',
            ],

            [
                'colour_name' => 'Chocolate',
                'hex' => '#8B4513',
            ],

            [
                'colour_name' => 'Ivory Black',
                'hex' => '#292421',
            ],

            [
                'colour_name' => 'Sienna',
                'hex' => '#A0522D',
            ],

            [
                'colour_name' => 'Coral',
                'hex' => '#FF7F50',
            ],

            [
                'colour_name' => 'Orange Red',
                'hex' => '#FF4500',
            ],

            [
                'colour_name' => 'Salmon',
                'hex' => '#FF8C69',
            ],

            [
                'colour_name' => 'Tomato',
                'hex' => '#FF6347',
            ],

            [
                'colour_name' => 'Rosy Brown',
                'hex' => '#FFC1C1',
            ],

            [
                'colour_name' => 'Brown',
                'hex' => '#A52A2A',
            ],

            [
                'colour_name' => 'Fire Brick',
                'hex' => '#B22222',
            ],

            [
                'colour_name' => 'Red',
                'hex' => '#FF0000',
            ],

            [
                'colour_name' => 'Sgi Beet',
                'hex' => '#8E388E',
            ],

            [
                'colour_name' => 'Sgi Slateblue',
                'hex' => '#7171C6',
            ],

            [
                'colour_name' => 'Sgi Teal',
                'hex' => '#388E8E',
            ],

            [
                'colour_name' => 'Black',
                'hex' => '#000000',
            ],
            [
                'colour_name' => 'White',
                'hex' => '#FFFFFF',
            ],

            [
                'colour_name' => 'Gainsboro',
                'hex' => '#DCDCDC',
            ],

            [
                'colour_name' => 'Silver',
                'hex' => '#C0C0C0',
            ],

            [
                'colour_name' => 'Gray99',
                'hex' => '#FCFCFC',
            ],

            [
                'colour_name' => 'Gray80',
                'hex' => '#CCCCCC',
            ],

            [
                'colour_name' => 'Gray70',
                'hex' => '#B3B3B3',
            ],

            [
                'colour_name' => 'Gray60',
                'hex' => '#999999',
            ],

            [
                'colour_name' => 'Gray50',
                'hex' => '#7F7F7F',
            ],

            [
                'colour_name' => 'Gray40',
                'hex' => '#666666',
            ],

            [
                'colour_name' => 'Gray30',
                'hex' => '#4D4D4D',
            ],

            [
                'colour_name' => 'Gray20',
                'hex' => '#333333',
            ],

            [
                'colour_name' => 'Gray10',
                'hex' => '#1A1A1A',
            ],

        ];

    	DB::table('users')->delete();

      	factory(App\User::class)->create([

	        'firstname' => 'Admin',

	        'email' => 'admin@admin.com',

	        'password' => bcrypt('lebsHabesha_admin'),

	        'role' => '1',

	      ]);

        DB::table('colors')->delete();

        foreach ($colors as $color) {
        
            factory(App\Colors::class)->create([

                'colour_name' => $color['colour_name'],

                'hex' => $color['hex'],

            ]);
    
        }

        DB::table('company_settings')->delete();

        factory(App\CompanySettings::class)->create([

            'company_name' => 'Habesha Lebs',

            'location' => '291 South 21th Street, Suite 721 New York NY 10016',

            'slug' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using "Content here, content here", making it look like readable English.',
            
            'phone_number' => '+ 1235 2355 98',
            
            'email' => 'info@yoursite.com',

            'charge' => 10,

            'delivery_price' => 50,
            
            'delivery_time' => '15 days',

            'delivery_email' => "Thankyou for ordering these item/s",

            'item_amount_flat_rate' => 2,

            'customer_support' => '+ 1234 (numbers)'

        ]);
    }
}
