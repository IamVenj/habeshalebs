<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('order_code');
            $table->boolean('payment_status')->default(0);
            $table->boolean('vendor_delivery_confirmation')->default(0);
            $table->boolean('customer_delivery_confirmation')->default(0);
            $table->integer('quantity');
            $table->float('paid_amount')->nullable();
            $table->float('shipping')->nullable();
            $table->longtext('personal_statement')->nullable();
            $table->bigInteger('size_id')->unsigned();
            $table->foreign('size_id')->references('id')->on('product_sizes')->onDelete('cascade');
            $table->bigInteger('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
