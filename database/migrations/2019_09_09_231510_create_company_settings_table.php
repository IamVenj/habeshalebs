<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->longtext('slug')->nullable();
            $table->string('location');
            $table->string('phone_number');
            $table->string('email');
            $table->text('facebook')->nullable();
            $table->text('twitter')->nullable();
            $table->text('google_plus')->nullable();
            $table->text('instagram')->nullable();
            $table->double('charge');
            $table->string('customer_support');
            $table->double('delivery_price')->nullable();
            $table->integer('item_amount_flat_rate')->nullable();
            $table->string('delivery_time')->nullable();
            $table->longtext('delivery_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_settings');
    }
}
