<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Colors;

use App\CompanySettings;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    return [

        'firstname' => $faker->name,

        'email' => $faker->unique()->safeEmail,

        'email_verified_at' => now(),

        'role' => 1,

        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password

        'remember_token' => Str::random(10),

    ];

});


$factory->define(Colors::class, function (Faker $faker) {

    return [

        'colour_name' => $faker->name,

        'hex' => Str::random(20),

    ];

});


$factory->define(CompanySettings::class, function (Faker $faker) {

    return [

        'location' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'slug' => $faker->name,
        'phone_number' => $faker->name,
        'company_name' => $faker->name,
        'charge' => $faker->name,
        'delivery_price' => $faker->name,
        'item_amount_flat_rate' => $faker->name,
        'customer_support' => $faker->name
        'delivery_time' => $faker->name
        'delivery_email' => $faker->name

    ];

});
