@extends('post-login.index.index')

@section('content')

@include('_session_.error2')

@include('_session_.success2')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Reviews')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>{{__('app.Rating')}}</th>

                  <th>{{__('app.Review')}}</th>
                  
              </tr>

            </thead>

            <tbody>

              @foreach($reviews as $review)

              <tr>

                <td>
                  
                  @if($review->rating == 1)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  @elseif($review->rating == 2)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  @elseif($review->rating == 3)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>
                  
                  @elseif($review->rating == 4)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>
                  
                  @elseif($review->rating == 5)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>
                  
                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  @endif

                </td>

                <td>{{$review->review}}</td>

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection