@extends('post-login.index.index')

@section('content')

@include('_session_.error2')

@include('_session_.success2')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{$vendor->shop_name}} | {{__('app.Reviews')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>{{__('app.product')}}</th>

                  <th>{{__('app.Product_Name')}}</th>

                  <th>{{__('app.Rating')}}</th>

                  <th>{{__('app.Review')}}</th>

                  <th></th>
                  
              </tr>

            </thead>

            <tbody>

              @foreach($reviews as $review)

              @for($i=0; $i < count($review); $i++)

              <tr>

                <td>

                  <img src="<?= Cloudder::show($review[$i]->product()->first()->productImage()->first()->image_public_id, array("version" => $review[$i]->product()->first()->productImage()->first()->image_version, "quality" => "auto", "height" => 1800, "width"=>1000));?>">

                </td>

                <td>{{$review[$i]->product()->first()->product_name}}</td>

                <td>
                  
                  @if($review[$i]->rating == 1)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  @elseif($review[$i]->rating == 2)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>

                  @elseif($review[$i]->rating == 3)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>

                  <i class="mdi mdi-star-outline"></i>
                  
                  @elseif($review[$i]->rating == 4)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star-outline"></i>
                  
                  @elseif($review[$i]->rating == 5)

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>
                  
                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  <i class="mdi mdi-star" style="color: #F68E28;"></i>

                  @endif

                </td>

                <td>{{$review[$i]->review}}</td>
                
                <td>
                	
                	<form action="/delete-review/{{$review[$i]->id}}" method="post">

                    @csrf

                    @method("DELETE")
                    
                    <button class="main-btn icon-btn" type="submit"><i class="mdi mdi-close"></i></button>

                  </form>

                </td>

              </tr>

              @endfor

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection