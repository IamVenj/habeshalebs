@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  	<div class="card">

    		<div class="card-body">

      		<h4 class="card-title">{{__('app.special_offers')}}</h4>

      			<form class="forms-sample" action="/special-offers" method="post">

      			@csrf
        
        			<div class="form-group">

          				<label>{{__('app.Vendor')}}</label>

	        			<select class="form-control form-control-lg" id="vendor" name="vendor" onchange="getVendors()">

	          				<option selected disabled>{{__('app.select_vendor')}}</option>

	          				@foreach($vendors as $vendor)

    	          				<option value="{{$vendor->id}}">{{$vendor->shop_name}}</option>

    	          				@endforeach

	        			</select>

  				</div>

        			<div id="products"></div>

      		</form>

    		</div>

  	</div>

</div>

<div class="col-12 grid-margin stretch-card">

	<div class="card">

    		<div class="card-body">

  			<div class="row">

        			@foreach($offers as $offer)

				<div class="col-md-3 mt-3">

					<div class="card">

            				<img class="card-img-top" style="height: 250px; object-fit: cover;" src="<?= Cloudder::show($offer->product()->first()->productImage()->first()->image_public_id, ['version'=> $offer->product()->first()->productImage()->first()->image_version, 'width'=>200, 'height'=>200, 'crop'=>'fill']);?>" alt="{{$offer->product()->first()->productImage()->first()->image_public_id}}">

            				<div class="card-body">

	              				<h4 class="card-title mb-3" style="font-weight: bold;">{{$offer->product()->first()->product_name}}</h4>

					        	<div class="dropdown-divider"></div>

              					<div class="ticket-actions mb-3">

                						<div class="btn-group dropdown">

	                						<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                            	
						                  	{{__('app.Manage')}}

						                	</button>

	                  					@include('post-login.partials.modal.offer-modal')

	                						<div class="dropdown-menu">

	                    						<a class="dropdown-item" data-toggle="modal" href="#" data-target="#delete-offer{{$offer->id}}">

	                    							<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Delete')}}</a>

	                						</div>

	                					</div>

	              				</div>

	              				<p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;">

	              					<i class="mdi mdi-clock"></i> {{$offer->created_at->diffForHumans()}}</p> 

	            			</div>

	          			</div>

				</div>

	        		@endforeach

			</div>

		</div>

	</div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection