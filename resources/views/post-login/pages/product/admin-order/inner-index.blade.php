@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>

                  <th>{{__('app.Order')}} #</th>

                  <th>{{__('app.order_date')}}</th>

                  <th>{{__('app.customer')}}</th>

                  <th>{{__('app.price')}} + {{__('app.shipping')}}</th>

                  <th>{{__('app.Item_SKU')}}</th>

                  <th>{{__('app.quantity')}}</th>

                  <th>{{__('app.Payment_status')}}</th>

                  <th>{{__('app.Sent_Date')}}</th>

                  <th></th>

                  <th></th>
                  
                  <th></th>

              </tr>

            </thead>

            <tbody>

              @foreach($orders as $order)

              <tr>

                  <td>{{$order->order_code}}</td>

                  <td><?= date('d M y' ,strtotime($order->created_at));?></td>

                  <td>{{$order->user->firstname.' '.$order->user->lastname}}</td>

                  <td>{{$order->paid_amount}}</td>

                  <td>{{$order->product->sku}}</td>

                  <td>x{{$order->quantity}}</td>

                  <td>

                    @if($order->payment_status == 1)

                    <label class="badge badge-success">{{__('app.Paid')}}<span class="mdi mdi-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green; color: #fff;"></span></label>

                    @elseif($order->payment_status == 0)

                    <label class="badge badge-danger">{{__('app.Unpaid')}}<span class="mdi mdi-close" style="margin-left: 10px;"></span></label>                    

                    @endif

                  </td>

                  <td>{{$order->created_at->diffForHumans()}}</td>

                  <td> 

                    @if(strtotime(date('Y-m-d')) - strtotime($order->created_at) < 86400)

                    <label class="badge badge-success">{{__('app.New')}}</label> 

                    @endif

                  </td>

                   <td> 

                    @if($order->vendor_delivery_confirmation == 1 && $order->customer_delivery_confirmation == 1)

                    <label class="badge badge-success">{{__('app.Delivered')}}<span class="mdi mdi-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green;"></span></label> 

                    @endif

                  </td>

                  @if($order->customer_delivery_confirmation == 0)

                  <td>

                    <a href="/admin/orders/{{$id}}/detail/{{$order->id}}"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>

                  </td>

                  @else

                  <td></td>

                  @endif

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection