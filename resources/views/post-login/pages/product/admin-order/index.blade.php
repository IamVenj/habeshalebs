@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>#</th>

                  <th>{{__('app.Vendor')}}</th>

                  <th></th>
                  
              </tr>

            </thead>

            <tbody>

              <?php $count = 0;?>

              @foreach($vendors as $vendor)
              
              <tr>

                <?php $count = $count + 1;?>

                <td>{{$count}}</td>

                <td>{{$vendor->shop_name}}</td>
                                                
                <td>
                  
                  <a href="/admin/orders/{{$vendor->id}}">

                    <button class="main-btn" style="border-radius: 50%;">

                      <i class="mdi mdi-eye"></i>

                    </button>

                  </a>

                </td>

              </tr>
              
              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection