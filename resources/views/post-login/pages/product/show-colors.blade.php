@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
 
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
 
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Products')}}</li>
 
    <li class="breadcrumb-item"><a href="/view-products">{{__('View_Products')}}</a></li>
 
    <li class="breadcrumb-item active" aria-current="page"><span>{{$product_name}}</span></li>
 
  </ol>

</div>

	
<div class="col-12 grid-margin stretch-card">

	<a href="/view-products" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Products')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>




<div class="col-12 grid-margin stretch-card">

	<div class="card">

	    <div class="card-body">

			<div class="row">
		                        
			    @foreach($productColors as $productColor)
			    
			    <div class='col-md-4'>

					<form action="/delete-product-color/{{$productColor->id}}" method="POST">

						@csrf

						@method("DELETE")
						
			        	<button class="close" type="submit" style="color: #000; cursor: pointer;"><span class="mdi mdi-close-circle" style="color: #000;"></span></button>
			        	
					</form>

					@foreach($productColor->colors()->get() as $chosen_colors)
		            
		        	<div class="form-check">
		        
		          		<label class="form-check-label">
		        
		           			{{$chosen_colors->colour_name}}
		        
		            		<div style="padding: 5%; background: {{$chosen_colors->hex}};"></div>
		        		        
		          		</label>
		        
		        	</div>

		        	@endforeach
		       

			        <div id="pending_status_edit"></div>

			        <form action="/update-product-color/{{$productColor->id}}" method="POST">

			        	@csrf

			        	@method('PATCH')
			        	
			        	<div class="row">
			        		
							<div class="col-md-10">
								
						        <select class="form-control form-control-sm" name="color-update">
						        	
						        	<option selected="" disabled="">{{__('app.Replace_Color')}}</option>

						        	@foreach($all_colors as $colors)

						        	<option value="{{$colors->id}}" style="background: {{$colors->hex}}">{{$colors->colour_name}}</option>

						        	@endforeach

						        </select>

							</div>	        		

							<div class="col-2">
		        
						        <button class="btn btn-warning btn-block save-image-update" type="submit" ><span class="mdi mdi-check"></span></button>

							</div>

			        	</div>

			    	</form>

			    </div>

			    @endforeach

			    @include('post-login.partials.modal.products-color-modal')

			    <a class="btn btn-primary mt-4" data-toggle="modal" href="#" data-target="#select-color"><span class="mdi mdi-plus"></span></a>


			</div>

		</div>

	</div>
	
</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection