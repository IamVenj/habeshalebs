@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Orders')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table datatable-1">

            <thead>

              <tr>

                  <th>{{__('app.Order')}} #</th>

                  <th>{{__('app.order_date')}}</th>

                  <th>{{__('app.customer')}}</th>

                  <th>{{__('app.price')}} + {{__('app.shipping')}}</th>

                  <th>{{__('app.Item_SKU')}}</th>

                  <th>{{__('app.quantity')}}</th>

                  <th>{{__('app.Payment_status')}}</th>

                  <th>{{__('app.Sent_Date')}}</th>

                  <th></th>

                  <th></th>
                  
                  <th></th>

              </tr>

            </thead>

            <tbody>

              @foreach($orders as $order)

              @for($i = 0; $i < count($order); $i++)

              <tr>

                  <td>{{$order[$i]['order_code']}}</td>

                  <td><?= date('d M y' ,strtotime($order[$i]['created_at']));?></td>

                  <td>{{$order[$i]->user()->first()->firstname.' '.$order[$i]->user()->first()->lastname}}</td>

                  <td>{{$order[$i]->paid_amount}}</td>

                  <td>{{$order[$i]->product()->first()->sku}}</td>

                  <td>x{{$order[$i]['quantity']}}</td>

                  <td>

                    @if($order[$i]['payment_status'] == 1)

                    <label class="badge badge-success">{{__('app.Paid')}}<span class="mdi mdi-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green; color: #fff;"></span></label>

                    @elseif($order[$i]['payment_status'] == 0)

                    <label class="badge badge-danger">{{__('app.Unpaid')}}<span class="mdi mdi-close" style="margin-left: 10px;"></span></label>                    

                    @endif

                  </td>

                  <td>{{$order[$i]['created_at']->diffForHumans()}}</td>

                  <td> 

                    @if(strtotime(date('Y-m-d')) - strtotime($order[$i]['created_at']) < 86400)

                    <label class="badge badge-success">{{__('app.New')}}</label> 

                    @endif

                  </td>

                   <td> 

                    @if($order[$i]->vendor_delivery_confirmation == 1 && $order[$i]->customer_delivery_confirmation == 1)

                    <label class="badge badge-success">{{__('app.Delivered')}}<span class="mdi mdi-check ml-2" style="padding: 10px; border-radius: 50px; background-color: green;"></span></label> 

                    @endif

                  </td>

                  @if($order[$i]->customer_delivery_confirmation == 0)

                  <td>

                    <a href="/orders/{{$order[$i]['id']}}"><button class="btn btn-outline-primary" style="border-radius: 50px;"><span class="mdi mdi-eye"></span> </button></a>

                  </td>

                  @else

                  <td></td>

                  @endif

              </tr>

              @endfor

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection