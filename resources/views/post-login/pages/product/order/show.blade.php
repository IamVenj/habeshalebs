@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>

    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Orders')}}</li>

    <li class="breadcrumb-item active" aria-current="page"><span>{{$order->order_code}}</span></li>

  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

    <a href="/orders" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Orders')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-md-12 grid-margin stretch-card d-none d-md-flex">

  <div class="card">

    <div class="card-body">

      <h4 class="card-title">{{$order->order_code}}</h4>

      <div class="row">

        <div class="col-4">

          <ul class="nav nav-tabs nav-tabs-vertical" role="tablist">

            

            <li class="nav-item">

              <a class="nav-link active" id="profile-tab-vertical" data-toggle="tab" href="#customer-info" role="tab" aria-controls="profile-2" aria-selected="false">

              {{__('app.Customer_Information')}}

              <i class="mdi mdi-account-outline text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#delivery-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Delivery_Information')}}

              <i class="mdi mdi-train-car text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="home-tab-vertical" data-toggle="tab" href="#order-info" role="tab" aria-controls="home-2" aria-selected="true">

              {{__('app.Order_Detail')}}

              <i class="mdi mdi-receipt text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#product-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Product_Information')}}

              <i class="mdi mdi-sitemap text-danger ml-2"></i>

              </a>

            </li>

            <li class="nav-item">

              <a class="nav-link" id="contact-tab-vertical" data-toggle="tab" href="#payment-info" role="tab" aria-controls="contact-2" aria-selected="false">

              {{__('app.Payment_Information')}}

              <i class="mdi mdi-cash text-danger ml-2"></i>

              </a>

            </li>

            

          </ul>

        </div>

        <div class="col-8">

          <div class="tab-content tab-content-vertical">

            <div class="tab-pane fade show active" id="customer-info" role="tabpanel" aria-labelledby="profile-tab-vertical">

                <div class="picture-container">
              
                  <div class="picture">
            
                    <img src="@if($customer->photo_url != null)<?= Cloudder::show(auth()->user()->photo_url, ['width'=>200, 'height'=>200, 'crop'=>'fill']);?>@else{{URL::asset('images/default-avatar.png')}}@endif" class="picture-src" id="profilePicturePreview" title=""/>
                            
                  </div>
                              
                </div>

                <h2 class="mt-0 text-center">{{$customer->firstname.' '.$customer->lastname}}</h2>

                <hr>

                <p class="mb-0">

                  <i class="mdi mdi-phone text-primary"></i> 

                  {{$customer->phone_number}}

                </p>

                <p class="mb-0">

                  <?php $count = 1; ?>

                  @foreach($customer->addresses()->get() as $address)

                  <i class="mdi mdi-map-marker text-primary"></i> 

                   {{$count}}. {{$address->address}} 

                   <?php $count++; ?>

                   @endforeach

                </p>

            </div>

            <div class="tab-pane fade" id="delivery-info" role="tabpanel" aria-labelledby="home-tab-vertical">

              <h2>{{__('app.Delivery_status')}}</h2>

               <!-- map -->

               <!-- vendors location -->

               <?php $count = 0; ?>

               @for($i = 0; $i < auth()->user()->addresses()->count(); $i++)

               <?php $count = $count + 1; ?>

              <div class="show-map2" id="zmapz{{$count}}"></div>

              <input type="hidden" id="vendor_lat{{$count}}" value="{{auth()->user()->addresses()->get()[$i]->latitude}}">

              <input type="hidden" id="vendor_lng{{$count}}" value="{{auth()->user()->addresses()->get()[$i]->longitude}}">

               <div id="km{{$count}}" style="font-weight: bold;"></div>

               @endfor

               <input type="hidden" name="add_count" value="{{auth()->user()->addresses()->count() + 1}}">

               @if(!is_null($customer->addresses()->first() ))

               <!-- Customers locations -->

               <input type="hidden" id="customer_lat" value="{{$customer->addresses()->first()->latitude}}">

               <input type="hidden" id="customer_lng" value="{{$customer->addresses()->first()->longitude}}">

               <!-- end of map -->

               @endif


               <hr>

               <label for="">{{__('app.changeDeliveryState')}}</label>

               <input type="hidden" name="csrf-token-for-delivery" value="{{csrf_token()}}">

               <input type="hidden" name="confirmation_order_id" value="{{$order->id}}">

              <select class="form-control form-control-sm" id="delivery_status_confirm" name="delivery_state" onchange="vendor_delivery_confirmation()">
                
                <option value="0" <?php if($order->vendor_delivery_confirmation == 0): ?> selected <?php endif; ?>>{{__('app.NotDeliveredYet')}}</option>
                
                <option value="1" <?php if($order->vendor_delivery_confirmation == 1): ?> selected <?php endif; ?>>{{__('app.Delivered')}}</option>

              </select>

            </div>


            <div class="tab-pane fade" id="order-info" role="tabpanel" aria-labelledby="home-tab-vertical">

              <h2>{{__('app.Order_Detail')}}</h2>

              <hr>

              <h5>order #: <span style="color: red;"> {{$order->order_code}} </span></h5>
              <h5>order's Personal statement: <span style="color: red;"> {{$order->personal_statement}} </span></h5>

              <h5>{{__('app.order_date')}}: <span style="color: green;"> {{$order->created_at->diffForHumans()}} | {{date('d M y', strtotime($order->created_at))}} </span></h5>

            </div>

            

            <div class="tab-pane fade" id="product-info" role="tabpanel" aria-labelledby="contact-tab-vertical">

              <h2> {{__('app.Product_Information')}} </h2>

              <hr>

              <div class="row">
                
                <div class="col-md-4">
                  
                  <img class="card-img-top" style="height: 200px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="<?= Cloudder::show($order->product()->first()->productImage()->first()->image_public_id, ['version' => $order->product()->first()->productImage()->first()->image_version, 'width'=>200, 'height'=>200, 'crop'=>'fill']);?>" alt="">

                </div>

                <div class="col-md-8">

                  <p class="mb-4">{{$order->product()->first()->product_name}}</p>

                  <p class="mb-4">{{$order->product()->first()->slug}}</p>
                  
                  <p class="mb-4">{{$order->product()->first()->category()->first()->category_name}}</p>
                  
                  <p class="mb-4">{{$order->product()->first()->brand}}</p>

                  <p class="mb-4">{{__('app.Item_SKU')}}: {{$order->product()->first()->sku}}</p>
                  
                  <p class="mb-4">

                    @if($order->product()->first()->product_location == 'all')

                    {{$order->product()->first()->product_location}} Stores 

                    @else 

                    {{$order->product()->first()->product_location}} 

                    @endif

                  </p>
                  
                  <p class="mb-4">{{$order->product()->first()->availability}} {{__('app.available')}}</p>
                
                </div>

              </div>


              
            </div>

            <div class="tab-pane fade" id="payment-info" role="tabpanel" aria-labelledby="contact-tab-vertical">

              <h2> {{__('app.Payment_Information')}} </h2>

              <hr>

              @if($order->payment_status == 0 && $order->payment_method == 'yenepay')

              <label class="badge badge-warning">{{__('app.Pending')}}<span class="mdi mdi-refresh" style="margin-left: 10px;"></span></label>

              @elseif($order->payment_status == 0 && $order->payment_method == 'on-delivery')

              <label class="badge badge-danger">{{__('app.Not_Paid')}}<span class="mdi mdi-close" style="margin-left: 10px;"></span></label>

              @elseif($order->payment_status == 1)

              <label class="badge badge-success">{{__('app.Paid')}}<span class="mdi mdi-check" style="margin-left: 10px;"></span></label>

              @endif


              <p>{{__('app.price')}}: ${{$order->product->current_price}}</p>

              <p>{{__('app.shipping')}}: ${{$order->shipping}}</p>

              <p>{{__('app.quantity')}}: x({{$order->quantity}})</p>

              <p>{{__('app.price')}} + {{__('app.shipping')}}: ${{$order->paid_amount}}</p>
              
              <p>{{__('app.Total_Price')}}: {{($order->product->current_price * $order->quantity) + $order->shipping}} {{__('app.birr')}}</p>

            </div>


          </div>

        </div>

      </div>

    </div>

  </div>
  
</div>
      
<div class="message"></div>

@endsection