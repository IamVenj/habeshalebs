@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Products')}}</li>
    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.Create_Product')}}</span></li>
  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

		<a href="/view-products" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Products')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card ">

  <div class="card">
 
    <div class="card-body">
 
      <h4 class="card-title">{{__('app.Create_Product')}}</h4>

      
      <form class="forms-sample" id="productsForm" action="/create-product" method="POST" enctype="multipart/form-data">


        <div id="csrf-token" content="{{csrf_token()}}"></div>


        @include('post-login.partials.modal.color_modal')


      	<div class="form-group">

          <label for="product_name">{{__('app.Product_Name')}}</label>

          <input id="product_name" class="form-control" name="product_name" type="text" placeholder="{{__('app.Product_Name')}}">

        </div>

        <div class="form-group">

          <label for="sku">{{__('app.unique_code')}}</label>

          <input id="sku" class="form-control" name="sku" type="text" placeholder="{{__('app.unique_code')}}">

        </div>

        <div class="form-group">

          <label for="maxlength-textarea">{{__('app.Product_Description')}}</label>

          <textarea id="maxlength-textarea" name="slug" class="form-control" maxlength="2000" rows="5" placeholder="{{__('app.Product_Description')}}"></textarea>

      	</div>

        <div class="row">
          
          <div class="col-md-6">
            
            <div class="form-group">

              <label>{{__('app.Upload_Images')}}</label>

              <div class="input-group col-xs-12">

                <input type="file" name="image[]" id="uploadFile" class="" multiple>

              </div>

            </div>
      
            <div class="progress" id="progress" style="display: none;">
             
              <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="" aria-valuemax="100" style="width: 0%">
             
                0%
             
              </div>
            
            </div>

            <a class="btn btn-success btn-block" id="show-current-images" style="color: #fff; margin-bottom: 20px; display: none;" data-toggle="modal" href="#" data-target="<?= '#show-image';?>">{{__('app.show_images')}}</a>

      
            <span id="error_multiple_files"></span>

          </div>


          <div class="col-md-6">

            <div class="form-group">

              <label for="brand">{{__('app.select_pColor')}}</label><br/>

               <a class="btn btn-success" style="color: #fff;" data-toggle="modal" href="#" data-target="<?= '#select-color';?>">{{__('app.Select_Colors')}}<span class="mdi mdi-format-color-fill ml-2"></span></a>

              <span id="selected-colors"></span>

            </div>
            
            

          </div>

        </div>
        
        <div class="row">

          <div class="col-md-6">
            
      	    <div class="form-group">

              <label for="brand">{{__('app.Product_Brand')}}</label>

              <input id="brand" class="form-control" name="brand" type="text" placeholder="{{__('app.Product_Brand')}}">

            </div>

          </div>

          <div class="col-md-6">
            
            <div class="form-group">

              <label for="design">{{__('app.Product_styling')}}</label>

              <select class="form-control form-control-sm" name="design">
                <option selected="" disabled="">select products design style</option>
                <option value="handmade">Handmade</option>
                <option value="machine work">Machine Work</option>
              </select>

            </div>
            
          </div>

        </div>

        <div class="row">
          
          <div class="col-md-6">

            <div class="form-group">

              <label for="old_price">{{__('app.old_price')}}</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

              <input id="old_price" class="form-control" name="old_price" type="number" placeholder="{{__('app.Price_in_birr')}}">

            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">

              <label for="current_price">{{__('app.current_price')}}</label>

              <input id="current_price" class="form-control" name="current_price" type="number" placeholder="{{__('app.Price_in_birr')}}">

            </div>

          </div>

        </div>

        <div class="row">
          
          <div class="col-md-6">
            
            <div class="form-group">
              
              <label for="">{{__('app.Size')}}</label>

              <select class="form-control form-control-sm" name="product_size[]" multiple="">
                
                <option value="XS">Extra Small</option>
                
                <option value="S">Small</option>
                
                <option value="M">Medium</option>
                
                <option value="L">Large</option>
                
                <option value="XL">Extra Large</option>

                <option value="free size">Free Size</option>

              </select>

            </div>

          </div>

          <div class="col-md-6">            

            <div class="form-group">

              <label for="availability">{{__('app.how_many_available')}}?</label>

              <input id="availability" class="form-control" name="availability" type="number" min="1" placeholder="{{__('app.Number_of_stock')}}">

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-3">            

            <div class="form-group">

              <label for="weight">{{__('app.weight_in_kg')}}?</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

              <input id="weight" class="form-control" name="weight" min="0" type="number" placeholder="{{__('app.weight_in_kg')}}">

            </div>

          </div>

          <div class="col-md-3">            

            <div class="form-group">

              <label for="washcare">{{__('app.washcare')}}?</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

              <input id="washcare" class="form-control" name="washcare" type="text" placeholder="{{__('app.washcare')}}({{__('app.washcare_example')}})">

            </div>

          </div>

          <div class="col-md-3">            

            <div class="form-group">

              <label for="composition">{{__('app.composition')}}?</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

              <input id="composition" class="form-control" name="composition" type="text" placeholder="{{__('app.composition_example')}}">

            </div>

          </div>

          <div class="col-md-3">            

            <div class="form-group">

              <label for="lining_composition">{{__('app.lining_composition')}}?</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

              <input id="lining_composition" class="form-control" name="lining_composition" type="text" placeholder="{{__('app.composition_example')}}">

            </div>

          </div>

        </div>

        <div class="form-group">
  
          <label>{{__('app.Category')}}</label>
  
          <select class="form-control form-control-lg" name="category" id="category">
  
            <option selected disabled>{{__('app.Select_Category')}}</option>
 
            @foreach($categories as $category)
 
                  <option value="{{$category->id}}">{{$category->category_name}}</option>

            @endforeach

          </select>
  
        </div>


        <div class="form-group">
  
          <label>{{__('app.ProductsLocation')}}</label>
  
          <select class="form-control form-control-lg" name="product_location" id="product_location">
  
            <option selected disabled>{{__('app.Select_Location')}}</option>

            <option value="all">{{__('app.All')}}</option>
  
            @foreach($addresses as $address)

            <option value="{{$address->address}}">{{$address->address}}</option>

            @endforeach
  
          </select>
  
        </div>


        <hr>

        <div class="row">
          
          <div class="col-md-11">
            
              <button type="submit" class="btn btn-primary btn-block btn-product-create" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.Create')}} <span class="mdi mdi-creation"></span></button>

          </div>

          <div class="col-md-1">
            
              <span id="pending_status"></span>
            
          </div>


        </div>
    
      </form>
 
    </div>
 
  </div>

</div>

<div class="message"></div>

@endsection