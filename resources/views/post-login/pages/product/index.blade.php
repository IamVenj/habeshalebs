@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">

    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>

    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Products')}}</li>

    <li class="breadcrumb-item active" aria-current="page"><span>{{__('app.View_Products')}}</span></li>

  </ol>

</div>

<div class="col-12 grid-margin stretch-card">

		<a href="/create-product" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.Add_Products')}}<span class="mdi mdi-plus ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card">

	<div class="card">

	    <div class="card-body">

    		<div class="row">

    			@foreach($products as $product)

				<div class="col-md-4 mt-3">

  					<div class="card" style="border-top-left-radius: 20px; border-bottom-right-radius: 20px;" >

  						@if($product->productImage()->count() > 0)

                        <img class="card-img-top" id="image-public" style="height: 250px; object-fit: cover; border-top-left-radius: 20px; border-bottom-right-radius: 20px;" src="<?= Cloudder::show($product->productImage()->first()->image_public_id, array("quality" => "auto", "height" => 500, "width"=>500));?>" alt="{{$product->productImage()->first()->image_public_id}}">

                        @else

                        <div style="padding-top: 68%; border-top-left-radius: 20px; border-bottom-right-radius: 20px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></div>

                        @endif

                        <div class="card-body">

                        	
                            <h4 class="card-title mb-3" style="font-weight: bold;">{{$product->product_name}}</h4>

                            <div class="dropdown-divider"></div>

                            <p style="font-size: 14px;">{{Str::limit($product->slug, 50)}}</p>

                            <div class="dropdown-divider"></div>

                            <p class="mb-3 mt-3" style="text-transform: uppercase; background: #e8e8e850; padding:10px 0px 10px 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;"><span class="mdi mdi-view-dashboard mr-3"></span>{{$product->category()->first()->category_name}}</p>

                            <div class="dropdown-divider"></div>

                            <div class="row mt-3">
                            	
                            	<div class="col-md-6">
                            		
	                                <p class=""><span class="mdi mdi-tag mr-3"></span>{{$product->brand}}</p>

                            	</div>

                            	<div class="col-md-6">
                            		
                            		<p class=""><span class="mdi mdi-cash mr-3"></span>{{$product->current_price}}</p>
                            		
                            	</div>

                            </div>

                            <div class="dropdown-divider"></div>

                            <div class="row mt-3">
                                
                                <div class="col-md-6">
                                    
                                    <p class=""><span class="mdi mdi-map-marker-check mr-3"></span>{{$product->product_location}} @if($product->product_location == 'all') {{__('app.Stores')}} @else {{__('app.Store')}} @endif</p>

                                </div>

                                <div class="col-md-6">
                                    
                                    <p class=""><span class="mdi mdi-calendar-multiple mr-3"></span>{{$product->availability}}</p>

                                </div>

                            </div>

                            <div class="dropdown-divider"></div>

                            <div class="row mt-3">
                                
                                <div class="col-md-6">
                                    
                                    <p><span class="mdi mdi-barcode-scan mr-3"></span>{{$product->sku}}</p>

                                </div>

                                <div class="col-md-6">
                                    
                                    <p><span class="mdi mdi-account-multiple mr-3"></span>Visits: {{$product->visits}}</p>

                                </div>

                            </div>


                            <div class="dropdown-divider"></div>

                            @include('post-login.partials.modal.products-modal')

                            <div class="ticket-actions mb-3">

		                        <div class="btn-group dropdown">

		                          	<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                            	{{__('app.Manage')}}

		                          	</button>

		                          	<div class="dropdown-menu">

		                            	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-product'.$product->id;?>">

		                              		<i class="mdi mdi-pen mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Edit')}}</a>

		                            	<div class="dropdown-divider"></div>

		                            	<a class="dropdown-item" href="/{{$product->product_name}}/images/{{$product->id}}">

		                              		<i class="mdi mdi-image mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Manage_Images')}}</a>

                                        <div class="dropdown-divider"></div>

                                        <a class="dropdown-item" href="/my-reviews/{{$product->id}}/">

                                            <i class="mdi mdi-check mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Product_Reviews')}}</a>

                                        <div class="dropdown-divider"></div>

                                        <a class="dropdown-item" href="/{{$product->product_name}}/colors/{{$product->id}}">

                                            <i class="mdi mdi-format-color-fill mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Manage_Colors')}}</a>

		                            	<div class="dropdown-divider"></div>

		                            	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-product'.$product->id; ?>">

		                              		<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Delete')}}</a>

		                          	</div>

		                        </div>

		                    </div>

                            <p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 12px;"><i class="mdi mdi-clock"></i> {{$product->created_at->diffForHumans()}}</p> 

                        </div>

                    </div>

				</div>

				@endforeach

			</div>

            {{$products->links()}}

		</div>

    </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection