@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <ol class="breadcrumb breadcrumb-custom bg-inverse-primary">
 
    <li class="breadcrumb-item"><a href="/dashboard">{{__('app.home')}}</a></li>
 
    <li class="breadcrumb-item" style="color: #4d83ff;">{{__('app.Products')}}</li>
 
    <li class="breadcrumb-item"><a href="/view-products">{{__('app.View_Products')}}</a></li>
 
    <li class="breadcrumb-item active" aria-current="page"><span>{{$product_name}}</span></li>
 
  </ol>

</div>
	
<div class="col-10 grid-margin stretch-card">

	<a href="/view-products" style="text-decoration: none;"><button class="btn btn-success btn-block" style="border-bottom-right-radius: 20px; border-top-left-radius: 20px;">{{__('app.View_Products')}}<span class="mdi mdi-eye ml-2"></span></button></a>

</div>


<div class="col-12 grid-margin stretch-card">

	<div class="card">

	    <div class="card-body">

			<div class="row">
		                        
			    @foreach($productImages as $productImage)
			    
			    <div class='col-md-4'>

					<form action="/delete-product-image/{{$productImage->id}}" method="POST">

						@csrf

						@method("DELETE")
						
			        	<button class="close" type="submit" style="color: #000; cursor: pointer;"><span class="mdi mdi-close-circle" style="color: #000;"></span></button>
			        	
			        	<input type="hidden" name="product-image-publicId" value="{{$productImage->image_public_id}}">

					</form>

			        <img class="card-image loading" style='max-width: 100%; height: 300px; object-fit: cover; margin-bottom:20px; border-top-left-radius: 20px; border-bottom-right-radius: 20px; position: relative;' src="<?= Cloudder::show($productImage->image_public_id, array("version"=>$productImage->image_version, "quality" => "auto", "height" => 200, "width"=>800));?>" />

			        <div id="pending_status_edit"></div>

			        <form action="/update-product-image/{{$productImage->id}}" method="POST" enctype="multipart/form-data" id="update-product-image-form">

			        	@csrf

			        	@method('PATCH')

			        	<input type="hidden" name="product-image-publicId" value="{{$productImage->image_public_id}}">
			        	
			        	<input type="hidden" name="product-id" value="{{$product_id}}">

			        	<div class="row">
			        		
							<div class="col-md-10">
								
						        <div class="form-group">
						                
						            <input type="file" name="replaced_image" class="file-upload-default">
						          
						            <div class="input-group col-xs-12">
						          
						                <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="Upload Image">
						          
						                <span class="input-group-append">
						          
						                    <button class="file-upload-browse btn btn-success" type="button">{{__('app.Replace_Image')}}</button>
						          
						                </span>

						            </div>
						          
						        </div>

							</div>	        		

							<div class="col-2">
		        
						        <button class="btn btn-warning btn-block save-image-update" type="submit" ><span class="mdi mdi-check"></span></button>

							</div>

			        	</div>

			    	</form>

			    </div>

			    @endforeach

			    @include('post-login.partials.modal.products-image-modal')

			    @if($productImages->count() < 6)

			    <a class="btn btn-primary" data-toggle="modal" href="#" data-target="#add-image"><span class="mdi mdi-plus"></span></a>

			    @endif


			</div>

		</div>

	</div>
	
</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection