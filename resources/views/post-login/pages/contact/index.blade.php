@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <div class="card">

      <div class="card-body">

        <h4 class="card-title">

          {{__('app.contact')}}

        </h4>

        <div class="row">

            <div class="col-12">

              <div class="table-responsive">

                <table class="order-listing table">

                  <thead>

                      <tr>

                          <th>#</th>

                          <th style="font-size: 18px;">Name</th>

                          <th style="font-size: 18px;">email</th>

                          <th style="font-size: 18px;">message</th>
                        
                      </tr>

                  </thead>

                  <tbody>

                      <?php $count = 0;?>

                      @foreach($contacts as $contact)
                  
                      <?php $count = $count + 1;?>
                    
                      <tr>

                        <td>{{$count}}</td>

                        <td style="font-size: 15px;">{{$contact->fullname}}</td>

                        <td style="font-size: 15px;">{{$contact->email}}</td>

                        <td style="font-size: 15px;">{{$contact->message}}</td>
                      
                      </tr>

                      @endforeach

                  </tbody>

                </table>

              </div>

            </div>

        </div>

      </div>

  </div>

</div>




@endsection