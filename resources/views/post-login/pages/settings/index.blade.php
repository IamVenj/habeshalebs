@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <div class="card">

    <div class="card-body">

      <h4 class="card-title">{{__('app.Settings')}}</h4>

      <form class="forms-sample" action="/settings" method="post" enctype="multipart/form-data">

        @csrf

        @method('PATCH')

        <div class="form-group">
        
            <label for="name" style="font-size: 15px;">{{__('app.Company_Name')}} <small style="color: red;">*</small></label>
        
            <input type="text" class="form-control" name="company_name" id="name" placeholder="{{__('app.Company_Name')}}" value="{{$setting->company_name}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
        
            <label for="about_company" style="font-size: 15px;">{{__('app.company_slug')}} <small style="color: red;">*</small></label>
        
            <textarea class="form-control" name="slug" id="about_company" placeholder="{{__('app.company_slug')}}" style="font-size: 15px; " rows="7">{{$setting->slug}}</textarea>
        
        </div>

        <div class="form-group">
        
            <label for="location" style="font-size: 15px;">{{__('app.Location')}} <small style="color: red;">*</small></label>
        
            <input type="text" name="location" class="form-control" id="location" placeholder="{{__('app.Location')}}" value="{{$setting->location}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
        
            <label for="Phone_Number" style="font-size: 15px;">{{__('app.phone_number')}}</label>
        
            <input type="text" name="phone_number" class="form-control" id="Phone_Number" placeholder="{{__('app.phone_number')}}" value="{{$setting->phone_number}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
        
            <label for="customer_support" style="font-size: 15px;">{{__('app.CustomerSupport')}}</label>
        
            <input type="text" name="customer_support" class="form-control" id="customer_support" placeholder="{{__('app.customer_support')}}" value="{{$setting->customer_support}}" style="font-size: 15px; ">
        
        </div>

        <div class="form-group">
         
            <label for="email" style="font-size: 15px;">{{__('app.email')}}</label>
        
            <input type="email" name="email" value="{{$setting->email}}" class="form-control" id="email" placeholder="{{__('app.email')}}" style="font-size: 15px; ">
        
        </div>

        <div class="row">

          <div class="col-md-3">
            
            <div class="form-group">
             
                <label for="charge" style="font-size: 15px;">{{__('app.charge_per_percent')}}?</label>
            
                <input type="number" step="0.01" name="charge" value="{{$setting->charge}}" class="form-control" id="charge" placeholder="%" style="font-size: 15px; ">
            
            </div>

          </div>

          <div class="col-md-3">
            
            <div class="form-group">
             
                <label for="delivery_price" style="font-size: 15px;">{{__('app.delivery_price')}}</label>
            
                <input type="number" name="delivery_price" value="{{$setting->delivery_price}}" class="form-control" placeholder="{{__('app.delivery_price')}}" style="font-size: 15px; ">
            
            </div>

          </div>

          <div class="col-md-3">
            
            <div class="form-group">
             
                <label for="item_amount_flat_rate" style="font-size: 15px;">{{__('app.items_resulting_in_increase_of_flat_rate')}}</label>
            
                <input type="number" name="item_amount_flat_rate" value="{{$setting->item_amount_flat_rate}}" min="1" class="form-control" placeholder="{{__('app.items_resulting_in_increase_of_flat_rate')}}" style="font-size: 15px; ">
            
            </div>

          </div>

          <div class="col-md-3">
            
            <div class="form-group">
             
                <label for="item_amount_flat_rate" style="font-size: 15px;">Delivery Time</label>
            
                <input type="text" name="delivery_time" value="{{$setting->delivery_time}}" class="form-control" placeholder="Delivery Time" style="font-size: 15px; ">
            
            </div>

          </div>
          
        </div>

        <div class="row">

          <div class="col-md-3">

            <div class="form-group">

                <label for="facebook" style="font-size: 15px;">{{__('app.facebook')}}</label>

                <input type="facebook" name="facebook" value="{{$setting->facebook}}" class="form-control" id="facebook" placeholder="{{__('app.facebook')}}" style="font-size: 15px; ">

            </div>  

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="twitter" style="font-size: 15px;">{{__('app.twitter')}}</label>

                <input type="twitter" name="twitter" value="{{$setting->twitter}}" class="form-control" id="twitter" placeholder="{{__('app.twitter')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="google_plus" style="font-size: 15px;">{{__('app.Google_plus')}}</label>

                <input type="google_plus" name="google_plus" value="{{$setting->google_plus}}" class="form-control" id="google_plus" placeholder="{{__('app.Google_plus')}}" style="font-size: 15px; ">

            </div>

          </div>

          <div class="col-md-3">

            <div class="form-group">

                <label for="linked_in" style="font-size: 15px;">{{__('app.linked_in')}}</label>

                <input type="linked_in" name="linked_in" value="{{$setting->linked_in}}" class="form-control" id="linked_in" placeholder="{{__('app.linked_in')}}" style="font-size: 15px; ">

            </div>

          </div>
          
        </div>

        <div class="form-group">
        
            <label for="about_company" style="font-size: 15px;">Message For delivery email <small style="color: red;">*</small></label>
        
            <textarea class="form-control" name="delivery_email" id="about_company" placeholder="Message For delivery email" style="font-size: 15px; " rows="7">{{$setting->delivery_email}}</textarea>
        
        </div>

        <div class="row">

          <div class="col-md-6">


            <div class="form-group">

              <label for="logo" style="font-size: 15px;">{{__('app.Logo')}} <small style="color: red;">*</small></label>

              <input type="file" name="logo" class="file-upload-default">

              <div class="input-group col-xs-12">

                <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.Upload_Logo')}}">

                <span class="input-group-append">

                  <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Select_Logo')}}</button>

                </span>

              </div>

            </div>



            <label style="font-size: 15px; font-weight: bold">{{__('app.Current_Logo')}} <i class="mdi mdi-arrow-down"></i></label>

            <div class="form-group">

                <img src="{{URL::asset('storage/uploads/logo.png')}}" class="mt-4" style="width: 300px; height: 200px; object-fit: contain">

            </div>

          </div>

          <div class="col-md-6">

            <div class="form-group">

              <label for="favicon" style="font-size: 15px;">{{__('app.Favicon')}} <small style="color: red;">*</small></label>

              <input type="file" name="favicon" class="file-upload-default">

              <div class="input-group col-xs-12">

                <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.Upload_Favicon')}}">

                <span class="input-group-append">

                  <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Select_Favicon')}}</button>

                </span>

              </div>

            </div>

            <label style="font-size: 15px; font-weight: bold">{{__('app.Current_Favicon')}} <i class="mdi mdi-arrow-down"></i></label>

            <div class="form-group">

                <img src="{{URL::asset('storage/uploads/favicon.png')}}" class="mt-4" style="width: 300px; height: 200px; object-fit: contain;">
   
            </div>
   
          </div>
   
        </div>
        
        <button type="submit" class="btn btn-primary mr-2">{{__('app.Update')}}</button>

      </form>

    </div>

  </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection