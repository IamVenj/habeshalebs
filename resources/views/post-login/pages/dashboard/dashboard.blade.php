@extends('post-login.index.index')

@section('content')

<div class="row">

	<div class="col-md-12 grid-margin">

		<div class="d-flex justify-content-between flex-wrap">

		    	<div class="d-flex align-items-end flex-wrap">

		    		<div class="mr-md-3 mr-xl-5">

		        		<h2>{{__('app.Welcome_back')}}, @if(!is_null(auth()->user()->name)) {{auth()->user()->name}} @else {{auth()->user()->shop_name}} @endif</h2>

		        		<p class="mb-md-0">{{__('app.analytics_statement')}}.</p>

	      		</div>

	      		<div class="d-flex">

		        		<i class="mdi mdi-home text-muted hover-cursor"></i>

		        		<p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;{{__('app.Dashboard')}}&nbsp;/&nbsp;</p>

		        		<p class="text-primary mb-0 hover-cursor">{{__('app.Analytics')}}</p>

	      		</div>

		    	</div>

	  	</div>

	</div>

</div>
	
@if(auth()->user()->role == 2)

<div class="row">

	<div class="col-md-6 col-xl-3 grid-margin stretch-card">

	  	<div class="card">

	  		<div id="cashIncomeCarousel" class="carousel slide card-carousel" data-ride="carousel">

	      		<div class="carousel-inner">
	      
	      			<div class="carousel-item active">
	      				
			      		<div class="card-body border-bottom">

			            		<p class="card-title">{{__('app.Cash_sales')}}</p>

			      	    	</div>

			          		<div class="card-body">

			            		<div class="d-flex align-items-center justify-content-between mb-3">

			            			<div>

					            		<p>{{__('app.Sales_Comparison')}}</p>

						          	</div>

					          		<div>

						            	<div class="icon-box-primary icon-box-lg">

						                  	<i class="mdi mdi-wallet"></i>

						                	</div>

						          	</div>

			  				</div>

					        	<p class="text-muted mb-0">

					        		This is an analytics where {{auth()->user()->shop_name}} can view Comparison between Sales last month and this month</p>

			  			</div>

	      			</div>

	      			@foreach($cashSales as $key => $value)

	      			<div class="carousel-item">
	      				
			      		<div class="card-body border-bottom">

			            		<p class="card-title">{{__('app.Cash_sales')}}</p>
				            
				            	<div class="row">

				              		<div class="col-6">

					                		<canvas class="cash-sales-chart-b"></canvas>

					              	</div>

				              		<div class="col-6">

				                			<div class="d-flex align-items-center ml-2">

					                  		<h2 class="font-weight-bold mb-0 mr-1">{{$cashSalesPercentage}}%</h2>

					                  		@if($cashSalesStatus == 'decrease')

					                  		<i class="mdi mdi-chevron-down text-info icon-md"></i>

					                  		@else

					                  		<i class="mdi mdi-chevron-up text-info icon-md"></i>

					                  		@endif

						                	</div>

				      	        	</div>

				            	</div>

			      	    	</div>

			          		<div class="card-body">

			            		<div class="d-flex align-items-center justify-content-between mb-3">

			            			<div>

					            		<p>{{__('app.Sales_last_month')}}</p>

					                		<h2 class="mb-0">{{$value}}</h2>

						          	</div>

					          		<div>

						            	<div class="icon-box-primary icon-box-lg">

						                  	<i class="mdi mdi-wallet"></i>

						                	</div>

						          	</div>

			  				</div>

					    		<h5>{{__('app.Gross_sales_of')}} {{$key}}</h5>

			  			</div>

	      			</div>

	      			@endforeach

	      		</div>

	      		<a class="carousel-control-prev bg-white" href="#cashIncomeCarousel" role="button" data-slide="prev">

		        		<span class="carousel-control-prev-icon" aria-hidden="true"></span>

		        		<span class="sr-only">{{__('app.Previous')}}</span>

	      		</a>

	      		<a class="carousel-control-next bg-white" href="#cashIncomeCarousel" role="button" data-slide="next">

			        	<span class="carousel-control-next-icon" aria-hidden="true"></span>

			        	<span class="sr-only">{{__('app.Next')}}</span>

		      	</a>

	      	</div>

    		</div>
	        
	</div>
	
	<div class="col-md-6 col-xl-3 grid-margin stretch-card">

	  	<div class="card">

	    		<div id="monthlyIncomeCarousel" class="carousel slide card-carousel" data-ride="carousel">

	      		<div class="carousel-inner">
	      
	      			<div class="carousel-item active">

	      				<div class="card-body border-bottom">

		            			<p class="card-title">{{__('app.View_Your_Monthly_income')}}</p>

	          				</div>

	          				<div class="card-body">

	            				<div class="d-flex align-items-center justify-content-between mb-3">

	              					<div>

						                	<div class="icon-box-warning icon-box-lg">

						                  	<i class="mdi mdi-credit-card"></i>

						                	</div>

					              	</div>

	            				</div>

	            				<p class="text-muted mb-0">
	            					
	            					This is an analytics where {{auth()->user()->shop_name}} can view monthly income. </p>

	          				</div>

	      			</div>

	      			@foreach($monthlyIncome as $key => $value)

		        		<div class="carousel-item">

		          			<div class="card-body border-bottom">

		            			<p class="card-title">{{__('app.Monthly_income')}}</p>

	            				<div class="row">

	            					@if(count($monthlyIncome) > 1)

	              					<div class="col-12">

	                						<canvas class="monthly-income-chart"></canvas>

	              					</div>

	              					@endif

	            				</div>

	          				</div>

	          				<div class="card-body">

	            				<div class="d-flex align-items-center justify-content-between mb-3">

	              					<div>

						                	<p>{{__('app.Sales_income')}}</p>

						                	<h2 class="mb-0">{{$value}} {{__('app.birr')}}</h2>

				              		</div>

	              					<div>

					                	<div class="icon-box-warning icon-box-lg">

					                  	<i class="mdi mdi-credit-card"></i>

					                	</div>

					              	</div>

	            				</div>

					            <h5>Gross sales of {{$key}}</h5>

	          				</div>

	        			</div>

	        			@endforeach

	      		</div>

	      		<a class="carousel-control-prev bg-white" href="#monthlyIncomeCarousel" role="button" data-slide="prev">

		        		<span class="carousel-control-prev-icon" aria-hidden="true"></span>

		        		<span class="sr-only">Previous</span>

	      		</a>

	      		<a class="carousel-control-next bg-white" href="#monthlyIncomeCarousel" role="button" data-slide="next">

			        	<span class="carousel-control-next-icon" aria-hidden="true"></span>

			        	<span class="sr-only">Next</span>

		      	</a>

	    		</div>    

	  	</div>

	</div>

	<div class="col-md-6 col-xl-3 grid-margin stretch-card">

	  	<div class="card">

	    		<div id="yearlySalesCarousel" class="carousel slide card-carousel" data-ride="carousel">

	      		<div class="carousel-inner">

	        			<div class="carousel-item active">

	        				<div class="card-body border-bottom">

	            				<p class="card-title">{{__('app.View_Your_Yearly_sales')}}</p>

          					</div>

          					<div class="card-body">

	            				<div class="d-flex align-items-center justify-content-between mb-3">

	              					<div>
	  
	                						<div class="icon-box-info icon-box-lg">

	                  						<i class="mdi mdi-cart"></i>

	                						</div>

	              					</div>

	            				</div>

					            <p>This is an analytics where {{auth()->user()->shop_name}} can view yearly sales. </p>

	          				</div>

	        			</div>

	        			@foreach($yearlyIncome as $key => $value)

	        			<div class="carousel-item">

	          				<div class="card-body border-bottom">

	            				<p class="card-title">{{__('app.Yearly_sales')}}</p>

            					<div class="row">

            						@if(count($yearlyIncome) > 1)

              						<div class="col-12">

          								<canvas class="yearly-sales-chart"></canvas>

              						</div>              				

              						@endif		

            					</div>

          					</div>

          					<div class="card-body">

	            				<div class="d-flex align-items-center justify-content-between mb-3">

	              					<div>

	                						<p>{{__('app.Purchases')}}</p>
	     
	                						<h2 class="mb-0">{{$value}} {{__('app.birr')}}</h2>

	              					</div>

	              					<div>
	  
	                						<div class="icon-box-info icon-box-lg">

	                  						<i class="mdi mdi-cart"></i>

	                						</div>

	              					</div>

	            				</div>

					            <h5>{{__('app.Gross_sales_of')}} {{$key}}</h5>

	          				</div>

	        			</div>

	        			@endforeach

	      		</div>

	      		<a class="carousel-control-prev bg-white" href="#yearlySalesCarousel" role="button" data-slide="prev">

	        			<span class="carousel-control-prev-icon" aria-hidden="true"></span>

	        			<span class="sr-only">{{__('app.Previous')}}</span>

	      		</a>

	      		<a class="carousel-control-next bg-white" href="#yearlySalesCarousel" role="button" data-slide="next">

	        			<span class="carousel-control-next-icon" aria-hidden="true"></span>

	        			<span class="sr-only">{{__('app.Next')}}</span>

	      		</a>

	    		</div>    

	  	</div>

	</div>

	<div class="col-md-6 col-xl-3 grid-margin stretch-card">

	  	<div class="card">

	    		<div id="dailyDepositsCarousel" class="carousel slide card-carousel" data-ride="carousel">

	      		<div class="carousel-inner">

	       			<div class="carousel-item active">

	       				<div class="card-body border-bottom">

	            				<p class="card-title">{{__('app.View_your_Daily_deposits')}}</p>

	          				</div>

	          				<div class="card-body">

	            				<div class="d-flex align-items-center justify-content-between mb-3">

	              					<div>

	                						<div class="icon-box-success icon-box-lg">

	                  						<i class="mdi mdi-calendar-heart"></i>

	                						</div>

	              					</div>

	            				</div>

	            				<p class="text-muted mb-0">

	            					This is an analytics where {{auth()->user()->shop_name}} can view daily deposits. </p>
	  
	          				</div>
	  
	       			</div>
	  
	       			@foreach($dailyIncome as $key => $value)
	  
	       			<div class="carousel-item">
	  
	          				<div class="card-body border-bottom">
	  
	            				<p class="card-title">{{__('app.Daily_deposits')}}</p>
	  
	            				<div class="row">
	  
	              					<div class="col-12">
	  
	                						<canvas class="_daily-deposits-chart_"></canvas>
	  
	              					</div>
	  
	            				</div>
	  
	          				</div>
	  
	          				<div class="card-body">
	  
	            				<div class="d-flex align-items-center justify-content-between mb-3">
	  
	              					<div>
	  
	                						<p>{{__('app.Daily_deposits')}}</p>
	  
	                						<h2 class="mb-0">{{$value}} {{__('app.birr')}}</h2>
	  
	              					</div>
	  
	              					<div>
	  
	                						<div class="icon-box-success icon-box-lg">
	  
	                  						<i class="mdi mdi-calendar-heart"></i>
	  
	                						</div>
	  
	              					</div>
	  
	            				</div>
	  
	            				<h5>{{__('app.Gross_sales_of')}} {{$key}}</h5>
	  
	          				</div>
	  
	        			</div>
	  
	        			@endforeach
	  
	      		</div>
	  
	      		<a class="carousel-control-prev bg-white" href="#dailyDepositsCarousel" role="button" data-slide="prev">
	  
	        			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  
	        			<span class="sr-only">{{__('app.Previous')}}</span>
	  
	      		</a>
	  
	      		<a class="carousel-control-next bg-white" href="#dailyDepositsCarousel" role="button" data-slide="next">
	  
	        			<span class="carousel-control-next-icon" aria-hidden="true"></span>
	  
	        			<span class="sr-only">{{__('app.Next')}}</span>
	  
	      		</a>
	  
	    		</div>    
	  
	  	</div>

	</div>

</div>

@endif

@if(auth()->user()->role == 1)

<div class="row">

	<div class="col-md-4 grid-margin stretch-card">

	  	<div class="card border-0 bg-danger text-white">

	    		<div id="customers-carousel" class="carousel slide card-carousel" data-ride="carousel">

	      		<div class="carousel-inner">

	        			<div class="carousel-item active">

	          				<div class="card-body pb-0">

	            				<p class="card-title text-white">{{__('app.customers')}}</p>

	            				<h1>{{$total_customers}}</h1>

	          					<p>{{__('app.This_is_the_list_of_total_customers_for')}} {{auth()->user()->name}}</p>
	          				
	          				</div>


          					<canvas height="350" id="customers-chart-a"></canvas>

	        			</div>

	        			<div class="carousel-item">

	          				<div class="card-body pb-0">

	            				<p class="card-title text-white">{{__('app.Vendors')}}</p>

	            				<h1>{{$total_vendors}}</h1>

	          				</div>

	          				<canvas height="367" id="customers-chart-b"></canvas>

	        			</div>

      			</div>

	      		<a class="carousel-control-prev control-light" href="#customers-carousel" role="button" data-slide="prev">

	        			<span class="carousel-control-prev-icon" aria-hidden="true"></span>

	        			<span class="sr-only">{{__('app.Previous')}}</span>

	      		</a>

	      		<a class="carousel-control-next control-light" href="#customers-carousel" role="button" data-slide="next">

	        			<span class="carousel-control-next-icon" aria-hidden="true"></span>

	        			<span class="sr-only">{{__('app.Next')}}</span>

	      		</a>

	    		</div>  

	  	</div>

	</div>

	<div class="col-md-8 grid-margin stretch-card">

	  	<div class="card">

	    		<div class="card-body">

	      		<p class="card-title">{{__('app.Total_sales')}}</p>

	      		<h1>{{$total_price}} {{__('app.birr')}}</h1>

	      		<h4>{{__('app.Gross_sales_over_the_years')}}</h4>

	      		<div id="total-sales-chart-legend"></div>                  

	    		</div>

	    		<canvas id="total-sales-chart"></canvas>

	  	</div>

	</div>

</div>

@endif	

@endsection