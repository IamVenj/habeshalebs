@extends('post-login.index.index')

@section('content')


<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Debts')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>#</th>

                  <th>{{__('app.Debt')}}</th>

                  <th>{{__('app.Order')}} #</th>

                  <th>{{__('app.Item_SKU')}}</th>

                  <th>{{__('app.Debt_Status')}}</th>
                  
              </tr>

            </thead>

            <tbody>

              <?php $count = 0;?>

              @foreach($debts as $debt)

              <?php $count = $count + 1;?>

              <tr>

                <td>{{$count}}</td>

                <td>{{$debt->debt}}</td>

                <td>{{$debt->order()->first()->order_code}}</td>
                
                <td>{{$debt->order()->first()->product()->first()->sku}}</td>
                
                <td>@if($debt->debt_status == 0) <div class="badge badge-danger">{{__('app.Unpaid')}}</div> @else <div class="badge badge-success">{{__('app.Paid')}}</div> @endif</td>

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

        <h1>{{__('app.Total_Debt')}}: {{$debts_sum}} {{__('app.birr')}}</h1>

      </div>

    </div>

  </div>

</div>

@endsection