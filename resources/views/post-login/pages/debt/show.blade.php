@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Debts')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>#</th>

                  <th>{{__('app.Debt')}}</th>

                  <th>{{__('app.Debt_Status')}}</th>

                  <th></th>
                  
              </tr>

            </thead>

            <tbody>

              <?php $count = 0;?>

              @foreach($debts as $debt)

              <?php $count = $count + 1;?>

              <tr>

                <td>{{$count}}</td>

                <td>{{$debt->debt}}</td>
                
                <td>@if($debt->debt_status == 0) <div class="badge badge-danger">{{__('app.Unpaid')}}</div> @else <div class="badge badge-success">{{__('app.Paid')}}</div> @endif</td>

                <td>
                  @if($debt->debt_status == 0)
                  <form action="/debt/item/{{$debt->id}}" method="post">
                    @csrf
                    @method('PATCH')
                    <button class="main-btn" style="font-size: 10px; border-radius: 50%;"><i class="mdi mdi-check"></i></button>
                  </form>
                  @endif

                </td>

              </tr>

              @endforeach

            </tbody>

          </table>

        </div>

        <h1>{{__('app.Total_Debt')}}: {{$debts_sum}} {{__('app.birr')}}</h1>

        @if($debtStatus > 0)

        <p style="font-size:20px;">{{__('app.check_debt_status')}} <i class="mdi mdi-chevron-down"></i></p>

        <form action="/debt/{{$id}}" method="post">

          @csrf

          @method('PATCH')
          
          <button class="main-btn" style="font-size: 30px; border-radius: 50%;"><i class="mdi mdi-check"></i></button>

        </form>

        @endif

      </div>

    </div>

  </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection