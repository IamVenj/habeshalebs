@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Debts')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <table class="order-listing table">

            <thead>

              <tr>

                  <th>#</th>

                  <th>{{__('app.Vendor')}}</th>

                  <th></th>
                  
              </tr>

            </thead>

            <tbody>

              <?php $count = 0;?>

              @foreach($debts as $debt => $value)

              <?php

              $vendor = App\User::find($debt);

              ?>
              
              <tr>

                <?php $count = $count + 1;?>

                <td>{{$count}}</td>

                <td>{{$vendor->shop_name}}</td>
                                                
                <td>
                  
                  <a href="/debt/{{$vendor->id}}">

                    <button class="main-btn" style="border-radius: 50%;">

                      <i class="mdi mdi-eye"></i>

                    </button>

                  </a>

                </td>

              </tr>
              
              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

@endsection