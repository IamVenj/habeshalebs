@extends('post-login.index.index')

@section('content')

@include('post-login.partials.modal.category_modal')

<div class="row">

  <div class="col-12 grid-margin stretch-card">
 
    <div class="card">
 
      <div class="card-body">
 
        <h4 class="card-title">{{__('app.Create_Category')}}</h4>

        <p class="card-description">
          
          {{__('app.category_desc')}}

        </p>
 
        <form class="forms-sample" id="categoryForm" action="/create-category" method="POST" enctype="multipart/form-data">

          <input type="hidden" name="csrf-token" value="{{csrf_token()}}">
          
          <div class="form-group">
 
            <label for="categoryName">{{__('app.Category_Name')}}</label>
 
            <input id="categoryName" class="form-control" name="categoryName" placeholder="{{__('app.Category_Name')}}" type="text">
 
          </div>

          <div class="form-group">
          
            <label>{{__('app.Category_Image')}}</label>
          
            <input type="file" id="image-category-upload" name="image" class="file-upload-default">
          
            <div class="input-group col-xs-12">
          
              <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="{{__('app.Upload_Image')}}">
          
              <span class="input-group-append">
          
                <button class="file-upload-browse btn btn-primary" type="button">{{__('app.choose_image')}}</button>
          
              </span>

            </div>
          
          </div>

          <div class="row">
            
            <div class="col-md-11">
              
              <button type="submit" class="btn btn-success button-create-category">{{__('app.Create')}}</button>

            </div>

            <div class="col-md-1">
              
              <span id="pending_status"></span>
              
            </div>

          </div>

        </form>
 
      </div>
 
    </div>
 
  </div>

</div>

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Main_Categories')}}</h4>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <div id="dtable"></div>

        </div>

      </div>

    </div>

  </div>

</div>



<div class="message"></div>

@endsection