@extends('post-login.index.index')

@section('content')

<div class="row">

  <div class="col-12 grid-margin">

    <div class="card" style="border-bottom-left-radius: 500px; border-top-right-radius: 500px;">

      <div class="card-body">

        <h4 class="card-title">{{__('app.fill_profile')}}</h4>

        <form id="accountForm" action="/wizard" method="post" enctype="multipart/form-data">

          <input type="hidden" name="csrf-token" value="{{csrf_token()}}">

          <input type="hidden" name="role" value="{{auth()->user()->role}}">

          <div>

            <h3>{{__('app.profile')}}</h3>
            
            <section>
            
              <div class="picture-container">
              
                <div class="picture">
            
                  <img src="@if(auth()->user()->role == 3){{URL::asset('images/default-avatar.png')}}@elseif(auth()->user()->role == 2){{URL::asset('images/default-avatar.png')}}@endif" class="picture-src" id="profilePicturePreview" title=""/>
                          
                </div>
                            
              </div>

              <div class="form-group">
          
                <label>{{__('app.profile_image')}} *</label>
              
                <input type="file" name="image" class="file-upload-default">
              
                <div class="input-group col-xs-12">
              
                  <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.choose_image')}}">
              
                  <span class="input-group-append">
              
                    <button class="file-upload-browse btn btn-primary" type="button">{{__('app.choose_image')}}</button>
              
                  </span>

                </div>
                
                <div id="image_error"></div>

              </div>

              @if(auth()->user()->role == 2)

              <div class="form-group">
            
                <label for="shop_name">{{__('app.shop_name')}} *</label>
            
                <input id="shop_name" name="shop_name" type="text" value="{{auth()->user()->shop_name}}" class="required form-control" placeholder="{{__('app.shop_name')}}">
                            
              </div>


              <div class="form-group">

                <label for="maxlength-textarea">{{__('app.about_shop')}} *</label>

                <textarea id="maxlength-textarea" name="shop_slug" class="form-control" maxlength="400" rows="5" placeholder="{{__('app.about_shop')}}"></textarea>
              
              </div>

              @endif
            
            </section>
            

            <h3>{{__('app.address')}}</h3>
            
            <section>
              
              <div class="form-group">
            
                <label for="phone">{{__('app.phone_number')}}</label>
            
                <input id="phone" name="phone_number" type="text" class="required form-control" placeholder="{{__('app.phone_number')}}">
            
              </div>

              <div class="form-group" id="address_main">
            
                <label for="address1">{{__('app.address')}} 1</label>
            
                <div class="row">

                  <div class="col-md-11">
                    
                    <input id="address" name="address1" type="text" class="required form-control address_uno" placeholder="{{__('app.address')}} 1">
                  
                  </div>

                  <div class="col-md-1">
                  
                    <button class="btn btn-warning" type="button" id="search_map1"><i class="mdi mdi-map-search"></i></button>
                    
                  </div>
                
                </div>


                <div id="map" class="show-map2"></div>

                <input type="hidden" name="lat" id="lat">

                <input type="hidden" name="lng" id="lng">

              </div>


              @if(auth()->user()->role != 3)

              <div id="address2"></div>

              <div class="row">

                <div class="col-3">
                  
                  <button type="button" class="btn btn-warning" id="more_address">{{__('app.add_more')}} <i class="mdi mdi-plus"></i></button>
                
                </div>
                
                <div class="col-9">
                  
                  <div class="form-group" id="address_inputs" style="display: none;">
              
                    <select class="form-control form-control-lg" name="address_input" id="address_input">
            
                      <option selected disabled>{{__('app.select_address')}}</option>
            
                      <option value="2">1</option>

                      <option value="3">2</option>
                      
                      <option value="4">3</option>
                      
                      <option value="5">4</option>
                      
                      <option value="6">5</option>
            
                    </select>
            
                  </div>

                </div>

              </div>

              @endif
            
            </section>

            <h3>{{__('app.finish')}}</h3>
            
            <section>

              <div class="form-check">
            
                <label class="form-check-label">
            
                  <input class="checkbox" name="newsletter" type="checkbox">
            
                  {{__('app.revieve_news')}}
            
                </label>
            
              </div>

              <div class="form-check">
            
                <label class="form-check-label">
            
                  <input class="checkbox" name="agreement" type="checkbox">
            
                  {{__('app.terms_conditions')}}
            
                </label>
            
              </div>

              <div id="agreement_error"></div>

              <button type="submit" id="submit-wizard-btn" class="btn btn-primary">{{__('app.Submit')}}</button>
            
            </section>

            <div id="pending_status"></div>

          </div>

        </form>

      </div>

    </div>

  </div>

</div>

<div class="message"></div>

@endsection    
