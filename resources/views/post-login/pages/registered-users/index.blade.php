@extends('post-login.index.index')

@section('content')

<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Type_of_Users')}}</h4>

    <div class="row">

      <div class="col-12">

        <form method="post" action="/users" id="userForm">

          <div id="csrf-token" content="{{csrf_token()}}"></div>

          <div class="form-group">
  
            <label>{{__('app.Users')}}</label>
    
            <select class="form-control form-control-lg" name="user" id="user_list">
        
              <option value="2">{{__('app.Vendors')}}</option>
    
              <option value="3">{{__('app.customers')}}</option>
    
            </select>
    
          </div>

        </form>

      </div>

    </div>

  </div>

</div>


<div class="card">

  <div class="card-body">

    <h4 class="card-title">{{__('app.Users')}}</h4>
       
    <div id="pending-status"></div>

    <div class="row">

      <div class="col-12">

        <div class="table-responsive">

          <div class="table-data"></div>
                      
        </div>

      </div>

    </div>

  </div>

</div>

<div class="message"></div>


@endsection