@extends('post-login.index.index')

@section('content')

@include('post-login.partials.modal.account-modal')



<div class="col-12 grid-margin stretch-card">

	<div class="card">

    	<div class="card-body">

	    	<h4 class="card-title">{{__('app.my_account')}}</h4>

	    	@if(auth()->user()->role == 2)

	    	<div class="row mb-3">
		        	
	        	<div class="col-md-4">
	        		
	        		<a data-toggle="modal" href="#" data-target="#profile-pic-update" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">{{__('app.change_pic')}}</button></a>

	        	</div>

	        	<div class="col-md-4">
	        		
	        		<a href="#" data-toggle="modal" href="#" data-target="#delivery-option-update" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">{{__('app.change_delivery_option')}}</button></a>

	        	</div>	

	        	<div class="col-md-4">
	        		
	        		<a href="#" data-toggle="modal" href="#" data-target="#upassword-change" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">{{__('app.Change_Password')}}</button></a>

	        	</div>	       	

	        </div>

	        @endif

	        @if(auth()->user()->role == 1)

	        <div class="row mb-3">
		        	
	        	<div class="col-md-6">
	        		
	        		<a data-toggle="modal" href="#" data-target="#profile-pic-update" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-top-left-radius: 30px; border-bottom-right-radius: 30px;">{{__('app.change_pic')}}</button></a>

	        	</div>

	        	<div class="col-md-6">
	        		
	        		<a href="#" data-toggle="modal" href="#" data-target="#upassword-change" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-success btn-block" style="border-bottom-right-radius: 30px; border-top-left-radius: 30px;">{{__('app.Change_Password')}}</button></a>

	        	</div>		        	

	        </div>

	        @endif

	      	<form class="forms-sample" action="/my-account" method="post">

	        	@csrf

	        	@method('PATCH')

	        	@if(auth()->user()->role == 1)

	        	<div class="form-group">
         
		            <label for="name" style="font-size: 15px;">{{__('app.Name')}}</label>
		        
		            <input type="text" name="name" value="{{$user->firstname}}" class="form-control" id="name" placeholder="{{__('app.Name')}}" style="font-size: 15px; ">
		        
		        </div>

		        @else

		        <div class="form-group">
         
		            <label for="shop_name" style="font-size: 15px;">{{__('app.shop_name')}}</label>
		        
		            <input type="text" name="shop_name" value="{{$user->shop_name}}" class="form-control" id="shop_name" placeholder="{{__('app.shop_name')}}" style="font-size: 15px; ">
		        
		        </div>


		        <div class="form-group">
        
		            <label for="about" style="font-size: 15px;">{{__('app.about_shop')}} <small style="color: red;">*</small></label>
		        
		            <textarea class="form-control" name="about_shop" id="about" placeholder="{{__('app.about_shop')}}" style="font-size: 15px; " rows="7">{{$user->about_shop}}</textarea>
		        
		        </div>

		        <div class="form-group">
         
		            <label for="phone_number" style="font-size: 15px;">{{__('app.phone_number')}}</label>
		        
		            <input type="text" name="phone_number" value="{{$user->phone_number}}" class="form-control" id="phone_number" placeholder="{{__('app.phone_number')}}" style="font-size: 15px; ">
		        
		        </div>

		        

		        @endif

		        <div class="form-group">
         
		            <label for="email" style="font-size: 15px;">{{__('app.email')}}</label>
		        
		            <input type="email" name="email" value="{{$user->email}}" class="form-control" id="email" placeholder="{{__('app.email')}}" style="font-size: 15px; ">
		        
		        </div>
		        

		        <button class="btn btn-primary" type="submit">{{__('app.Update')}}</button>

      		</form>

  		</div>

	</div>

</div>


@include('_session_.error2')

@include('_session_.success2')

@endsection