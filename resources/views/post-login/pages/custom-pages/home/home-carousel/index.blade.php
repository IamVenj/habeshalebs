@extends('post-login.index.index')

@section('content')

<div class="col-12 grid-margin stretch-card">

  <div class="card">

    <div class="card-body">

      <h4 class="card-title">{{__('app.home_carousel')}}</h4>

      <form class="forms-sample" action="/custom-carousel" method="post" enctype="multipart/form-data">

        @csrf
        
        <div class="form-group">

          <label>{{__('app.File_upload')}}</label>

          <input type="file" name="image" class="file-upload-default">

          <div class="input-group col-xs-12">

            <input type="text" class="form-control file-upload-info" disabled="" placeholder="{{__('app.Upload_Image')}}">

            <span class="input-group-append">

              <button class="file-upload-browse btn btn-primary" type="button">{{__('app.choose_image')}}</button>

            </span>

          </div>

        </div>

        <div class="form-group">

        	<label>{{__('app.Category')}}</label>

	        <select class="form-control form-control-lg" id="exampleFormControlSelect1" name="category">

	          <option selected disabled>{{__('app.Select_Category')}}</option>

            <?php $categories = App\Category::latest()->get(); ?>
 
            @foreach($categories as $category)

    	          <option value="{{$category->id}}">{{$category->category_name}}</option>
                
            @endforeach

	        </select>

	    </div>

      <button type="submit" class="btn btn-primary mr-2">{{__('app.Submit')}}</button>

      </form>

    </div>

  </div>

</div>


<div class="col-12 grid-margin stretch-card">

	<div class="card">

    <div class="card-body">

  		<div class="row">

        @foreach($carousels as $carousel)

				<div class="col-md-3 mt-3">

					<div class="card">

            <img class="card-img-top" style="height: 250px; object-fit: cover;" src="<?= Cloudder::show($carousel->image_public_id, ['version'=> $carousel->image_version, 'width'=>200, 'height'=>200, 'crop'=>'fill']);?>" alt="portfolio->image">

            <div class="card-body">

              <h4 class="card-title mb-3" style="font-weight: bold;">{{$carousel->category()->first()->category_name}}</h4>

              <div class="dropdown-divider"></div>

              <div class="ticket-actions mb-3">

                <div class="btn-group dropdown">

                	<button type="button" class="btn btn-secondary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                            	
                    {{__('app.Manage')}}

                	</button>

                  @include('post-login.partials.modal.custom-home-modal')

                	<div class="dropdown-menu">

                  	<a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#edit-carousel'.$carousel->id; ?>">

                    		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Edit')}}</a>
                  	
                    <div class="dropdown-divider"></div>

                  	
                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#update-image-carousel'.$carousel->id; ?>">

                    		<i class="mdi mdi-tooltip-edit mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Update_Image')}}</a>
                  	
                    <div class="dropdown-divider"></div>

                  	
                    <a class="dropdown-item" data-toggle="modal" href="#" data-target="<?= '#delete-carousel'.$carousel->id; ?>">

                    		<i class="mdi mdi-delete-forever mr-2" style="color: rgba(0,0,0,0.5);"></i>{{__('app.Delete')}}</a>

                	</div>

                </div>

              </div>

              <p class="card-text" style="color: rgba(0,0,0,0.3); font-size: 14px;"><i class="mdi mdi-clock"></i> {{$carousel->created_at->diffForHumans()}}</p> 

            </div>

          </div>

				</div>

        @endforeach

			</div>

		</div>

  </div>

</div>

@include('_session_.error2')

@include('_session_.success2')

@endsection