<?php
$companysettings = App\CompanySettings::first();
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="csrf-token" content="{{csrf_token()}}"/>

  <meta name="userId" content="{{ Auth::check() ? auth()->user()->id : ''}}">

  <title>{{$companysettings->company_name}}</title>

  <link rel="stylesheet" href="{{URL::asset('vendors/mdi/css/materialdesignicons.min.css')}}">
  
  <link rel="stylesheet" href="{{URL::asset('vendors/css/vendor.bundle.base.css')}}">
    
  <link rel="stylesheet" href="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  
  <link rel="stylesheet" href="{{URL::asset('css/admin_css/style.css')}}">

  <link rel="shortcut icon" href="{{URL::asset('storage/uploads/favicon.png')}}" />

  <style type="text/css">
    
    .main-panel {

      transition: width 0.25s ease, margin 0.25s ease;

      @if(!Request::is('wizard'))

        width: calc(100% - 257px);

      @else

        width: 100%;

      @endif

      min-height: calc(100vh - 60px);

      display: -webkit-flex;

      display: flex;

      -webkit-flex-direction: column;

      flex-direction: column;

    }

  </style>

</head>

<body>

  <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBPCRMMp4wCB1RYgrj0lavgRLSn-v9kz8g&libraries=places"></script>

   
  <div class="jumping-dots-loader" id="loader-main" style="position: absolute; top: 40%; left: 48%;">

    <span></span>

    <span></span>

    <span></span>

  </div>
  
  <div id="app" style="display: none;">
    
    <div class="container-scroller">

      @include('post-login.index.navigation')

      <div class="container-fluid page-body-wrapper">

        @if(!Request::is('wizard'))

          @include('post-login.index.sidebar')

        @endif

        <div class="main-panel">

          <div class="content-wrapper">

            @yield('content')

          </div>

          @include('post-login.index.footer')

        </div>

      </div>

    </div>


  </div>


  <script src="{{URL::asset('vendors/js/vendor.bundle.base.js')}}"></script>

  <script src="{{URL::asset('js/app.js')}}"></script>

  @if(\Request::is('users'))

  <script src="{{URL::asset('js/_customJs_/userList.js')}}"></script>

  @endif


  @if(\Request::is('wizard'))

  <script src="{{URL::asset('js/jquery.min.js')}}"></script>

  <script src="{{URL::asset('vendors/jquery-steps/jquery.steps.min.js')}}"></script>

  <script src="{{URL::asset('js/_customJs_/wizard.js')}}"></script>

  @endif

  <script src="{{URL::asset('js/_customJs_/order-show.js')}}"></script>

  @if(Request::is('my-address'))

  <script src="{{URL::asset('js/_customJs_/map_address.js')}}"></script>
  
  @endif

  @if(Request::is('category'))

  <script src="{{URL::asset('js/_customJs_/category.js')}}"></script>

  @endif

  @if(Request::is('special-offers'))

  <script src="{{URL::asset('js/_customJs_/special_offers.js')}}"></script>

  @endif

  @if(Request::is('my-account'))

  <script src="{{URL::asset('js/_customJs_/my_account.js')}}"></script>

  @endif

  @if(Request::is('create-product'))

  <script src="{{URL::asset('js/_customJs_/images.js')}}"></script>

  <script src="{{URL::asset('js/_customJs_/products.js')}}"></script>

  @endif

  <script type="text/javascript">
    
    $(window).on('load', function() {

      document.getElementById('app').style.display = 'block';

      document.getElementById('loader-main').style.display = 'none';

    });

  </script>

  @if(!Request::is('wizard'))

  @if(Request::is('dashboard'))

  <script src="{{URL::asset('vendors/chart.js/Chart.min.js')}}"></script>

  @endif

  <script src="{{URL::asset('vendors/datatables.net/jquery.dataTables.js')}}"></script>

  <script src="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>

  @endif


  <script src="{{URL::asset('vendors/jquery-validation/jquery.validate.min.js')}}"></script>

  <script src="{{URL::asset('vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
  
  <?php

  /*
  |---------------------------------------------------------------------
  | Custom Javascript
  |---------------------------------------------------------------------
  */

  ?>

  <script src="{{URL::asset('js/admin_js/form-validation.js')}}"></script>

  @if(!Request::is('wizard'))

  <script src="{{URL::asset('js/admin_js/off-canvas.js')}}"></script>

  <script src="{{URL::asset('js/admin_js/hoverable-collapse.js')}}"></script>

  <script src="{{URL::asset('js/admin_js/settings.js')}}"></script>

  <!-- <script src="{{URL::asset('js/admin_js/todolist.js')}}"></script> -->

  @if(Request::is('dashboard'))

  <script src="{{URL::asset('js/admin_js/dashboard.js')}}"></script>

  @endif

  <script src="{{URL::asset('js/admin_js/data-table.js')}}"></script>

  @endif

  @if(!Request::is('orders'))

  <script src="{{URL::asset('js/admin_js/bt-maxLength.js')}}"></script>

  <script src="{{URL::asset('js/admin_js/file-upload.js')}}"></script>

  <script src="{{URL::asset('js/admin_js/template.js')}}"></script>

  @endif

</body>

</html>