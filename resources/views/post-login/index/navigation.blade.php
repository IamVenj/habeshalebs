<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

  <div class="navbar-brand-wrapper d-flex justify-content-center">

    <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">  

      <a class="navbar-brand brand-logo" href="/dashboard"><img src="{{URL::asset('storage/uploads/logo.png')}}" alt="logo"/></a>

      <a class="navbar-brand brand-logo-white" href="/dashboard"><img src="{{URL::asset('storage/uploads/logo.png')}}" alt="logo"/></a>

      <a class="navbar-brand brand-logo-mini" href="/dashboard"><img src="{{URL::asset('storage/uploads/logo.png')}}" alt="logo"/></a>

      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">

        <span class="mdi mdi-sort-variant"></span>

      </button>

    </div>  

  </div>

  <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">


    <ul class="navbar-nav navbar-nav-right">

      @if(!Request::is('wizard'))

      <notification v-bind:notifications="notifications"></notification>

      @endif

      <li class="nav-item nav-profile dropdown">

        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">

          <img src="@if(!is_null(auth()->user()->photo_url))<?= Cloudder::show(auth()->user()->photo_url, ['width'=>200, 'height'=>200, 'crop'=>'fill']);?>@else {{URL::asset('images/default-avatar.png')}} @endif" alt="profile"/>


          <span class="nav-profile-name">@if(auth()->user()->role == 1){{auth()->user()->firstname}}@elseif(auth()->user()->role == 2){{auth()->user()->shop_name}}@endif</span>

        </a>

        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">

          @if(!Request::is('wizard'))

          @if(auth()->user()->role == 1)

          <a class="dropdown-item" href="/settings">

            <i class="mdi mdi-settings text-primary"></i>

            {{__('app.Settings')}}

          </a>

          @endif

          <a class="dropdown-item" href="/my-account">

            <i class="mdi mdi-account text-primary"></i>

            {{__('app.my_account')}}

          </a>

          @if(auth()->user()->role == 2)

          <a class="dropdown-item" href="/my-address">

            <i class="mdi mdi-map-marker text-primary"></i>

            {{__('app.my_address')}}

          </a>

          @endif

          @endif

          <a class="dropdown-item" href="/logout">

            <i class="mdi mdi-logout text-primary"></i>

            {{__('app.logout')}}

          </a>

        </div>

      </li>


    </ul>

    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">

      <span class="mdi mdi-menu"></span>

    </button>

  </div>

</nav>