<?php $setting = App\CompanySettings::first(); ?>

<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">{{__('app.copyright')}} © <?= date('Y'); ?> <a href="" target="_blank">{{$setting->company_name}}</a>. {{__('app.rights')}}.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">{{__('app.developer_note2')}} <i class="mdi mdi-heart text-danger"></i></span>
  </div>
</footer>