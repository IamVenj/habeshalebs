<div class="modal modal-edu-general fade" role="dialog" id="profile-pic-update">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.UpdateProfilePicture')}} </h4><div id="pending_status_edit"></div>

                <hr>

            </div>

            <form method="POST" action="/update-profile-pic" method="post" enctype="multipart/form-data">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="@if(\Request::is('account')) picture-container2  @else picture-container @endif">
              
                        <div class="@if(\Request::is('account')) picture2 @else picture @endif">

                            <input type="file" name="image" class="file-upload-default image">
                    
                            <img src="@if($user->photo_url == null){{URL::asset('images/default-avatar.png')}}@else <?= Cloudder::show($user->photo_url, ['width'=>200, 'height'=>200, 'crop'=>'fill']);?> @endif" class="picture-src" id="@if(\Request::is('account')) Profile_Picture_Preview @else profilePicturePreview @endif" title=""/>
                                
                        </div>
                                    
                    </div>                    

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>


<div class="modal modal-edu-general fade" role="dialog" id="delivery-option-update">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.UpdateDeliveryOption')}} </h4>

                <hr>

            </div>

            <form action="/update-delivery-option" method="post">

                @csrf

                @method('PATCH')

                <div class="modal-body">


                    <div class="form-check">
            
                        <label class="form-check-label">
                
                            <input class="checkbox" name="m-delivery" type="checkbox" id="material-delivery" <?php if($user->delivery == 1): ?> checked <?php endif; ?>>
                
                            {{__('app.customer_delivery')}}
                
                        </label>
                
                    </div>
                    
                    <div id="delivery"></div>

                    <input type="hidden" name="price_per_km" value="@if($user->delivery == 1){{$user->delivery()->first()->price}}@endif">


                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>



<div class="modal modal-edu-general fade" role="dialog" id="upassword-change">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Change_Password')}} </h4><div id="pending_status_edit"></div>

                <hr>

            </div>

            <form method="POST" action="/change-u-password">

                @csrf

                @method('PATCH')

                <div class="modal-body">


                    <div class="form-group">
         
                        <label for="c_password" style="font-size: 15px;">{{__('app.Current_Password')}}</label>
                    
                        <input type="password" name="current_password" class="form-control" id="c_password" placeholder="{{__('app.Current_Password')}}" style="font-size: 15px; ">
                    
                    </div>

                    <div class="form-group">
         
                        <label for="n_password" style="font-size: 15px;">{{__('app.NewPassword')}}</label>
                    
                        <input type="password" name="new_password" class="form-control" id="n_password" placeholder="{{__('app.NewPassword')}}" style="font-size: 15px; ">
                    
                    </div>


                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>