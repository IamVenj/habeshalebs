<div class="modal modal-edu-general fade" role="dialog" id=<?= "edit-product".$product->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Edit')}} - {{$product->product_name}} </h4>

                <hr>

            </div>

            <form method="POST" action="/update-product/{{$product->id}}">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                      <label for="product_name">{{__('app.Product_Name')}}</label>

                      <input id="product_name" class="form-control" name="product_name" value="{{$product->product_name}}" type="text" placeholder="{{__('app.Product_Name')}}">

                    </div>

                    <div class="form-group">

                        <label for="sku">{{__('app.unique_code')}}</label>

                        <input id="sku" class="form-control" name="sku" type="text" value="{{$product->sku}}" placeholder="{{__('app.unique_code')}}">

                    </div>



                    <div class="form-group">

                      <label for="maxlength-textarea">{{__('app.Product_Description')}}</label>

                      <textarea id="maxlength-textarea" name="slug" class="form-control" maxlength="2000" rows="5" placeholder="{{__('app.Product_Description')}}">{{$product->slug}}</textarea>

                    </div>

                    <div class="form-group">

                      <label for="brand">{{__('app.Product_Brand')}}</label>

                      <input id="brand" class="form-control" name="brand" type="text" value="{{$product->brand}}" placeholder="{{__('app.Product_Brand')}}">

                    </div>



                    <div class="form-group">

                      <label for="price">{{__('app.current_price')}}</label>

                      <input id="price" class="form-control" name="current_price" type="number" value="{{$product->current_price}}" placeholder="{{__('app.Price_in_birr')}}">

                    </div>


                    <div class="form-group">

                      <label for="price">{{__('app.old_price')}}</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

                      <input id="price" class="form-control" name="old_price" type="number" value="{{$product->old_price}}" placeholder="{{__('app.Price_in_birr')}}">

                    </div>


                    <div class="form-group">
              
                      <label for="">{{__('app.Size')}}</label><small style="color: #F84F42"> ({{__('app.optional')}})</small>

                      <select class="form-control form-control-sm" name="product_size[]" multiple="">

                        @if($product->productSize()->count() > 0)

                        <?php
                        
                        $productSizes = $product->productSize()->get();

                        $productSizeArray = array();
                        
                        ?>

                        @foreach($productSizes as $key=>$value)

                        <?php

                        array_push($productSizeArray, $productSizes[$key]->size);

                        ?>

                        @endforeach
                        
                        <option value="XS" <?php if(in_array('XS', $productSizeArray)): ?> selected <?php endif;?>>Extra Small</option>
                        
                        <option value="S" <?php if(in_array('S', $productSizeArray)): ?> selected <?php endif;?>>Small</option>
                        
                        <option value="M" <?php if(in_array('M', $productSizeArray)): ?> selected <?php endif;?>>Medium</option>
                        
                        <option value="L" <?php if(in_array('L', $productSizeArray)): ?> selected <?php endif;?>>Large</option>
                        
                        <option value="XL" <?php if(in_array('XL', $productSizeArray)): ?> selected <?php endif;?>>Extra Large</option>

                        

                        @else
                
                        <option value="XS">Extra Small</option>
                        
                        <option value="S">Small</option>
                        
                        <option value="M">Medium</option>
                        
                        <option value="L">Large</option>
                        
                        <option value="XL">Extra Large</option>


                        @endif

                      </select>

                    </div>


                    <div class="form-group">

                      <label for="availability">{{__('app.how_many_available')}}?</label>

                      <input id="availability" class="form-control" name="availability" value="{{$product->availability}}" type="number" placeholder="Number of stock">

                    </div>


                    <div class="form-group">

                        <label for="weight">{{__('app.weight_in_kg')}}?</label>

                        <input id="weight" class="form-control" name="weight" type="number" placeholder="{{__('app.weight_in_kg_example')}}5 kg">

                    </div>



                    <div class="form-group">

                        <label for="washcare">{{__('app.washcare')}}?</label>

                        <input id="washcare" class="form-control" name="washcare" type="number" placeholder="{{__('app.washcare_example')}}Dry Clean">

                    </div>



                    <div class="form-group">

                        <label for="composition">{{__('app.composition')}}?</label>

                        <input id="composition" class="form-control" name="composition" type="number" placeholder="{{__('app.composition_example')}}100% Polyester">

                    </div>



                    <div class="form-group">

                        <label for="lining_composition">{{__('app.lining_composition')}}?</label>

                        <input id="lining_composition" class="form-control" name="lining_composition" type="number" placeholder="{{__('app.lining_composition_example')}}100% Polyester">

                    </div>



                    <div class="form-group">
              
                      <label>{{__('app.Category')}}</label>
              
                      <select class="form-control form-control-lg" name="category" id="exampleFormControlSelect1">
              
                        <option selected disabled>{{__('app.Select_Category')}}</option>
              
                        @foreach($categories as $category)

                        <option value="{{$category->id}}" <?php if($category->id == $product->category_id): ?> selected <?php endif; ?>>{{$category->category_name}}</option>

                        @endforeach
              
                      </select>
              
                    </div>


                    <div class="form-group">
              
                      <label>{{__('app.ProductsLocation')}}</label>
              
                      <select class="form-control form-control-lg" name="product_location" id="exampleFormControlSelect1">
              
                        <option selected disabled>{{__('app.Select_Location')}}</option>

                        <option value="all" <?php if($product->product_location == 'all'): ?>selected<?php endif; ?>>{{__('app.All')}}</option>
              
                        @foreach($addresses as $address)

                        <option value="{{$address->address}}" <?php if($address->address == $product->product_location): ?> selected <?php endif; ?>>{{$address->address}}</option>

                        @endforeach
              
                      </select>
              
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save_Changes')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id=<?= "delete-product".$product->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Delete')}} - {{$product->product_name}} </h4>

                <hr>

            </div>

            <form method="POST" action="/delete-product">

                @csrf

                @method('delete')
                
                <div class="modal-footer">

                    <button class="btn btn-danger" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>
