<div class="modal modal-edu-general fade" role="dialog" id="edit-address{{$locations[$i]->id}}">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Edit_Address')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/update-address/{{$locations[$i]->id}}" method="post">

                @csrf

                @method('PATCH')

                <div id="mod">
                    
                    <input id="searchTextField{{$locations[$i]->id}}" class="form-control" type="text" value="{{$locations[$i]->address}}" name="address" size="50" placeholder="{{__('app.search')}}">
                
                </div>


                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12">
                        
                            <div id="map{{$locations[$i]->id}}" class="show-map2"></div>

                            <input type="hidden" name="lat" id="lat{{$locations[$i]->id}}" value="{{$locations[$i]->latitude}}">

                            <input type="hidden" name="lng" id="lng{{$locations[$i]->id}}" value="{{$locations[$i]->longitude}}">
                      
                        </div>
                    
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</button>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="add-address">

    <div class="modal-dialog ">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-plus"></i>{{__('app.Add_Address')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/add-address" method="post">

                @csrf

                 <div id="mod">
                    
                    <input id="__address" class="form-control" type="text" name="address" size="50" placeholder="{{__('app.search')}}">
                
                </div>


                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12">
                        
                            <div id="_map" class="show-map"></div>

                            <input type="hidden" name="lat" id="lat">

                            <input type="hidden" name="lng" id="lng">
                      
                        </div>
                    
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Add')}}</button>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="remove-map{{$locations[$i]->id}}">

    <div class="modal-dialog ">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-close"></i>{{__('app.Remove_Map')}} </h4>

                <hr>

            </div>

            <form action="/remove-map/{{$locations[$i]->id}}" method="post">

                @csrf

                @method('DELETE')
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Remove')}}</button>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>
