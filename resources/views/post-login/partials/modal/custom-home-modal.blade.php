<div class="modal modal-edu-general fade" role="dialog" id="update-image-carousel{{$carousel->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.AddImage')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/update-image-carousel/{{$carousel->id}}" enctype="multipart/form-data">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">
                                
                        <input type="file" name="image" class="file-upload-default">
                      
                        <div class="input-group col-xs-12">
                      
                            <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="{{__('app.Upload_Image')}}">
                      
                            <span class="input-group-append">
                      
                                <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Replace_Image')}}</button>
                      
                            </span>

                        </div>
                      
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Update')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>


<div class="modal modal-edu-general fade" role="dialog" id="edit-carousel{{$carousel->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Edit_Carousel')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/edit-carousel/{{$carousel->id}}">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                    <label>{{__('app.Category')}}</label>

                    <select class="form-control form-control-lg" id="exampleFormControlSelect1" name="category">

                        <option selected disabled>{{__('app.Select_Category')}}</option>

                        <?php
             
                        $categories = App\Category::latest()->get();
             
                        ?>
             
                        @foreach($categories as $_c)

                            <option value="{{$_c->id}}" <?php if($_c->id == $carousel->category_id): ?> selected <?php endif;?>>{{$_c->category_name}}</option>

                        @endforeach

                    </select>

                </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Update')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>



<div class="modal modal-edu-general fade" role="dialog" id="delete-carousel{{$carousel->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.Delete_Carousel')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/delete-carousel/{{$carousel->id}}">

                @csrf

                @method('DELETE')
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>
