<div class="modal modal-edu-general fade" role="dialog" id="categoryModal">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Edit_Category')}} </h4><div id="pending_status_edit"></div>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

                <hr>

            </div>

            <form method="POST" action="/edit-category" id="edit_category_form">

                <input type="hidden" name="csrf-edit-token" value={{csrf_token()}}>

                <div class="modal-body">


                        
                    <div class="form-group">
     
                        <label for="category_name">{{__('app.Category_Name')}}</label>
             
                        <input id="category_name" class="form-control" name="categoryName" placeholder="{{__('app.Category_Name')}}" type="text">
             
                    </div>

                    <div class="form-group">
              
                        <label>{{__('app.Category')}}</label>
              
                        <select class="form-control form-control-lg" id="mainCategory" name="mainCategory">
              
                          <option selected disabled>{{__('app.Select_Category')}}</option>
              
                          @foreach($categories as $category)

                          <option value="{{$category->id}}">{{$category->category_name}}</option>

                          @endforeach
              
                        </select>
              
                    </div>

                </div>
                
                <div class="modal-footer">

                    <input type="hidden" name="category_id" id="category_id">                    

                    <button class="btn btn-success" id="button-edit-category" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</a>
                    
                    <button class="btn btn-info" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="categoryImageModal">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.UpdateImageCategory')}} </h4><div id="pending_status_image"></div>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

                <hr>

            </div>

            <form method="POST" action="/edit-category-image" id="edit_image_category_form">

                <input type="hidden" name="csrf-edit-image-token" value={{csrf_token()}}>

                <div class="modal-body">
                        
                    <div class="form-group">
          
                        <label>{{__('app.Category_Image')}}</label>
                      
                        <input type="file" id="image-category-upload" name="image_update" class="file-upload-default">
                      
                        <div class="input-group col-xs-12">
                      
                          <input type="text" class="form-control file-upload-info" id="file-upload-info" disabled="" placeholder="{{__('app.Upload_Image')}}">
                      
                          <span class="input-group-append">
                      
                            <button class="file-upload-browse btn btn-primary" type="button">{{__('app.Choose_Image')}}</button>
                      
                          </span>

                        </div>
                      
                    </div>

                    <img src="" id="current_image" alt="Category Image" style="width: auto !important; height: 150px; border-top-left-radius: 20px; border-bottom-right-radius: 20px; box-shadow: 0px 10px 20px #00000010;" />

                </div>
                
                <div class="modal-footer">

                    <input type="hidden" name="category_id" id="category_id">

                    <button class="btn btn-success" id="button-edit-image-category" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Save')}}</button>
                    
                    <button class="btn btn-info" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal modal-edu-general fade" role="dialog" id="categoryDeleteModal">

    <div class="modal-dialog ">
        <!-- modal-lg -->

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i>{{__('app.Delete_Category')}} </h4><div id="pending_status_delete"></div>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

                <hr>

            </div>

            <form method="DELETE" action="/delete-category" id="delete_category_form">

                <input type="hidden" name="csrf-delete-token" value={{csrf_token()}}>

                <div class="modal-footer">

                    <input type="hidden" name="category_id" id="category_id">

                    <button class="btn btn-danger" id="button-delete-category" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</button>
                    
                </div>

            </form>

        </div>

    </div>

</div>