<div class="modal modal-edu-general fade" role="dialog" id="delete-offer{{$offer->id}}">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-trash"></i>{{__('app.DeleteOffer')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/special-offer/{{$offer->id}}">

                @csrf

                @method('DELETE')
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Delete')}}</a>

                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</a>

                </div>

            </form>

        </div>

    </div>

</div>
