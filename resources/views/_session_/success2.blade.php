@if(session('success'))

    <div class="form-group mt-4">

    	<div class="alert golden2">

    		<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="color: #fff; cursor: pointer;"><strong>&times;</strong></small>

    		<p class="mr-5 ml-5" style="font-family: 'Montserrat', sans-serif; color: #fff; font-size: 16px; cursor: default;">{{ session('success') }}</p>

      	</div>

    </div>

@endif