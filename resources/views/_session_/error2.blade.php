@if(count($errors))

    <div class="form-group mt-4">

    	<div class="alert golden pb-0">

    		<ul style="list-style-type: none;">

    			<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="color: #fff; cursor: pointer;"><strong>&times;</strong></small>

	      		@foreach($errors->all() as $error)

	      			<li class="mb-1 mr-5 ml-5" style="font-family: 'Montserrat', sans-serif; font-size: 16px; color: #fff; cursor: default;">{{ $error }}</li>

	      		@endforeach

      		</ul>


      	</div>

    </div>

@endif