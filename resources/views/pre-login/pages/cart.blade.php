@extends('pre-login.index.index')

@section('content')
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.cart')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="content-page">
    <div class="container mb-80">
        <div class="row">
        	@if(Cart::instance('default')->count() == 0)
				<section class="" style="margin-bottom: 200px;">
                        <div class="home-about-blocks">
                            <div class="col-12 about-blocks-wrap2">
                                <div class="row">
                                    <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                        <div class="about-box-inner2">
                                            <h4 class="mb-25">{{__('app.sorry')}}!</h4><span class="fa fa-shopping-bag text-center" style="font-size: 50px;"></span>  
                                            <h5 class="mb-20 mt-25">{{__('app.empty_cart')}}</h5>
                                            <p>{{__('app.empty_added_cart')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
			

			@else
            <div class="col-sm-12">
                <article class="post-8">
                    <div class="cart-product-table-wrap responsive-table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-remove"></th>
                                    <th class="product-thumbnail"></th>
                                    <th class="product-name">{{__('app.product')}}</th>
                                    <th class="product-price">{{__('app.price')}}</th>
                                    <th class="product-quantity">{{__('app.quantity')}}</th>
                                    <th class="product-subtotal">{{__('app.total')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<input type="hidden" name="global-token" value="{{csrf_token()}}">

								@foreach(Cart::content() as $item)

								<input type="hidden" name="price_pay" class="total-price-of-one-item{{$item->model->id}}" value="{{$item->model->current_price * $item->qty}}">

								<input type="hidden" id="Item-rowId{{$item->model->id}}" value="{{$item->rowId}}">

								<input type="hidden" id="current_price{{$item->model->id}}" value="{{$item->model->current_price}}">
                                
                                <tr>
                                    <td class="product-remove">

                                        <a  onclick="removeFromCart2(<?= $item->model->id; ?>)"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                        
                                    </td>
                                    <td class="product-thumbnail">
                                        <a>
                                            <img src="<?= Cloudder::show($item->model->productImage()->first()->image_public_id, array("version" => $item->model->productImage()->first()->image_version, "quality" => "auto", "height" => 200, "width"=>200))?>" alt="{{$item->model->productImage()->first()->image_public_id}}"></a>
                                    </td>
                                    <td class="product-name">
                                        <a>{{$item->model->product_name}}</a>
                                    </td>
                                    <td class="product-price">
                                        <span class="product-price-amount amount"><span class="currency-sign">$</span>{{$item->model->current_price}}</span>
                                    </td>
                                    <td>
                                        <div class="product-quantity">
                                            <!-- <span data-value="+" class="quantity-btn quantityPlus"></span> -->
                                            <input class="quantity input-lg" step="1" min="1" max="20" name="quantity_pay[]" title="Quantity" onchange="update_cart(<?= $item->model->id; ?>)" type="number" id="qty{{$item->model->id}}" value="{{$item->qty}}"/>
                                            <!-- <span data-value="-" class="quantity-btn quantityMinus"></span> -->
                                        </div>
                                    </td>
                                    <td class="product-subtotal">
                                        <span class="product-price-sub_totle amount total-price-of-one-item{{$item->model->id}}"><span class="currency-sign">$</span>{{$item->qty * $item->model->current_price}} </span>
                                    </td>
                                </tr>

								@endforeach
                            </tbody>
                        </table>
                    </div>
                        
                    <div class="cart-collateral">
                        <div class="cart_totals">
                            <h3>Cart totals</h3>
                            <div class="responsive-table">
                                <table>
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>{{__('app.SUBTOTAL')}}</th>

                                            <td><span class="product-price-amount amount cart-total-price"></span></td>
                                        </tr>
                                        <tr class="shipping">
                                            <th>{{__('app.DELIVERY')}}</th>
                                            <td>
                                                <ul id="shipping_method">
                                                    <li>
                                                        <label for="shipping_method_0_legacy_flat_rate">Flat Rate: <span class="woocommerce-Price-currencySymbol">$</span><span class="woocommerce-Price-amount amount flat_rate">
                                                        </span></label>
                                                    </li>
                                                    
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr class="order-total">
                                            <th style="text-transform: uppercase;">{{__('app.total')}}</th>
                                            <td><span class="product-price-amount amount" id="total_price_main"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="product-proceed-to-checkout">
                                <a class="btn btn-lg btn-color form-full-width" href="/checkout">Proceed to checkout</a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            @endif
        </div>
    </div>

</section>

@endsection