@extends('pre-login.index.index')

@section('content')

 <!-- Intro -->
<section id="intro" class="intro">
    <!-- Revolution Slider -->
    <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="background-color: transparent; padding: 0px;">
        <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
        <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display: none;" data-version="5.3.0.2">
            <ul>
                @for($i=0; $i < $carousels->count(); $i++)
                <li class="dark-bg" data-index="rs-{{$carousels[$i]->id}}" data-transition="random" data-slotamount="7" data-masterspeed="500" data-thumb="" data-saveperformance="off" data-title="{{$carousels[$i]->category->category_name}}">
                   
                    <img src="<?= Cloudder::show($carousels[$i]->image_public_id, array("quality" => "auto", "version"=>$carousels[$i]->image_version, "height" => 1900, "width"=>1900)); ?>" alt="{{$carousels[$i]->image_public_id}}" title="{{$carousels[$i]->category->category_name}}" width="1920" height="1100" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="6" class="rev-slidebg" />

                    <a href="/{{$carousels[$i]->category->category_name}}/products/{{$carousels[$i]->category->id}}" id="{{$carousels[$i]->id}}" style="font-size: 48px; color: #fff; background-image: linear-gradient(to right, #000000, #00000000); font-family: 'pacifico', cursive;" class="tp-caption NotGeneric-Title tp-resizeme" data-x="25" data-y="500" data-hoffset="" data-voffset="75" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_in="y:50px;opacity:0;s:700;e:Power3.easeOut;" data-transform_out="s:500;e:Power3.easeInOut;s:500;e:Power3.easeInOut;" data-start="1100" data-speed="500" data-endspeed="500" data-splitin="none" data-splitout="none" data-responsive_offset="on">{{$carousels[$i]->category->category_name}} - {{__('app.SeeMore')}}  </a>

                </li>
               @endfor
                
            </ul>
        </div>
    </div>

    <!-- End Revolution Slider -->
</section>
<!-- End Intro -->

<!-- Promo Box -->
<section id="promo" class="section-padding-sm promo ">
    <div class="container">
        <div class="promo-box row">
            <div class="col-md-4 mtb-sm-30 promo-item">
                <div class="icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
                <div class="info">
                    <a href="#">
                        <h6 class="normal">{{__('app.Delivery')}}</h6>
                    </a>
                    <!-- <p>On Order Over $150</p> -->
                </div>
            </div>
            <div class="col-md-4 mtb-sm-30 promo-item">
                <div class="icon"><i class="fa fa-repeat" aria-hidden="true"></i></div>
                <div class="info">
                    <a href="#">
                        <h6 class="normal">{{__('app.Exchange_or_Return')}}</h6>
                    </a>
                    <!-- <p>30 Day Money Back Guarantee</p> -->
                </div>
            </div>
            <div class="col-md-4 mtb-sm-30 promo-item">
                <div class="icon"><i class="fa fa-headphones" aria-hidden="true"></i></div>
                <div class="info">
                    <a href="#">
                        <h6 class="normal">{{__('app.Support')}}</h6>
                    </a>
                    <!-- <p>24/7 Online Support</p> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Promo Box -->


<!-- Product (Tab with Slider) -->
<section class="section-padding-b">
    <div class="container">
        <h2 class="page-title">{{__('app.TopInteresting')}}</h2>
    </div>
    <div class="container">
        <ul class="product-filter nav" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#latest" role="tab" data-toggle="tab">{{__('app.NewProduct')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#best-sellar" role="tab" data-toggle="tab">{{__('app.best_seller')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#features" role="tab" data-toggle="tab">{{__('app.special_offers')}}</a>
            </li>
        </ul>
        <div class="tab-content">
            <!-- Tab1 - Latest Product -->
            <div id="latest" role="tabpanel" class="tab-pane fade in active">
            	@if(count($latestP) == 0)
            	<span class="alert alert-danger text-center" style="width: 100%; position: absolute;">
            	{{__('app.sorry_items')}}</span>
            	@endif
                <div id="new-product" class="product-item-4 owl-carousel owl-theme nf-carousel-theme1">

                	@foreach($latestP as $product)

                    <div class="product-item">
                        <div class="product-item-inner">
                            @if ($product->current_price < $product->old_price)
                            <div class="sale-label">
                                <?= round(($product->current_price/$product->old_price)*100);?> %</div>
                            @endif
                            <div class="product-img-wrap">
                                <img src="<?= Cloudder::show($product->productImage->first()->image_public_id, array("quality" => "auto", "version"=>$product->productImage->first()->image_version , "height" => 800, "width"=>800)); ?>" alt="{{$product->product_name}}"  style="height: 300px; width: 300px; object-fit:cover;">
                            </div>
                            <div class="product-button">
                                @guest
                                <a href="/login" class="js_tooltip" data-mode="top" data-tip="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                @else
                                <a class="js_tooltip" data-mode="top" data-tip="Add To Wishlist" onclick="add_wish(<?= $product->id;?>)"><i class="fa fa-heart"></i></a>
                                @endguest
                                <a href="/{{$product->product_name}}/detail/{{$product->id}}" class="js_tooltip" data-mode="top" data-tip="Quick&nbsp;View"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <a class="tag" href="/{{$product->category->category_name}}/products/{{$product->category->id}}">{{$product->category->category_name}}</a>
                            <p class="product-title"><a href="">{{$product->product_name}}</a></p>
                            <p class="product-rating">
                                sold by: {{$product->user->shop_name}}
                            </p>
                            <p class="product-description">
                                {{$product->slug}}
                            </p>
                            <h5 class="item-price"><del>
                            @if(!is_null($product->old_price))
                            ${{$product->old_price}}
                            @endif
                            </del> ${{$product->current_price}}</h5>
                        </div>
                    </div>
                    
                   @endforeach

                </div>
            </div>

            <!-- Tab2 - Best Sellar -->
            <div id="best-sellar" role="tabpanel" class="tab-pane fade">
            	@if(count($best_products) == 0)
            	<span class="alert alert-danger text-center" style="width: 100%; position: absolute;">
            	{{__('app.sorry_items')}}</span>
            	@endif
                <div id="popular-product" class="product-item-4 owl-carousel owl-theme nf-carousel-theme1">

                    @foreach($best_products as $product)

                    <div class="product-item">
                        <div class="product-item-inner">
                            @if ($product->current_price < $product->old_price)
                            <div class="sale-label">
                                <?= round(($product->current_price/$product->old_price)*100);?> %</div>
                            @endif
                            <div class="product-img-wrap">
                                <img src="<?= Cloudder::show($product->productImage->first()->image_public_id, array("quality" => "auto", "version"=>$product->productImage->first()->image_version , "height" => 800, "width"=>800)); ?>" alt="{{$product->product_name}}"  style="height: 300px; width: 300px; object-fit:cover;">
                            </div>
                            <div class="product-button">
                                @guest
                                <a href="/login" class="js_tooltip" data-mode="top" data-tip="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                @else
                                <a class="js_tooltip" data-mode="top" data-tip="Add To Wishlist" onclick="add_wish(<?= $product->id;?>)"><i class="fa fa-heart"></i></a>
                                @endguest
                                <a href="/{{$product->product_name}}/detail/{{$product->id}}" class="js_tooltip" data-mode="top" data-tip="Quick&nbsp;View"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <a class="tag" href="/{{$product->category->category_name}}/products/{{$product->category->id}}">{{$product->category->category_name}}</a>
                            <p class="product-title"><a href="">{{$product->product_name}}</a></p>
                            <p class="product-rating">
                                sold by: {{$product->user->shop_name}}
                            </p>
                            <p class="product-description">
                                {{$product->slug}}
                            </p>
                            <h5 class="item-price"><del>
                            @if(!is_null($product->old_price))
                            ${{$product->old_price}}
                            @endif
                            </del> ${{$product->current_price}}</h5>
                        </div>
                    </div>

                    @endforeach
                </div>
            </div>

            <!-- Tab3 - Features -->
            <div id="features" role="tabpanel" class="tab-pane fade">
            	@if(count($offers) == 0)
            	<span class="alert alert-danger text-center" style="width: 100%; position: absolute;">
            	{{__('app.sorry_offers')}}</span>
            	@endif
                <div id="features-product" class="product-item-4 owl-carousel owl-theme nf-carousel-theme1">
                    @foreach($offers as $product)

                    <div class="product-item">
                        <div class="product-item-inner">
                            @if ($product->product->current_price < $product->product->old_price)
                            <div class="sale-label">
                                <?= round(($product->product->current_price/$product->product->old_price)*100);?> %</div>
                            @endif
                            <div class="product-img-wrap">
                                <img src="<?= Cloudder::show($product->product->productImage->first()->image_public_id, array("quality" => "auto", "version"=>$product->product->productImage->first()->image_version , "height" => 800, "width"=>800)); ?>" alt="{{$product->product_name}}"  style="height: 300px; width: 300px; object-fit:cover;">
                            </div>
                            <div class="product-button">
                                @guest
                                <a href="/login" class="js_tooltip" data-mode="top" data-tip="Add To Wishlist"><i class="fa fa-heart"></i></a>
                                @else
                                <a class="js_tooltip" data-mode="top" data-tip="Add To Wishlist" onclick="add_wish(<?= $product->product->id;?>)"><i class="fa fa-heart"></i></a>
                                @endguest
                                <a href="/{{$product->product->product_name}}/detail/{{$product->product->id}}" class="js_tooltip" data-mode="top" data-tip="Quick&nbsp;View"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="product-detail">
                            <a class="tag" href="/{{$product->product->category->category_name}}/products/{{$product->product->category->id}}">{{$product->product->category->category_name}}</a>
                            <p class="product-title"><a href="">{{$product->product->product_name}}</a></p>
                            <p class="product-rating">
                                {{__('app.sold_by')}}: {{$product->product->user->shop_name}}
                            </p>
                            <p class="product-description">
                                {{$product->product->slug}}
                            </p>
                            <h5 class="item-price"><del>
                            @if(!is_null($product->product->old_price))
                            ${{$product->product->old_price}}
                            @endif
                            </del> ${{$product->product->current_price}}</h5>
                        </div>
                    </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Product (Tab with Slider) -->

<!-- Categories -->
<section class="">
    <div class="section-padding container-fluid bg-image text-center overlay-light90" data-background-img="img/bg/bg_5.jpg" data-bg-position-x="center center" style="padding-bottom: 80px;">
        <div class="container">
            <h2 class="page-title">{{__('app.shop_category')}}</h2>
        </div>
    </div>
    <div class="container container-margin-minus-t list-none-ib owl-carousel owl-theme">
        <div class="row">
        	@if(count($categories) == 0)
        	<span class="alert alert-danger text-center" style="width: 100%; position: absolute;">
            	{{__('app.sorry_category')}}</span>
        	@endif
        	@foreach($categories as $category)
            <div class="col-md-4">
            	<a href="/{{$category->category_name}}/products/{{$category->id}}">
	                <div class="categories-box">
	                    <div class="categories-image-wrap">
	                        <img src="<?= Cloudder::show($category->image_public_id, ['version'=> $category->image_version, "quality" => "auto", 'width'=>400, 'height'=>400, 'crop'=>'fill']);?>" alt="" />
	                    </div>
	                    <div class="categories-content">
	                        <a href="/{{$category->category_name}}/products/{{$category->id}}">
	                            <div class="categories-caption">
	                                <h6 class="normal">{{$category->category_name}}</h6>
	                            </div>
	                        </a>
	                    </div>
	                </div>
            	</a>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End Categories -->

<section class="section-padding dark-bg container-fluid bg-image text-center overlay-black40" data-background-img="{{asset('img/habesha.jpg')}}" data-bg-position-x="center center">
    <div class="container newsletter section-padding-b">
        <h2 class="page-title">{{__('app.subscribe_newsletter')}}</h2>
        <form action="/join-newsletter" class="newsletter-from" onsubmit="event.preventDefault(); return Newsletter(this)" id="form-newsletter" method="post">
            <div class="form-input">
                <input class="input-lg" name="email" id="frmemail" placeholder="Enter Email Address..." title="Enter Email Address..." type="text">
            </div>
            <button type="submit" class="btn btn-lg btn-color btn-subscribe">{{__('app.SignUp')}}</button>
        </form>
        <label style="color: #fff; font-size: 16px;">{{__('app.exclusiveText')}}</label>
    </div>
</section>

<!-- Newsletter -->

<!-- About blocks -->
<section class="">
    <div class="container container-margin-minus-t">
        <div class="home-about-blocks">
            <div class="col-12 about-blocks-wrap">
                <div class="row">
                   
                    <div class="col-sm-12 col-md-12 ">
                        <div class="about-box-inner">
                            <h4 class="mb-25">{{__('app.About')}} {{$settings->company_name}}</h4>
                            <p class="mb-20">{{__('app.welcome_to')}} <b class="black">{{$settings->company_name}}</b> - {{$settings->slug}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About blocks -->

@endsection