@extends('pre-login.index.index')

@section('content')

<!--Page Title-->
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.contact')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="content-page">
    <div class="container mb-80">
        <div class="row">
            <div class="col-sm-12">
                <article class="post-8">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="margin-bott-20" style="background: #cccccc20; padding-top: 5px; padding-bottom: 5px; text-align: center;">Our Address</h3>
                            <!-- <ul class="info-box"> -->
                            <div class="address"><strong><span class="mdi mdi-map-marker"></span>Address</strong> {{$setting->location}}</div>
                            <div class="address"><strong><span class="mdi mdi-phone"></span>Phone</strong> {{$setting->phone_number}}</div>
                            <div class="address"><strong><span class="mdi mdi-email"></span>Email</strong> {{$setting->email}}</div>
                            <!-- </ul> -->
                        </div>
                        <div class="col-md-4" style="margin-top: 30px;">
                            <h3 class="margin-bott-20">Main Office</h3>
                            <div style="margin-bottom: 20px;">
                                <div class="address"><strong><span class="mdi mdi-account"></span>Abenezer Bekele</strong> </div>
                                <div class="address"><strong><span class="mdi mdi-phone"></span>+251932118929</strong></div>
                                <div class="address"><strong><span class="mdi mdi-email"></span> abenezer@habeshalebs.com</strong> </div>
                            </div>
                            <hr>
                            <div style="margin-top: 20px; margin-bottom: 20px;">
                                <div class="address"><strong><span class="mdi mdi-account"></span>Tensaye Desalegn</strong> </div>
                                <div class="address"><strong><span class="mdi mdi-phone"></span>+251933841405</strong></div>
                                <div class="address"><strong><span class="mdi mdi-email"></span> contact@habeshalebs.com</strong> </div>
                            </div>
                            <hr>
                            <div style="margin-top: 20px;">
                                <div class="address"><strong><span class="mdi mdi-account"></span>Berhan Taye</strong> </div>
                                <div class="address"><strong><span class="mdi mdi-phone"></span> +251913642361</strong></div>
                                <div class="address"><strong><span class="mdi mdi-email"></span> contact@habeshalebs.com</strong> </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 30px;">
                            <h3 class="margin-bott-20">Kuwait Branch</h3>
                            <div>
                                <div class="address"><strong><span class="mdi mdi-account"></span>Amanuel Abi</strong> </div>
                                <div class="address"><strong><span class="mdi mdi-phone"></span>009655058319</strong></div>
                                <div class="address"><strong><span class="mdi mdi-email"></span> mannyabi.m@gmail.com</strong> </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 30px;">
                            <h3 class="margin-bott-20">Australia Branch</h3>
                            <div>
                                <div class="address"><strong><span class="mdi mdi-account"></span> Hana worku</strong> </div>
                                <div class="address"><strong><span class="mdi mdi-phone"></span> +61423457912</strong></div>
                                <div class="address"><strong><span class="mdi mdi-map-marker"></span> Australia Melbourne</strong> </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('contact.store')}}" method="POST" class="product-checkout mt-45" id="contact-form">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <h3 style="background: #cccccc20; padding-top: 5px; padding-bottom: 5px; text-align: center;">Send Message</h3>
                                <div class="row">
                                                                       
                                    
                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="fullname" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            fullname
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="fullname" title="fullname" value="" placeholder="Your Name" type="text" required="" aria-required="true">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="email" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            email
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="contact-email" title="email" value="" placeholder="Email" id="email" type="text" required="" aria-required="true">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="message" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            message
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <textarea name="message" rows="5" class="form-full-width" id="message-contact" placeholder="Message" required="" aria-required="true"></textarea>
                                    </div>
                                </div>
                                 <div class="form-group col-md-12 col-xs-12">
                                    <div class="text-center"><button type="submit" id="send_contact" class="btn-sm btn-color" style="width: 100%;">Send</button></div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </article>
            </div>
        </div>
    </div>

</section>

@endsection