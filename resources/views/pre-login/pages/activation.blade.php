@extends('pre-login.index.index')

@section('content')
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>Activate</span>
                </nav>
            </div>
        </div>
    </div>
</section>


<section class="content-page">
    <div class="container mb-80">
        <div class="row">
            <section class="" style="margin-bottom: 200px;">
                    <div class="home-about-blocks">
                        <div class="col-12 about-blocks-wrap2">
                            <div class="row">
                                <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                    <div class="about-box-inner2">
                                        <h4 class="mb-25">Sorry!</h4><span class="fa fa-check-circle text-center" style="font-size: 50px;"></span>  
                                        <h5 class="mb-20 mt-25">You need to activate your account</h5>
                                        <p>Call {{$settings->customer_support}} To activate your account</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>            
        </div>
    </div>

</section>

@endsection