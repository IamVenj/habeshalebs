@extends('pre-login.index.index')

@section('content')
<!-- Bread Crumb -->
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">Home</a>
                    <span>{{__('app.search_results')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- Bread Crumb -->

<!-- Page Content -->
<section class="content-page">
    <div class="container">
        <div class="row">
        	<input type="hidden" name="csrf-token-" value="{{csrf_token()}}">
            <!-- Product Content -->
            <div class="col-md-9 push-md-3">
                <!-- Title -->
                <div class="list-page-title">
                    <h2 class="">{{$query}}</h2>
                </div>
                <!-- End Title -->

                <!-- Product Filter -->
                <div class="product-filter-content">
                    <div class="product-filter-content-inner">

                        <!--Product Sort By-->
                        <form class="product-sort-by">
                            <label for="short-by">{{__('app.sort_by')}}</label>
                            <select name="sort_by" id="sort_by" class="nice-select-box">
                                <option value="0">{{__('app.Latest')}}</option>
                                <option value="1">{{__('app.Price_High_Low')}}</option>
                                <option value="2">{{__('app.Price_Low_High')}}</option>
                            </select>
                        </form>

                        <!--Product Show-->
                        <form class="product-show">
                            <label for="product-show">{{__('app.Show')}}</label>
                            <select name="records_per_page" id="records_per_page" class="nice-select-box">
                                <option value="12">12</option>
				
								<option value="24">24</option>
				
								<option value="36">36</option>
                            </select>
                        </form>


                        <!--Product List/Grid Icon-->
                        <div class="product-view-switcher">
                            <label>View</label>
                            <div class="product-view-icon product-grid-switcher product-view-icon-active">
                                <a class="" href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-view-icon product-list-switcher">
                                <a class="" href="#"><i class="fa fa-th-list" aria-hidden="true"></i></a>
                            </div>
                        </div>

                    </div>
                </div>


				<div class="row product-list-item" id="get-product">

					@if($category != 0)

					<input type="hidden" name="category_id" value="{{$category}}">

					@endif

					<input type="hidden" id="query" value="{{$query}}">

					@foreach($products as $product)

					<input type="hidden" name="product_id[]" value="{{$product->id}}">

					@endforeach
					
				</div>

                <span id="pagination"></span>

            </div>

			@include('pre-login.partials.filter')

		</div>
    
    </div>

</section>


<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	$(document).ready(function() {

		var view_status = $("#view-status").val();
		
		$("#sort_by").change(function() {

			get_products();

		}).change();



		$("input[name='colors[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $("input[name='size[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $("input[name='design[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $("input[name='vendor_filter[]']").bind('change', function(){
       
	        get_products();
	   
	    });	

	    $('#records_per_page').change(function() {

			get_products();

		}).change();


	    var price_range = [document.getElementById("min-price"), document.getElementById("max-price")];

	      document.getElementById('price-slider').noUiSlider.on('change', function(values) {

	        get_products();

	      });


		function get_products(page)
		{
			var product_id = [];

			$("input[name='product_id[]']").each(function(){
       
		        product_id.push($(this).val());
		   
		    });

			var token = $("input[name=csrf-token-]");

			var category_name = $("input[name=category_name]").val();

			var category_id = $("input[name=category_id]").val();

			var sort_by = $("#sort_by").val();

			var query = $("#query").val();

			var records_per_page = $("#records_per_page").val();

			$.ajaxSetup({

				headers: {

					'X-CSRF-TOKEN': token.val()

				}


			});

			var min_price = $("input[name=min-price]").val();

			var max_price = $("input[name=max-price]").val();

			var multiple_colors_filter = [];

			$("input[name='colors[]']:checked").each(function(){
       
		        multiple_colors_filter.push($(this).val());
		   
		    });

		    var multiple_size_filter = [];

		    $("input[name='size[]']:checked").each(function(){
       
		        multiple_size_filter.push($(this).val());
		   
		    });


		    var design = [];

		    $("input[name='design[]']:checked").each(function(){

				design.push($(this).val());

			});



		    var vendor_filter = [];

			$("input[name='vendor_filter[]']:checked").each(function(){

				vendor_filter.push($(this).val());

			});

			console.log(query);


			$.ajax({

				url: '/search-result',

				type: "POST",

				data: {query:query, category_name: category_name, category_id:category_id, min_price: min_price, max_price: max_price, multiple_size_filter: multiple_size_filter, multiple_colors_filter: multiple_colors_filter, vendor_filter: vendor_filter, design: design, sort_by:sort_by, view_status:view_status, records_per_page: records_per_page, page:page},

				success: function(response) 
				{
					$("#get-product").html(response.data);

					$("#pagination").html(response.pagination);  
				},
				error: function (error)
				{
					console.log(error);
				}

			});
		
		}

		$(document).on('click', '.pagination_link', function() {

			var page = $(this).attr('id');

			get_products(page);

		});

		

	});



</script>

@endsection