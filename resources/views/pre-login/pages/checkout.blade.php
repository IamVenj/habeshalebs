@extends('pre-login.index.index')

@section('content')

<!-- Bread Crumb -->
<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="#">{{__('app.home')}}</a>
                    <span>{{__('app.checkout')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- Bread Crumb -->

<!-- Page Content -->
<section class="content-page">
    <div class="container mb-80">
        <div class="row">
            <div class="col-sm-12">
                <article class="post-8">
                    <form action="{{route('checkout.store')}}" method="POST" class="product-checkout mt-45" id="payment-form">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <h3 style="background: #cccccc20; padding-top: 5px; padding-bottom: 5px; text-align: center;">{{__('app.Billing_details')}}</h3>
                                <div class="row">
                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="billing_first_name" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.firstname')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_first_name" title="{{__('app.firstname')}}" value="{{auth()->user()->firstname}}" placeholder="{{__('app.firstname')}}" id="firstname" type="text" required="" aria-required="true">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="billing_last_name" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.lastname')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_last_name" id="lastname" title="{{__('app.lastname')}}" value="{{auth()->user()->lastname}}" placeholder="{{__('app.lastname')}}" type="text" required="" aria-required="true">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="billing_company" class="left" style="font-family: 'Montserrat', sans-serif;">{{__('app.Company_Name')}}</label>
                                        <input class="input-md form-full-width" name="billing_company" title="{{__('app.Company_Name')}}" value="{{auth()->user()->company_name}}" placeholder="{{__('app.Company_Name')}}" type="text">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="billing_country" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.country')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <select name="billing_country" class="input-md form-full-width" autocomplete="country" tabindex="-1" aria-hidden="true" id="country" required="" aria-required="true" >
                                            <option value="" selected="">Select a country</option>
                                            <option value="AX">Åland Islands</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="PW">Belau</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="VG">British Virgin Islands</option>
                                            <option value="BN">Brunei</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo (Brazzaville)</option>
                                            <option value="CD">Congo (Kinshasa)</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CW">Curaçao</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GG">Guernsey</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard Island and McDonald Islands</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IM">Isle of Man</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="CI">Ivory Coast</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JE">Jersey</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Laos</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macao S.A.R., China</option>
                                            <option value="MK">Macedonia</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia</option>
                                            <option value="MD">Moldova</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="ME">Montenegro</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="KP">North Korea</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PS">Palestinian Territory</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russia</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="ST">São Tomé and Príncipe</option>
                                            <option value="BL">Saint Barthélemy</option>
                                            <option value="SH">Saint Helena</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="SX">Saint Martin (Dutch part)</option>
                                            <option value="MF">Saint Martin (French part)</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="RS">Serbia</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia/Sandwich Islands</option>
                                            <option value="KR">South Korea</option>
                                            <option value="SS">South Sudan</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syria</option>
                                            <option value="TW">Taiwan</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TL">Timor-Leste</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom (UK)</option>
                                            <option value="US">United States (US)</option>
                                            <option value="UM">United States (US) Minor Outlying Islands</option>
                                            <option value="VI">United States (US) Virgin Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VA">Vatican</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Vietnam</option>
                                            <option value="WF">Wallis and Futuna</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="billing_address" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.address')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                            
                                        @if(is_null(auth()->user()->addresses()->first()))
                                  
                                        <input id="__address" class="input-md form-full-width mb-20" type="text" name="map_address" size="50" placeholder="{{__('app.search')}}" required="" area-required="true">

                                        <div id="_map" class="show-map" style="margin-bottom: 20px;"></div>
                                        <input type="hidden" name="lat" id="lat">
                                        <input type="hidden" name="lng" id="lng">
                                     
                                        @else
                                        
                                        <input id="searchTextField{{auth()->user()->addresses()->first()->id}}" class="input-md form-full-width mb-20" type="text" value="{{auth()->user()->addresses()->first()->address}}" name="map_address" size="50" placeholder="{{__('app.search')}}">

                                        <div id="map{{auth()->user()->addresses()->first()->id}}" class="show-map2"></div>
                                        <input type="hidden" name="lat" id="lat{{auth()->user()->addresses()->first()->id}}" value="{{auth()->user()->addresses()->first()->latitude}}">
                                        <input type="hidden" name="lng" id="lng{{auth()->user()->addresses()->first()->id}}" value="{{auth()->user()->addresses()->first()->longitude}}">

                                        @endif

                                        <input class="input-md form-full-width mb-20" name="billing_address" title="Address" value="{{auth()->user()->address}}" placeholder="Street Address" type="text" id="address" required="" aria-required="true">
                                        <input class="input-md form-full-width" name="billing_address_op" title="Address" value="{{auth()->user()->apartment}}" placeholder="Apartment, suite, unit etc. (optional)" type="text">                    
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="billing_town_city" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.State')}} | {{__('app.Province')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_state" title="State" value="{{auth()->user()->state}}" placeholder="" type="text" id="state" required="" aria-required="true">
                                    </div>

                                    <div class="form-field-wrapper form-center col-sm-6">
                                        <label for="billing_city" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.City')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_city" title="City" value="{{auth()->user()->city}}" placeholder="" type="text" id="city" required="" aria-required="true">
                                    </div>
                                    
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="billing_postcode" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.Postcode')}} / {{__('app.ZIP')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_postcode" title="Postcode" value="{{auth()->user()->postcode}}" placeholder="" type="text"  id="postalcode" required="" aria-required="true">
                                    </div>
                                    <div class="form-field-wrapper form-center col-sm-12">
                                        <label for="billing_phone" class="left" style="font-family: 'Montserrat', sans-serif;">
                                            {{__('app.phone_number')}}
                                            <abbr class="form-required" title="required">*</abbr></label>
                                        <input class="input-md form-full-width" name="billing_phone" title="phone" value="{{auth()->user()->phone_number}}" placeholder="(+00) 123 456 7890" type="tel" required="" aria-required="true">
                                    </div>
                                </div>
                                <h3 style="background: #cccccc20; padding-top: 5px; padding-bottom: 5px; text-align: center;">{{__('app.PaymentDetails')}}</h3>
                                <div class="form-group">
                                	 <label for="card-element-stripe" style="font-family: 'Montserrat', sans-serif;">
								      {{__('app.Credit_or_debit_card')}}
								    </label>
								    <div id="card-element-stripe"></div>
								    <div id="card-errors" role="alert"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-order-review">
                                    <h3>{{__('app.YourOrder')}}</h3>
                                    <div class="product-checkout-review-order">
                                        <div class="responsive-table">
                                            <table class="">
                                                <thead>
                                                    <tr>
                                                        <th class="product-name">{{__('app.product')}}</th>
                                                        <th class="product-total">{{__('app.total')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                	@foreach(Cart::content() as $item)
                                                    <tr class="cart_item">
                                                        <td class="product-name">{{$item->model->product_name}}<strong> x {{$item->qty}}</strong></td>
                                                       
                                                        <td class="product-total">
                                                            <span class="product-price-amount amount"><span class="currency-sign">$</span>{{$item->model->current_price * $item->qty}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr class="cart_item">
                                                         <td colspan="2"><textarea placeholder="Personal Statement on order" class="form-control form-full-width" name="personal_statement[]"></textarea></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr class="cart-subtotal">
                                                        <th>{{__('app.SUBTOTAL')}}</th>
                                                        <td>
                                                            <strong><span class="product-price-amount amount"><span class="currency-sign">$</span>{{Cart::subtotal()}}</span></strong>
                                                        </td>
                                                    </tr>
                                                    <tr class="shipping">
                                                        <th>{{__('app.shipping')}}</th>
                                                        <td>
                                                            <ul id="shipping_method">
                                                                <li>
                                                                    <label for="shipping_method_0_legacy_flat_rate">Flat Rate: <span class="woocommerce-Price-currencySymbol">$</span><span class="woocommerce-Price-amount amount flat_rate"></span></label>
                                                                </li>
                                                                
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr class="order-total">
                                                        <th>{{__('app.total')}}</th>
                                                        <td>
                                                            <span class="product-price-amount amount" id="total_price_main"></span>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="product-checkout-payment">
                                           
                                            <div class="place-order">
                                                <button class="btn btn-lg btn-color form-full-width" type="submit" id="checkout-button">Place order</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </article>
            </div>
        </div>
    </div>

</section>
<!-- End Page Content -->
@if(!is_null(auth()->user()->addresses()->first()))
<script type="text/javascript">

    var lat = <?= auth()->user()->addresses()->first()->latitude; ?>;
    var lng = <?= auth()->user()->addresses()->first()->longitude;?>;

    var map = new google.maps.Map(document.getElementById('map'+<?= auth()->user()->addresses()->first()->id; ?>), {
          center: {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
          },
          zoom: 18
        });

        var marker = new google.maps.Marker({
          position: {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
          },
          map: map,
          draggable: true
    });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchTextField'+<?= auth()->user()->addresses()->first()->id; ?>));            

    (function (searchBox, map, marker) {
        google.maps.event.addListener(searchBox, 'places_changed', function() {

                var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for (i = 0; place=places[i]; i++) {
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

                map.fitBounds(bounds);
                map.setZoom(18);

          });
    })(searchBox, map, marker);


        google.maps.event.addListener(marker, 'position_changed', function() {

          var lat = marker.getPosition().lat();
          var lng = marker.getPosition().lng();

          $('#lat'+<?= auth()->user()->addresses()->first()->id; ?>).val(lat);
          $('#lng'+<?= auth()->user()->addresses()->first()->id; ?>).val(lng);

        });

</script>
@endif
@endsection