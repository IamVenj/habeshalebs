@extends('pre-login.index.index')

@section('content')

<!-- Bread Crumb -->
<section class="breadcrumb">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <nav class="breadcrumb-link">

                    <a href="/">{{__('app.home')}}</a>

                    <a href="#">{{__('app.Products')}}</a>

                    <a href="/{{$product->category()->first()->category_name}}/products/{{$product->category()->first()->id}}">{{$product->category()->first()->category_name}}</a>

                    <span>{{$product->product_name}}</span>

                </nav>

            </div>

        </div>

    </div>

</section>
<!-- Bread Crumb -->

<!-- Page Content -->

<section id="product-ID_XXXX" class="content-page single-product-content">

    <!-- Product -->

    <div id="product-detail" class="container">

        <div class="row">

            <!-- Product Image -->

            <div class="col-lg-6 col-md-6 col-sm-12 mb-30">

                <div class="product-page-image">

                    <!-- Slick Image Slider -->

                    <div class="product-image-slider product-image-gallery" id="product-image-gallery" data-pswp-uid="3">

						@foreach($product->productImage()->get() as $proImages)

                        <div class="item">
                            <a class="product-gallery-item" href="<?= Cloudder::show($proImages->image_public_id, array("version" => $proImages->image_version, "quality" => "auto", "height" => 1800, "width"=>1000));?>" data-size="" data-med="<?= Cloudder::show($proImages->image_public_id, array("version" => $proImages->image_version, "quality" => "auto", "height" => 1800, "width"=>1000));?>" data-med-size="">
                                <img src='<?= Cloudder::show($proImages->image_public_id, array("version" => $proImages->image_version, "quality" => "auto", "height" => 1800, "width"=>1000));?>' alt="{{$proImages->image_public_id}}"/>
                            </a>
                        </div>
     
                        @endforeach
                        
                    </div>
     
                    <!-- End Slick Image Slider -->

                    <a href="javascript:void(0)" id="zoom-images-button" class="zoom-images-button"><i class="fa fa-expand" aria-hidden="true"></i></a>


                </div>

                <!-- Slick Thumb Slider -->
                <div class="product-image-slider-thumbnails">

                	@foreach($product->productImage()->get() as $proImages)
   
                    <div class="item">
   
                        <img src='<?= Cloudder::show($proImages->image_public_id, array("version" => $proImages->image_version, "quality" => "auto", "height" => 1800, "width"=>1000));?>' alt="{{$proImages->image_public_id}}"/>
   
                    </div>
   
                    @endforeach
                    
                </div>
   
                <!-- End Slick Thumb Slider -->
   
            </div>
   

            <!-- End Product Image -->

            <!-- Product Content -->
   
            <div class="col-lg-6 col-md-6 col-sm-12 mb-30">
   
                <div class="product-page-content">
   
                    <h2 class="product-title">{{$product->product_name}}</h2>
   
                    <div class="product-rating">
  
                        <a href="#" class="product-rating-count"><span class="count">{{$total_review_count}}</span> Reviews</a>
  
                    </div>
  
                    <div class="product-price">
  
                        <del>${{$product->old_price}} </del><span class="product-price-text"><span class="product-price-sign">$</span>{{$product->current_price}} </span>
  
                    </div>
  
                    <p class="product-description">
  
                        {{$product->slug}}
  
                    </p>
  
                    <form action="/cart" method="post" class="cart-form">

	                    <div class="row product-filters">

                            @if($product->productColors()->count() > 0)

	                        <div class="col-md-6 filters-color">

	                            <label for="select-color">{{__('app.Color')}}</label>

	                            @foreach($product->productColors()->get() as $productColor)

	                            <div class="form-check">

		                            <label class="form-check-label">

		                            	@foreach($productColor->Colors()->get() as $colors)

	                            		<div class="entry" style="background: {{$colors->hex}};">&nbsp;</div>

		                              	<input type="radio" class="form-check-input" name="color" id="color" value="{{$colors->id}}">

		                              	@endforeach

		                            <i class="input-helper"></i></label>

		                        </div>

		                        @endforeach

	                        </div>
                            @endif
                        	@if($product->productSize()->count() > 0)
	                        <div class="col-md-6 filters-size">
	 

	                            <label for="select-size">{{__('app.Size')}}:</label>

	                            @foreach($product->productSize()->get() as $productSIze)

									<div class="form-check">

			                            <label class="form-check-label">

			                              <input type="radio" class="form-check-input" name="size" id="size" value="{{$productSIze->id}}">

			                                {{$productSIze->size}}

			                            <i class="input-helper"></i></label>

			                        </div>

		                         	@endforeach
	                           

	                        </div>
							@endif
	                    </div>
	                    <div class="single-variation-wrap">
	                        <div class="product-quantity">
	                            <span data-value="+" class="quantity-btn quantityPlus"></span>
	                            <input class="quantity input-lg" step="1" min="1" max="9" id="qty" value="1" title="Quantity" type="number" />
	                            <span data-value="-" class="quantity-btn quantityMinus"></span>
	                        </div>
	                        <input type="hidden" name="product_name" value="{{$product->product_name}}">

							<input type="hidden" name="product_price" value="{{$product->current_price}}">

							<input type="hidden" name="product_availability" value="{{$product->availability}}">

							<input type="hidden" name="cart-csrf-token" value="{{csrf_token()}}">

                            <input type="hidden" name="cart-product-id" value="{{$product_id}}">

							<input type="hidden" name="product_weight" value="{{$product->weight}}">

							@auth
	                        <button type="submit" class="btn btn-lg btn-black" id="add_cart"><i class="fa fa-shopping-bag" aria-hidden="true"></i>{{__('app.add_cart')}}</button>
	                        <a style="margin-left: 20px; color: #fff; background: #333; border-radius: 40px; padding: 10px;" onclick="add_wish(<?= $product_id?>)"><i class="fa fa-heart" aria-hidden="true"></i></a>
	                        @else
	                        <a href="/login"><button type="button" class="btn btn-lg btn-black" ><i class="fa fa-shopping-bag" aria-hidden="true"></i>{{__('app.add_cart')}}</button></a>				
	                        <a style="margin-left: 20px; color: #fff; background: #333; border-radius: 40px; padding: 10px;" href="/login"><i class="fa fa-heart" aria-hidden="true"></i></a>
	                        @endauth

	                    </div>
	                </form>
	                <hr>
                    <div class="product-meta">
                        <span>SKU : <span class="sku" itemprop="sku">{{$product->sku}}</span></span>
                        <span>{{__('app.Category')}} : <span class="category" itemprop="category">{{$product->category()->first()->category_name}}</span></span>
                        <span>{{__('app.Availability')}}: <span class="category" itemprop="category">{{$product->availability}} {{__('app.left')}}</span></span>
                        <span><strong>{{__('app.Brand')}}: <span class="category" itemprop="category">{{$product->brand}}</span></span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Product -->

    <!-- Product Content Tab -->
    <div class="product-tabs-wrapper container">
        <ul class="product-content-tabs nav nav-tabs" role="tablist">

            <li class="nav-item">
                <a class="" href="#tab_description" role="tab" data-toggle="tab">{{__('app.Description')}}</a>
            </li>
            <li class="nav-item">
                <a class="" href="#tab_additional_information" role="tab" data-toggle="tab">{{__('app.Additional_Information')}}</a>
            </li>
            <li class="nav-item">
                <a class="" href="#tab_location" role="tab" data-toggle="tab">{{__('app.Item_Location')}}</a>
            </li>
            <li class="nav-item">
                <a class="" href="#tab_reviews" role="tab" data-toggle="tab">{{__('app.Reviews')}} (<span>{{$total_review_count}}</span>)</a>
            </li>

        </ul>
        <div class="product-content-Tabs_wraper tab-content container">
            <div id="tab_description" role="tabpanel" class="tab-pane fade in active">
                <!-- Accordian Title -->
                <h6 class="product-collapse-title" data-toggle="collapse" data-target="#tab_description-coll">Description</h6>
                <!-- End Accordian Title -->
                <!-- Accordian Content -->
                <div id="tab_description-coll" class="shop_description product-collapse collapse container">
                    <div class="row">
                        <div class=" col-md-12">
                            <p>
                                {{$product->slug}}
                            </p>
                            
                        </div>
                    </div>
                </div>
                <!-- End Accordian Content -->
            </div>

            <div id="tab_additional_information" role="tabpanel" class="tab-pane fade">

                <h6 class="product-collapse-title" data-toggle="collapse" data-target="#tab_additional_information-coll">{{__('app.Additional_Information')}}</h6>

                <div id="tab_additional_information-coll" class="container product-collapse collapse">

                    <table class="shop_attributes">
                        <tbody>
                            <tr>
                                <th>{{__('app.weight_in_kg')}}</th>
                                <td>{{$product->weight}} kg</td>
                            </tr>
                            <tr>
                                <th>{{__('app.washcare')}}</th>
                                <td>{{$product->washcare}}</td>
                            </tr>
                            <tr>
                                <th>{{__('app.composition')}}</th>
                                <td>{{$product->composition}}</td>
                            </tr>
                            <tr>
                                <th>{{__('app.lining_composition')}}</th>
                                <td>{{$product->lining_composition}}</td>
                            </tr>
                            <tr>
                                <th>{{__('app.design')}}</th>
                                <td>{{$product->styling}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <!-- End Accordian Content -->
            </div>

            <div id="tab_location" role="tabpanel" class="tab-pane fade">

                <h6 class="product-collapse-title" data-toggle="collapse" data-target="#tab_location-coll">{{__('app.Item_Location')}}</h6>

                <div id="tab_location-coll" class="shop_description product-collapse collapse container">

                    <?php $count = 0;?>

					<div class="row">
						
						@for($i = 0; $i < $locations->count(); $i++)
						
						<div class="col-md-4">
							
							<?php $count = $count + 1;?>

							<div class="show-map" id="map{{$count}}"></div>

							<input type="hidden" id="lat{{$count}}" value="{{$locations[$i]->latitude}}">

							<input type="hidden" id="lng{{$count}}" value="{{$locations[$i]->longitude}}">
						
						</div>

						@endfor

					</div>

                </div>
                <!-- End Accordian Content -->
            </div>
            <div id="tab_reviews" role="tabpanel" class="tab-pane fade">
                <!-- Accordian Title -->
                <h6 class="product-collapse-title" data-toggle="collapse" data-target="#tab_reviews-coll">{{__('app.Reviews')}} ({{$total_review_count}})</h6>
                <!-- End Accordian Title -->
                <!-- Accordian Content -->
                <div id="tab_reviews-coll" class=" product-collapse collapse container">
                    <div class="row">
                        <div class="review-form-wrapper col-md-6">
                            <h6 class="review-title">{{__('app.add_review')}} </h6>
                            <form class="review-form" action="/review" method="post">
								@csrf

								<input type="hidden" name="product_id" value="{{$product_id}}">
                                <div class="form-field-wrapper">
                                    <label>{{__('app.your_rating')}}</label>

                                    <div class="stars">

										<input type="radio" id="star10" name="rating" value="5" name="rating" /><label for="star10"></label>

										<input type="radio" id="star9" name="rating" value="4" name="rating" /><label for="star9"></label>

										<input type="radio" id="star8" name="rating" value="3" name="rating" /><label for="star8"></label>

										<input type="radio" id="star7" name="rating" value="2" name="rating" /><label for="star7"></label>

										<input type="radio" id="star6" name="rating" value="1" name="rating" /><label for="star6"></label>

									</div>
                                </div>
                                <div class="form-field-wrapper">
                                    <label>{{__('app.your_review')}} <span class="required">*</span></label>
                                    <textarea id="comment" class="form-full-width" name="review" cols="45" rows="8" aria-required="true" required=""></textarea>
                                </div>
                                <div class="form-field-wrapper">
                                	@auth
                                    <button name="submit" class="submit btn btn-md btn-color" type="submit">{{__('app.Submit')}}</button>
                                    @else
                                    <a href="/login"><button name="submit" class="submit btn btn-md btn-color" type="submit">{{__('app.Submit')}}</button></a>
                                    @endauth
                                </div>
                            </form>
                        </div>
                        <div class="comments col-md-6">
                            <h6 class="review-title">{{__('app.Reviews')}}</h6>
                            <!--<p class="review-blank">There are no reviews yet.</p>-->
                            <ul class="commentlist">
                                <li>
                                    @auth

                                    @if(!is_null($current_user_review))

                                        <div class="single-review">

                                            <div class="review-heading">

                                                <div><i class="fa fa-user-o"></i>@if(!is_null($current_user_review->user()->first()->firstname)) {{$current_user_review->user()->first()->firstname.' '.$current_user_review->user()->first()->lastname}} @else {{$current_user_review->user()->first()->shop_name}} @endif</div>

                                                <div><i class="fa fa-clock-o"></i> <?= date('d M Y', strtotime($current_user_review->created_at));?></div>

                                                <div class="review-rating pull-right">

                                                    @if($current_user_review->rating == 1)

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    @elseif($current_user_review->rating == 2)

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    @elseif($current_user_review->rating == 3)

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o empty"></i>

                                                    <i class="fa fa-star-o empty"></i>
                                                    
                                                    @elseif($current_user_review->rating == 4)

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o empty"></i>
                                                    
                                                    @elseif($current_user_review->rating == 5)

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>
                                                    
                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star"></i>

                                                    @endif

                                                </div>

                                            </div>

                                            <div class="review-body">

                                                <p>{{$current_user_review->review}}</p>

                                                @include('pre-login.partials.modal.product-detail-modal')

                                                 <a data-toggle="modal" href="#" data-target="#edit-review<?= $current_user_review->id;?>"><button class="btn btn-sm btn-black"><span class="fa fa-edit"></span></button></a>

                                                 <a data-toggle="modal" href="#" data-target="#delete-review<?= $current_user_review->id;?>"><button class="btn btn-sm btn-black"><span class="fa fa-trash"></span></button></a>

                                            </div>

                                        </div>

                                        @endif

                                    @endauth
                                   
                                </li>

                                <li id="comment-102" class="comment-102">
                                    @foreach($reviews as $review)
                                            
                                    <div class="single-review">

                                        <div class="review-heading">

                                            <div><a href="#"><i class="fa fa-user-o"></i>@if(!is_null($review->user()->first()->firstname)) {{$review->user()->first()->firstname.' '.$review->user()->first()->lastname}} @else {{$review->user()->first()->shop_name}} @endif</a></div>

                                            <div><a href="#"><i class="fa fa-clock-o"></i> <?= date('d M Y', strtotime($review->created_at));?></a></div>

                                            <div class="review-rating pull-right">

                                                @if($review->rating == 1)

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                @elseif($review->rating == 2)

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                @elseif($review->rating == 3)

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star-o empty"></i>

                                                <i class="fa fa-star-o empty"></i>
                                                
                                                @elseif($review->rating == 4)

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star-o empty"></i>
                                                
                                                @elseif($review->rating == 5)

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>
                                                
                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star"></i>

                                                @endif

                                            </div>

                                        </div>

                                        <div class="review-body">

                                            <p>{{$review->review}}</p>

                                        </div>


                                    </div>

                                    @endforeach


                                    <li>{{$reviews->links()}}</li>

                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Accordian Content -->
            </div>

        </div>
    </div>
    <!-- End Product Content Tab -->

    <!-- Product Carousel -->
    <div class="container product-carousel">
        <h2 class="page-title">{{__('app.related')}}</h2>
        
        <div id="new-tranding" class="product-item-4 owl-carousel owl-theme nf-carousel-theme1">

        	@foreach($related_products as $product)
            <!-- item.1 -->
            <div class="product-item">
	            <div class="product-item-inner">
	            	@if ($product->current_price < $product->old_price)
	            	<div class="sale-label">
	            		<?= round(($product->current_price/$product->old_price)*100);?> %</div>
					@endif
	                <div class="product-img-wrap">
	                    <img src="<?= Cloudder::show($product->productImage->first()->image_public_id, array("quality" => "auto", "height" => 400, "width"=>400)).'" alt="'.$product->productImage->first()->image_public_id; ?>" alt="{{$product->product_name}}"  style="height: 300px; width: 300px; object-fit:cover;">
	                </div>
	                <div class="product-button">
	                	@guest
	                    <a href="/login" class="js_tooltip" data-mode="top" data-tip="Add To Wishlist"><i class="fa fa-heart"></i></a>
	                    @else
	                    <a class="js_tooltip" data-mode="top" data-tip="Add To Wishlist" onclick="add_wish(<?= $product->id;?>)"><i class="fa fa-heart"></i></a>
	                    @endguest
	                    <a href="/{{$product->product_name}}/detail/{{$product->id}}" class="js_tooltip" data-mode="top" data-tip="Quick&nbsp;View"><i class="fa fa-eye"></i></a>
	                </div>
	            </div>
	            <div class="product-detail">
	                <a class="tag" href="/{{$product->category->category_name}}/products/{{$product->category->id}}">{{$product->category->category_name}}</a>
	                <p class="product-title"><a href="">{{$product->product_name}}</a></p>
	                <p class="product-rating">
	                    {{__('app.sold_by')}}: {{$product->user->shop_name}}
	                </p>
	                <p class="product-description">
	                    {{$product->slug}}
	                </p>
	                <h5 class="item-price"><del>
	                @if(!is_null($product->old_price))
	            	${{$product->old_price}} 
	            	@endif
	            	</del> ${{$product->current_price}} </h5>
	            </div>
	        </div>
	        
            @endforeach

        </div>
    </div>
    <!-- End Product Carousel -->

</section>

<div class="message"></div>


<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	for (var i = 1; i < <?= $locations->count() + 1; ?>; i++) {
		
		var lat = $("#lat"+i).val();
		var lng = $("#lng"+i).val();

		var map = new google.maps.Map(document.getElementById("map"+i), {

			center: {
				lat: parseFloat(lat), 
				lng: parseFloat(lng)
			},
			zoom: 18

		});

		var marker = new google.maps.Marker({

			position: {
				lat: parseFloat(lat),
				lng: parseFloat(lng)
			},
			map: map

		});

	}
	
	
</script>


@endsection