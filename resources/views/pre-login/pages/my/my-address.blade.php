@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.my_address')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>

@include('pre-login.partials.modal.address-modal')

<section class="content-page">
    <div class="container mb-80">
        <div class="row">

            @if($locations->count() == 0)
                <section class="" style="margin-bottom: 200px;">
                        <div class="home-about-blocks">
                            <div class="col-12 about-blocks-wrap2">
                                <div class="row">
                                    <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                        <div class="about-box-inner2">
                                            <h4 class="mb-25">Sorry!</h4><span class="fa fa-map-marker text-center" style="font-size: 50px;"></span>  
                                            <h5 class="mb-20 mt-25">Empty</h5>
                                            <p>You don't not have any listed addresses</p>
                                            <a data-toggle="modal" href="#" data-target="#add-address2">
                                                <button class="main-btn" style="font-size: 20px;">
                                                    <i class="fa fa-plus-circle"></i>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                </section>
            @else
        	
            <div class="col-sm-12">
                <article class="post-8">
                    <?php $count = 0;?>
			  		<input type="hidden" name="location_count" value="{{$locations->count() + 1}}">
               
                    <div class="row">
					
					@for($i = 0; $i < $locations->count(); $i++)
					   @include('post-login.partials.modal.address-modal')
						
						<div class="col-md-12">
							
							<?php $count = $count + 1;?>

							<a data-toggle="modal" href="#" data-target="#edit-address{{$locations[$i]->id}}" style="color: #fff; text-decoration: none;">

								<button class="btn btn-sm btn-black mb-2" onclick="show_map(<?= $locations[$i]->latitude; ?>, <?= $locations[$i]->longitude;?>, <?= $locations[$i]->id; ?>)"><i class="mdi mdi-pen"></i></button>

							</a>

							<div class="show-map" id="mapz{{$count}}"></div>
							<input type="hidden" id="lat{{$count}}" value="{{$locations[$i]->latitude}}">
							<input type="hidden" id="lng{{$count}}" value="{{$locations[$i]->longitude}}">

						</div>

					@endfor

					</div>
				                            
                </article>
            </div>
            @endif
        </div>
    </div>

</section>


@endsection