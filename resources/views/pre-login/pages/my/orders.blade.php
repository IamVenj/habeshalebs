@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.Orders')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>

<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<section class="content-page" id="section-order" style="display: none;">
    <div class="container mb-80">
        <div class="row">
        	@if(is_null($orders))
				<section class="" style="margin-bottom: 200px;">
                        <div class="home-about-blocks">
                            <div class="col-12 about-blocks-wrap2">
                                <div class="row">
                                    <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                        <div class="about-box-inner2">
                                            <h4 class="mb-25">Sorry!</h4><span class="fa fa-shopping-bag text-center" style="font-size: 50px;"></span>  
                                            <h5 class="mb-20 mt-25">Empty</h5>
                                            <p>You have not ordered any item</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
			@else
            <div class="col-sm-12">
                <article class="post-8">
                    <div class="cart-product-table-wrap responsive-table">
                        <table class="table table-bordered dataTable1">
                            <thead>
                                <tr>
                                	<th>{{__('app.order')}} #</th>
				                  	<th>{{__('app.order_date')}}</th>
				                  	<th>{{__('app.item_name')}}</th>
				                  	<th>{{__('app.Item_SKU')}}</th>
				                  	<th>{{__('app.price')}}</th>
				                  	<th>{{__('app.quantity')}}</th>
				                  	<th>{{__('app.Total_Price')}} + {{__('app.shipping')}}</th>
				                  	<th>{{__('app.Vendor')}}</th>
				                  	<th>{{__('app.vendors_phone_number')}}</th>
				                  	<th>{{__('app.Payment_status')}}</th>
				                  	<th>{{__('app.shipping')}}</th>
				                  	<th>{{__('app.Size')}}</th>
				                  	<th>{{__('app.Sent_Date')}}</th>
				                  	<th class="product-remove"></th>
                                </tr>
                            </thead>
                            <tbody>
                            	
                                @foreach($orders as $order)
                                <tr>
                                    @include('pre-login.partials.modal.delivery-modal')

				                  	<td>{{$order->order_code}}</td>
				                  	<td><?= date('d M y' ,strtotime($order->created_at));?></td>
				                  	<td>{{$order->product->product_name}}</td>				                  	
				                 	<td>{{$order->product->sku}}</td>
				                  	<td>{{$order->product->current_price}}</td>
				                  	<td>x{{$order->quantity}}</td>
				                  	<td>${{$order->paid_amount}}</td>
				                  	<td>{{$order->product->user()->first()->shop_name}}</td>
				                  	<td>{{$order->product->user()->first()->phone_number}}</td>

				                  	<td>

				                    	@if($order->payment_status == 1)

						              	<label class="badge badge-success" style="background-color: green;">
						              		Paid
						              		<span class="mdi mdi-check" style="margin-left: 10px;"></span>
						              	</label>

						              	@else

						              	<label class="badge badge-danger" style="background-color: red;">
						              		Not Paid
						              		<span class="mdi mdi-close" style="margin-left: 10px;"></span>
						              	</label>

						              	@endif

				                  	</td>

				                  	<td>${{$order->shipping}}</td>
				                  	<td>{{$order->productSize->size}}</td>

				                  	<td>{{$order->created_at->diffForHumans()}}</td>

				                  	@if($order->vendor_delivery_confirmation == 1 && $order->customer_delivery_confirmation == 1)

				                  	<td>

				                  		<p class="text-success" style="font-weight: bold; font-size: 15px;">Delivered <span class="fa fa-check" style="padding: 10px; border-radius: 50px; background-color: green; color: #fff;"></span></p>

				                  	</td>

				                  	@elseif($order->vendor_delivery_confirmation == 1 && $order->customer_delivery_confirmation == 0)

				                  	<td>

				                    	<a data-toggle="modal" href="#" data-target="#delivery-status<?= $order->id; ?>"><button class="btn btn-primary">is the item delivered?</button></a>

				                  	</td>

				                  	@else

				                  	<td>
				                  		
				                  		<a data-toggle="modal" href="#" data-target="#cancel-order<?= $order->id; ?>"><button class="main-btn icon-btn"><i class="fa fa-close"></i></button></a>

				                  	</td>

				                  	@endif
                                </tr>

								@endforeach
                            </tbody>
                        </table>
                    </div>
                        
                    
                </article>
            </div>

            @endif
        </div>
    </div>

</section>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

		$(".dataTable1").DataTable();
	
	});

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-order").style.display = 'block';

	});

</script>

@endsection