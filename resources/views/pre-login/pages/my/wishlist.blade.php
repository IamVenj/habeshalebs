@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.my_wishlist')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>

<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->


<section class="content-page" id="section-wish" style="display: none;">
    <div class="container mb-80">
        <div class="row">
        	@if($user_wishes->count() == 0)
				<section class="" style="margin-bottom: 200px;">
                        <div class="home-about-blocks">
                            <div class="col-12 about-blocks-wrap2">
                                <div class="row">
                                    <div class="col-sm-12" style="box-shadow: 0px 10px 15px rgba(50, 50, 50, 0.1);">
                                        <div class="about-box-inner2">
                                            <h4 class="mb-25">Sorry!</h4><span class="fa fa-shopping-bag text-center" style="font-size: 50px;"></span>  
                                            <h5 class="mb-20 mt-25">Your WishList is empty</h5>
                                            <p>You have not added any wishes to your Wish list</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
			@else
            <div class="col-sm-12">
                <article class="post-8">
                    <div class="cart-product-table-wrap responsive-table">
                        <table>
                            <thead>
                                <tr>
                                    <th>{{__('app.product')}}</th>
				                  	<th>{{__('app.product_name')}}</th>
				                  	<th>{{__('app.Brand')}}</th>
				                  	<th>{{__('app.price')}}</th>
				                  	<th>{{__('app.Vendor')}}</th>
				                  	<th>{{__('app.Location')}}</th>
				                  	<th class="product-remove"></th>
                                </tr>
                            </thead>
                            <tbody>
                            	
                                @foreach($user_wishes as $wish)
                                <tr>
                                    <td class="thumb"><img src="<?= Cloudder::show($wish->product->productImage()->first()->image_public_id, array("version" => $wish->product->productImage()->first()->image_version, "quality" => "auto", "height" => 200, "width"=>200))?>" alt="{{$wish->product->productImage()->first()->image_public_id}}"></td>

				                  	<td>{{$wish->product->product_name}}</td>
					                <td>{{$wish->product->brand}}</td>
					                <td>{{$wish->product->current_price}}</td>
					                <td>{{$wish->product->user()->first()->shop_name}}</td>
					                <td>{{$wish->product->product_location}}</td>
									<td>
					                  	<form action="/remove-wish/{{$wish->id}}" method="post">
					                  		@csrf
					                  		@method("DELETE")
					                    	<button class="main-btn icon-btn" type="submit"><i class="fa fa-close"></i></button>
					                  	</form>
					                  </td>
                                </tr>

								@endforeach
                            </tbody>
                        </table>
                    </div>
                        
                    
                </article>
            </div>

            @endif
        </div>
    </div>

</section>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	$(document).ready(function() {

		$(".dataTable1").DataTable();
	
	});

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-wish").style.display = 'block';

	});

</script>

@endsection