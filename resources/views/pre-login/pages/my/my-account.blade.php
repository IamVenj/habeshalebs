@extends('pre-login.index.index')

@section('content')

<!-- BREADCRUMB -->

<section class="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="/">{{__('app.home')}}</a>
                    <span>{{__('app.my_account')}}</span>
                </nav>
            </div>
        </div>
    </div>
</section>

@include('post-login.partials.modal.account-modal')
<!-- /BREADCRUMB -->

<div class="demo" id="section-load"></div>

<!-- section -->

<section class="content-page" id="section-wish" style="display: none;">
    <div class="container">
    	<div class="row" style="margin-bottom: 20px;">
        	<div class="col-md-12">
        		
        		<a href="#" data-toggle="modal" href="#" data-target="#upassword-change" style="color: #fff; text-decoration: none;"><button type="button" class="btn btn-sm btn-black" style="width: 100%;">Change Password</button></a>

        	</div>		
        </div>
        <hr>
        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-12">
                <article class="post-8">

			        <form action="/account" method="post">
			        	@csrf
			        	@method('PATCH')
				    	<div class="section">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">

							            <label for="name" style="font-size: 15px;">Firstname</label>
							            <input type="text" name="name" value="{{$user->firstname}}" class="input-md form-full-width" id="name" placeholder="Name" style="font-size: 15px; " required="" area-required="true">

							        
							        </div>
								</div>

								<div class="col-md-6">
									<div class="form-group">

							            <label for="name" style="font-size: 15px;">Lastname</label>
							            <input type="text" name="name" value="{{$user->lastname}}" class="input-md form-full-width" id="name" placeholder="Name" style="font-size: 15px; " required="" area-required="true">
							        
							        </div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
				         
							            <label for="email" style="font-size: 15px;">Email</label>
							            <input type="email" name="email" value="{{$user->email}}" class="input-md form-full-width" id="email" placeholder="email" style="font-size: 15px; " required="" area-required="true">
							        
							        </div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
				         
							            <label for="phone_number" style="font-size: 15px;">Phone Number</label>
							            <input type="text" name="phone_number" value="{{$user->phone_number}}" class="input-md form-full-width" id="phone_number" placeholder="Phone Number" style="font-size: 15px; " required="" area-required="true">
							        
							        </div>
								</div>

							</div>
				        </div>
							
						<button class="main-btn primary-btn">Update</button>

					</form>
				</article>
			</div>

		</div>
		
		
	</div>
</section>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	$(window).on('load', function() {

		document.getElementById("section-load").style.display = 'none';
		
		document.getElementById("section-wish").style.display = 'block';

	});
</script>

@endsection