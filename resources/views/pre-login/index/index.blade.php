<?php $setting = App\CompanySettings::first();?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{csrf_token()}}">

	<title>{{$setting->company_name}}</title>

	<link href="{{URL::asset('plugins/rev_slider/css/settings-ver.5.3.1.css')}}" rel="stylesheet" type="text/css" />
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/slick.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/slick-theme.css')}}" />

	<link rel="stylesheet" href="{{URL::asset('vendors/mdi/css/materialdesignicons.min.css')}}">
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/nouislider.min.css')}}" />
	<link rel="stylesheet" href="{{URL::asset('css/font-awesome.min.css')}}">

    <link href="{{URL::asset('css/plugins/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('css/plugins/bootstrap.css')}}" rel="stylesheet" type="text/css" />

  	@if(Request::is('wishes') || Request::is('my-orders'))
	<link rel="stylesheet" href="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
	@endif

	<link rel="shortcut icon" href="{{URL::asset('storage/uploads/favicon.png')}}" />
	<link type="text/css" rel="stylesheet" href="{{URL::asset('css/style.css')}}" />

</head>

<body>

	<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBPCRMMp4wCB1RYgrj0lavgRLSn-v9kz8g&libraries=places"></script>

	@if(Request::is('checkout'))
		<script src="https://js.stripe.com/v3/"></script>
	@endif

	<div class="jumping-dots-loader" id="loader-main" style="position: absolute; top: 40%; left: 48%;">

	    <span></span>
	    <span></span>
	    <span></span>

  	</div>

  	@include('pre-login.partials.newsletter-popup')
  	@include('pre-login.partials.search')
  	@include('pre-login.partials.sidebar-cart')

  	<div class="wraper" id="loading-content" style="opacity: 0;">
		@include('pre-login.index.header')
		<div class="page-content-wraper">

			@yield('content')
			@include('pre-login.index.footer')

		</div>
  	</div>

  	<div class="message" style="z-index: 100000000000 !important;"></div>
	
	<script src="{{URL::asset('js/jquery.min.js')}}"></script>

	@if(Request::is('my-address') || Request::is('checkout'))
		<script src="{{URL::asset('js/_customJs_/map_address.js')}}"></script>
	@endif

	@if(Request::is('contact'))
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	@endif

	<script src="{{URL::asset('js/nouislider.min.js')}}"></script>
	<script src="{{URL::asset('js/slick.min.js')}}"></script>
	<script src="{{URL::asset('js/sweetalert.min.js')}}"></script>
	
	<script src="{{URL::asset('js/plugins/jquery-ui.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{URL::asset('plugins/rev_slider/js/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{URL::asset('js/plugins/tether.min.js')}}"></script>
    <script src="{{URL::asset('js/plugins/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('js/plugins/owl.carousel.js')}}"></script>
    <script src="{{URL::asset('js/plugins/plugins-all.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

  	@if(Request::is('wishes') || Request::is('my-orders'))

  	<script src="{{URL::asset('vendors/datatables.net/jquery.dataTables.js')}}"></script>
  	<script src="{{URL::asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>

  	@endif

	<script src="{{URL::asset('js/bootstrap3-typeahead.min.js')}}"></script>

  	<!-- Custom JS -->

  	<script type="text/javascript">
		
		$(window).on('load', function() {

  			document.getElementById('loader-main').style.opacity = 0;
  			document.getElementById('loading-content').style.opacity = 1;

  		});

	</script>

    <script src="{{URL::asset('js/custom.js')}}"></script>
	<script src="{{URL::asset('js/money.js')}}"></script>
	
	@if(auth()->user())

		<script src="{{URL::asset('js/_customJs_/cart.js')}}"></script>
		<script src="{{URL::asset('js/_customJs_/wishlist.js')}}"></script>

		@if(Request::is('checkout'))
		<script src="{{URL::asset('js/_customJs_/stripe.js')}}"></script>
		@endif
	
	@endif

	<script src="{{URL::asset('js/_customJs_/lang.js')}}"></script>

	<script src="{{URL::asset('js/_customJs_/search-products.js')}}"></script>
	<script src="{{URL::asset('js/_customJs_/newsletter.js')}}"></script>

</body>

</html>