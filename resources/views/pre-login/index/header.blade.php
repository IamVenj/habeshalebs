<?php
$settings = App\CompanySettings::first();
?>
<header class="header" style="height: none;">
    <!--Topbar-->
    <div class="header-topbar">
        <div class="header-topbar-inner">
            <!--Topbar Left-->
            <div class="topbar-left hidden-sm-down">
                <div class="phone"><i class="fa fa-phone left" aria-hidden="true"></i>{{__('app.CustomerSupport')}} : <b>{{$settings->customer_support}}</b></div>
            </div>
            <!--End Topbar Left-->

            <!--Topbar Right-->
            <div class="topbar-right">
                <ul class="list-none">
                    <li>
                    	@guest
                        <a href="/login"><i class="fa fa-lock left" aria-hidden="true"></i><span class="hidden-sm-down">{{__('app.login')}}</span></a>
                        @endguest
                    </li>
                    <li class="dropdown-nav">
                    	@guest
                        <a href="/login"><i class="fa fa-user left" aria-hidden="true"></i><span class="hidden-sm-down">{{__('app.my_account')}}</span><i class="fa fa-angle-down right" aria-hidden="true"></i></a>
                        
                        @else
                        <a href="/account"><i class="fa fa-user left" aria-hidden="true"></i><span class="hidden-sm-down">{{__('app.my_account')}}</span><i class="fa fa-angle-down right" aria-hidden="true"></i></a>
                        @endif
                        <div class="dropdown-menu">
                            <ul>
                                @auth
                                <li><a href="/account">{{__('app.my_account')}}</a></li>
                                <li><a href="/my-address">{{__('app.my_address')}}</a></li>
                                <li><a href="/my-orders">{{__('app.OrderHistory')}}</a></li>
                                <li><a href="/wishes">{{__('app.my_wishlist')}}</a></li>
                                @endauth
                            </ul>
                            <span class="divider"></span>
                            <ul>
                            	@guest
                                <li><a href="/login"><i class="fa fa-unlock left" aria-hidden="true"></i>{{__('app.login')}}</a></li>
                                <li><a href="/register"><i class="fa fa-user left" aria-hidden="true"></i>{{__('app.createAccount')}}</a></li>
                                @endguest
                                 @auth
                                <li><a href="/logout"><i class="fa fa-lock left" aria-hidden="true"></i>{{__('app.logout')}}</a></li>
                                @endauth
                            </ul>
                        </div>
                        <!--End Dropdown-->
                    </li>
                    <li class="dropdown-nav">
                    	@if(app()->getLocale() == 'en')

                        <a href="#">
                        	{{__('app.short_eng')}}<i class="fa fa-angle-down right" aria-hidden="true"></i>
                        </a>

						@elseif(app()->getLocale() == 'am')

                        <a href="#">
                        	{{__('app.amh')}}<i class="fa fa-angle-down right" aria-hidden="true"></i>
                        </a>

						@endif
                        <!--Dropdown-->
                        <div class="dropdown-menu">
                            <ul>
                            	<li onclick="change_language('en')">

									<a style="cursor: pointer;">
											
										{{__('app.eng')}} ({{__('app.short_eng')}})

									</a>

								</li>

								<li onclick="change_language('am')">

									<a style="cursor: pointer;">

										{{__('app.amh')}} ({{__('app.short_amh')}})

									</a>

								</li>
                            </ul>
                        </div>
                        <!--End Dropdown-->
                    </li>
                    <li>
                        <a href="/contact">{{__('app.contact')}}</a>
                    </li>
                </ul>
            </div>
            <!-- End Topbar Right -->
        </div>
    </div>
    <!--End Topbart-->

    <!-- Header Container -->
    <div id="header-sticky" class="header-main">
        <div class="header-main-inner">
            <!-- Logo -->
            <div class="logo">
                <a href="/">
                    <img src="{{asset('storage/uploads/logo.png')}}" alt="{{$settings->company_name}}" />
                </a>
            </div>
            <!-- End Logo -->


            <!-- Right Sidebar Nav -->
            <div class="header-rightside-nav">
                

                <!-- Sidebar Icon -->
                <div class="sidebar-icon-nav">
                    <ul class="list-none-ib">
                        <!-- Search-->
                        <li><a id="search-overlay-menu-btn"><i aria-hidden="true" class="fa fa-search"></i></a></li>

                        <!-- Whishlist-->
                        <li><a class="js_whishlist-btn" href="/wishes"><i aria-hidden="true" class="fa fa-heart"></i></a></li>

                        <!-- Cart-->
                        <li><a id="sidebar_toggle_btn">
                            <div class="cart-icon">
                                <i aria-hidden="true" class="fa fa-shopping-bag"></i>
                            </div>

                            <div class="cart-title">
                                <span class="countTip cartCount"></span>
                                <span class="cart-price strong cart-total-price"></span>
                            </div>
                        </a></li>
                    </ul>
                </div>
                <!-- End Sidebar Icon -->
            </div>
            <!-- End Right Sidebar Nav -->


            <!-- Navigation Menu -->
            <nav class="navigation-menu">
                <ul>
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <?php

					$categories = App\Category::latest()->get();

					?>
					@foreach($categories as $category)
                    <li><a href="/{{$category->category_name}}/products/{{$category->id}}">{{$category->category_name}}</a></li>
                    @endforeach
                </ul>
            </nav>

        </div>
    </div>
    <!-- End Header Container -->
</header>