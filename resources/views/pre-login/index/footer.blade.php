<?php

$categories = App\Category::latest()->take(6)->get();

?>

<!-- Footer Section -------------->
<footer class="footer section-padding">
    <!-- Footer Info -->
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 mb-sm-45">
                <div class="footer-block about-us-block">
                    <img src="{{URL::asset('storage/uploads/logo.png')}}" width="167" alt="">
                    <p>{{$setting->slug}}</p>
                    <ul class="footer-social-icon list-none-ib">
                        @if(!is_null($setting->facebook))
                        <li><a href="{{$setting->facebook}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        @endif
                        @if(!is_null($setting->twitter))
                        <li><a href="{{$setting->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        @endif
                        @if(!is_null($setting->pinterest))
                        <li><a href="{{$setting->pinterest}}" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                        @endif
                        @if(!is_null($setting->google_plus))
                        <li><a href="{{$setting->google_plus}}" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        @endif
                         @if(!is_null($setting->instagram))
                        <li><a href="{{$setting->instagram}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div><!-- 
            <div class="col-lg-2 col-md-4 col-sm-4 mb-sm-45">
                <div class="footer-block information-block">
                    <h6>Information</h6>
                    <ul>
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="#">Terms &amp; Condition</a></li>
                    </ul>
                </div>
            </div> -->
            <div class="col-lg-2 col-md-4 col-sm-4 mb-sm-45">
                <div class="footer-block links-block">
                    <h6>{{__('app.my_account')}}</h6>
                    <ul>
                        @auth
                        <li><a href="/account">{{__('app.my_account')}}</a></li>
                        <li><a href="/my-orders">{{__('app.OrderHistory')}}</a></li>
                        <li><a href="/wishes">{{__('app.my_wishlist')}}</a></li>
                        <li><a href="/cart">{{__('app.my_cart')}}</a></li>
                        @endauth
                        @guest
                        <li><a href="/login">{{__('app.login')}}</a></li>
                        <li><a href="/register">{{__('app.createAccount')}}</a></li>
                        @endguest
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 mb-sm-45">
                <div class="footer-block extra-block">
                    <h6>{{__('app.categories')}}</h6>
                    <ul>
                        @foreach($categories as $category)
                        <li><a href="/{{$category->category_name}}/products/{{$category->id}}">{{$category->category_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
                <div class="footer-block contact-block">
                    <h6>{{__('app.contact')}}</h6>
                    <ul>
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{$setting->location}}</li>
                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:{{$setting->email}}">{{$setting->email}}</a></li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i>{{$setting->phone_number}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Info -->

    <!-- Footer Newsletter -->
    <div class="container">
        <div class="footer-newsletter">
            <h4>{{__('app.SubscribeNewsletter')}}</h4>
            <form class="footer-newslettr-inner" action="/join-newsletter" method="POST" onsubmit="event.preventDefault(); return Newsletter(this)">
                <input class="input-md fancy" name="email" title="Enter Email Address.." placeholder="Enter Email Address.." type="text">
                <button type="submit" class="btn btn-md btn-color fancy btn-subscribe">Sign Up</button>
            </form>
        </div>
    </div>
    <!-- End Footer Newsletter -->

    <!-- Footer Copyright -->
    <div class="container">
        <div class="copyrights">
            <p class="copyright">{{__('app.copyright')}} &copy;<script>document.write(new Date().getFullYear());</script> {{__('app.rights')}}
        </div>
    </div>
    <!-- End Footer Copyright -->
</footer>


@include('_session_.error2')
@include('_session_.success2')