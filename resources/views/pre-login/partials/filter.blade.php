<!-- Sidebar -->
<div class="sidebar-container col-md-3 pull-md-9">

    <!-- Filter By Price -->
    <div class="widget-sidebar widget-price-range">
        <h6 class="widget-title">{{__('app.filter_price')}}</h6>
        <div id="price-slider"></div>
        <input type="hidden" name="max-price" id="max-price">
        <input type="hidden" name="min-price" id="min-price">
    </div>

    <!-- Filter By Color -->
    <div class="widget-sidebar widget-filter-color">
        <h6 class="widget-title">{{__('app.filter_color')}}</h6>
        <ul class="widget-content" style="overflow-x: hidden; height: 250px;">

          @foreach($all_colors as $colors)
            <li>
                <div class="form-check">
                  <label class="form-check-label">
                    <div class="filter-color-switcher"><span style="background-color: {{$colors->hex}}"></span></div>
                    {{$colors->colour_name}}
                    <input class="checkbox" id="multiple_colors_filter<?= $colors->id;?>" type="checkbox" name="colors[]" value="{{$colors->id}}"><i class="input-helper"></i></label></div>
            </li>
            <input type="hidden" name="colors_id" value="{{$colors->id}}">
          @endforeach
        </ul>
    </div>

    <!-- Filter By Size -->
    <div class="widget-sidebar widget-filter-size">
        <h6 class="widget-title">{{__('app.filter_size')}}</h6>
        <ul class="widget-content">
            <li>
              <div class="form-check">
                <label class="form-check-label">
                  L
                  <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="L">
                  <i class="input-helper"></i>
                </label>
              </div>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                M
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="M">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                S
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="S">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                X
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="X">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                XL
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XL">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                XS
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XS">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                XXL
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="XXL">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                Free Size
                <input class="checkbox" id="multiple_size_filter" type="checkbox" name="size[]" value="free size">
                <i class="input-helper"></i>
              </label>
            </li>
        </ul>
    </div>

     <!-- Filter By design -->
    <div class="widget-sidebar widget-filter-tag">
        <h6 class="widget-title">{{__('app.filter_design')}}</h6>
        <ul class="widget-content">
            <li>
              <div class="form-check">
                <label class="form-check-label">
                Hand Made
                <input class="checkbox" id="multiple_design_filter" type="checkbox" name="design[]" value="handmade">
                <i class="input-helper"></i>
              </label>
            </li>
            <li>
              <div class="form-check">
                <label class="form-check-label">
                Machine Work
                <input class="checkbox" id="multiple_design_filter" type="checkbox" name="design[]" value="machine work">
                <i class="input-helper"></i>
              </label>
            </li>
        </ul>
    </div>

    <!-- Filter By Tag -->
    <div class="widget-sidebar widget-filter-tag">
        <h6 class="widget-title">{{__('app.filter_vendor')}}</h6>
        <ul class="widget-content" style="overflow-x: hidden; height: 250px;">
          @foreach($vendors as $vendor)
            <li>
              <div class="form-check">
                <label class="form-check-label">
                {{$vendor->shop_name}}
                <input class="checkbox" id="multiple_vendor_filter" type="checkbox" name="vendor_filter[]" value="{{$vendor->id}}">
                <i class="input-helper"></i>
              </label>
            </li>
          @endforeach
        </ul>
    </div>

    

</div>