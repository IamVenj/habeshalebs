<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:400,400i,700,700i" rel="stylesheet" />
    <!--<![endif]-->
	<title>Email Template</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#5D4D44; -webkit-text-size-adjust:none }
		a { color:#000001; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }
		.text-footer2 a { color: #ffffff; } 
		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			
			.m-center { text-align: center !important; }
			.m-left { text-align: left !important; margin-right: auto !important; }
			
			.center { margin: 0 auto !important; }
			.content2 { padding: 8px 15px 12px !important; }
			.t-left { float: left !important; margin-right: 30px !important; }
			.t-left-2  { float: left !important; }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.content { padding: 30px 15px !important; }
			.section { padding: 30px 15px 0px !important; }

			.m-br-15 { height: 15px !important; }
			.mpb5 { padding-bottom: 5px !important; }
			.mpb15 { padding-bottom: 15px !important; }
			.mpb20 { padding-bottom: 20px !important; }
			.mpb30 { padding-bottom: 30px !important; }
			.m-padder { padding: 0px 15px !important; }
			.m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
			.p70 { padding: 30px 0px !important; }
			.pt70 { padding-top: 30px !important; }
			.p0-15 { padding: 0px 15px !important; }
			.p30-15 { padding: 30px 15px !important; }			
			.p30-15-0 { padding: 30px 15px 0px 15px !important; }			
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }			


			.text-footer { text-align: center !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-dir,
			.column-top,
			.column-empty,
			.column-top-30,
			.column-top-60,
			.column-empty2,
			.column-bottom { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 15px !important; }
			.column-empty2 { padding-bottom: 30px !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#5D4D44; -webkit-text-size-adjust:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#5D4D44">
		<tr>
			<td align="center" valign="top">
				<!-- Main -->
				<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								
								<tr>
									<td bgcolor="#ffffff" class="p30-15 img-center" style="padding: 30px; border-radius: 20px 20px 0px 0px; font-size:0pt; line-height:0pt; text-align:center;"><a href="https://habeshalebs.com" target="_blank"><img src="https://habeshalebs.com/public/storage/uploads/logo.png" width="146" border="0" alt="" /></a></td>
								</tr>

								<tr>
									<td bgcolor="#FAAF40" class="p30-15 img-center" style="padding: 30px; font-size:20pt; line-height:0pt; text-align:center; box-shadow: 0px 10px 10px rgba(0,0,0,1);z-index: 100000000 !important;">
										<p style="color: #fff; font-family: 'Montserrat', sans-serif;">{{$buyer['name']}}</p>
									</td>
								</tr>
								
							</table>
							<!-- END Header -->
								
							<!-- Section 1 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
								<tr>
									<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://habeshalebs.com/public/storage/uploads/adv.jpg" width="650" height="400" border="0" alt="" style="object-fit: cover;" /></td>
								</tr>
								<tr>
									<td class="p30-15-0" style="padding: 50px 30px 0px;" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="h2-center"style="color:#000000; font-family:'Montserrat', Times, 'Montserrat', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;">Your Order</td>
											</tr>
											<tr>
												<td class="text-center"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:center; padding-bottom:22px;">{{$buyer['date']}}</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 1 -->

							<!-- Section 2 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FAAF40">
											<tr>
												<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://habeshalebs.com/public/storage/uploads/support/free_blue_white.jpg" width="650" height="162" border="0" alt="" /></td>
											</tr>
											<tr>
												<td class="p0-15" style="padding: 0px 30px;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														@foreach($buyer['contents'] as $item)
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>{{$item->model->product_name.' x'.$item->qty}}</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$item->model->current_price * $item->qty.' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														@endforeach

														
														
														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>Subtotal</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$buyer['subtotal'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>

														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; color:#fff;"><multiline>Shipping</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left;"><multiline>{{$buyer['shipping'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>

														<tr>
															<td class="pb40"style="padding-bottom:20px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="event-separator"style="padding-bottom:20px; border-bottom:1px solid #ffffff;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#fff;"><multiline>Total</multiline></td>
																							</tr>
																						</table>
																					</th>
																					<th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																					<th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td align="right">
																									<table class="m-left" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="day"style="color:#fff; font-family:'Raleway', Arial,sans-serif; font-size:20px; line-height:14px; text-align:left; font-weight:bold;"><multiline>{{$buyer['amount'].' '.$buyer['currency']}}</multiline></td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</th>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="fluid-img"style="font-size:0pt; line-height:0pt; text-align:left;"><img src="https://habeshalebs.com/public/storage/uploads/support/free_white_blue.jpg" width="650" height="160" border="0" alt="" /></td>
								</tr>
							</table>
							<!-- END Section 2 -->
							
							<!-- Section 3 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
								<tr>
									<td class="p0-15-30" style="padding: 0px 30px 30px 30px;" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="h2-center pb40"style="color:#000000; font-family:'Montserrat', Times, 'Montserrat', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;"><multiline>Your Details</multiline></td>
											</tr>
											<tr>
												<td class="pb30" style="padding-bottom:30px;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="separator2"style="padding-bottom:40px; border-bottom:1px solid #ebebeb;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<th class="column-top" width="280"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td valign="top">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="h5-black black"style="font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:18px; text-align:left; padding-bottom:15px; text-transform:uppercase; font-weight:bold; color:#000000;"><multiline>Shipping</multiline></td>
																							</tr>
																							
																						</table>
																					</td>
																				</tr>
																			</table>
																		</th>
																		<th class="column-empty" width="30"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
																		<th class="column-top" width="280"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td valign="top">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							
																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>Country: {{$buyer['shippingTo']['country']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>Address: {{$buyer['shippingTo']['address']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>State: {{$buyer['shippingTo']['state']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>City: {{$buyer['shippingTo']['city']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>Apartment: {{$buyer['shippingTo']['apartment']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>PostCode/ZIP: {{$buyer['shippingTo']['postal_code']}}</multiline></td>
																							</tr>

																							<tr>
																								<td class="text"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:left;"><multiline>Map Address: {{$buyer['shippingTo']['map_address']}}</multiline></td>
																							</tr>
																						</table>
																					</td>
																					
																				</tr>
																			</table>
																		</th>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 3 -->

							<!-- Section 5 -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
								<tr>
									<td class="p30-15-0" style="padding: 70px 30px 0px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="h2-center"style="color:#000000; font-family:'Montserrat', Times, 'Montserrat', serif; font-size:32px; line-height:36px; text-align:center; padding-bottom:20px;"><multiline>Your item will arrive in {{$settings->delivery_time}}</multiline></td>
											</tr>
											
											<tr>
												<td class="text-center pb20"style="color:#5d5c5c; font-family:'Raleway', Arial,sans-serif; font-size:14px; line-height:22px; text-align:center; padding-bottom:20px;"><multiline>{{$settings->delivery_email}}</multiline></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							<!-- END Section 5 -->

							<!-- Footer -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-15-0" bgcolor="#ffffff" style="border-radius: 0px 0px 20px 20px; padding: 70px 30px 0px 30px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
										 	<tr>
												<td class="m-padder2 pb30" align="center"style="padding-bottom:30px;">
													
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>
				<!-- END Main -->

			</td>
		</tr>
	</table>
</body>
</html>
