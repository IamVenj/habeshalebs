<div class="modal customWidth fade" role="dialog" id="edit-review<?= $current_user_review->id; ?>">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            	<h4 class="modal-title" style="color:#fff;">{{__('app.Edit')}} {{__('app.Review')}}</h4>
            
            </div>

        	<form class="review-form" action="/update-review/{{$current_user_review->id}}" method="post">
            	
            	<div class="modal-body">

					@csrf

					@method('PATCH')

					<div class="form-group">

						<textarea class="input" placeholder="Your review" id="review" name="review" style="width: 100%;">{{$current_user_review->review}}</textarea>

					</div>

					<div class="form-group">

						<div class="input-rating">

							<strong class="text-uppercase">{{__('app.your_rating')}}: </strong>

							<div class="stars">

								<input type="radio" id="star5" name="rating" value="5" <?php if($current_user_review->rating == 5): ?> checked <?php endif; ?>/><label for="star5"></label>

								<input type="radio" id="star4" name="rating" value="4" <?php if($current_user_review->rating == 4): ?> checked <?php endif; ?>/><label for="star4"></label>

								<input type="radio" id="star3" name="rating" value="3" <?php if($current_user_review->rating == 3): ?> checked <?php endif; ?>/><label for="star3"></label>

								<input type="radio" id="star2" name="rating" value="2" <?php if($current_user_review->rating == 2): ?> checked <?php endif; ?>/><label for="star2"></label>

								<input type="radio" id="star1" name="rating" value="1" <?php if($current_user_review->rating == 1): ?> checked <?php endif; ?>/><label for="star1"></label>

							</div>

						</div>

					</div>


	        	</div>

	        	<div class="modal-footer">
	        		
					<button class="btn btn-sm btn-black" type="submit">{{__('app.Update')}}</button>

	        	</div>
			
			</form>

	    </div>

	</div>

</div>


<div class="modal customWidth fade" role="dialog" id="delete-review<?= $current_user_review->id; ?>">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            	<h4 class="modal-title" style="color:#fff;">{{__('app.Delete')}} {{__('app.Review')}}</h4>
            
            </div>

            <div class="modal-footer">

            	<form action="/delete-review/{{$current_user_review->id}}" method="post">
            		
	            	@csrf

	            	@method('DELETE')
	         
	            	<button class="btn btn-sm" type="submit" style="background-color: #F8694A; color: #fff;">{{__('app.Delete')}}</button>

            	</form>


	        </div>

	    </div>

	</div>

</div>
