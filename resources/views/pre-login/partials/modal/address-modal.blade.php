
<div class="modal modal-edu-general fade" role="dialog" id="add-address2">

    <div class="modal-dialog ">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="mdi mdi-plus"></i>{{__('app.Add_Address')}} </h4>

                <hr>

            </div>

            <form method="POST" action="/add-address" method="post">

                @csrf

                 <div id="mod">
                    
                    <input id="__address" class="input-md form-full-width" type="text" name="address" size="50" placeholder="{{__('app.search')}}">
                
                </div>


                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12">
                        
                            <div id="_map" class="show-map"></div>

                            <input type="hidden" name="lat" id="lat">

                            <input type="hidden" name="lng" id="lng">
                      
                        </div>
                    
                    </div>

                </div>
                
                <div class="modal-footer">

                    <button class="btn btn-success" type="submit" style="padding: 10px; padding-left: 15px; padding-right: 15px;">{{__('app.Add')}}</button>
                    
                    <button class="btn btn-danger" type="button" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">{{__('app.Close')}}</button>

                </div>

            </form>

        </div>

    </div>

</div>
