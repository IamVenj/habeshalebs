<div class="modal customWidth fade" role="dialog" id="delivery-status<?= $order->id;?>">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            	<h4 class="modal-title" style="color:#fff;">{{__('app.ItemDeliveredState')}}</h4>
            
            </div>

            <form action="/confirm-customer-delivery/{{$order->id}}" method="post">
                    
                @csrf

                @method('PATCH')

                <div class="modal-body">
                    
                    <label for="delivery_status">{{__('app.changeDeliveryState')}}</label>
             
                    <select id="delivery_status" name="delivery_status" class="form-control form-control-sm">
                        
                        <option value="1" <?php if($order->customer_delivery_confirmation == 1): ?> selected <?php endif; ?>>{{__('app.yes')}}</option>

                        <option value="0" <?php if($order->customer_delivery_confirmation == 0): ?> selected <?php endif; ?>>{{__('app.no')}}</option>

                    </select>

                </div>

                <div class="modal-footer">                       

                    <button class="btn btn-danger" type="submit">{{__('app.Submit')}}</button>

    	        </div>

            </form>

	    </div>

	</div>

</div>


<div class="modal customWidth fade" role="dialog" id="cancel-order<?= $order->id;?>">

    <div class="modal-dialog modal-sm">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3" style="background: #F8694A; color: #fff;">

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

                <h4 class="modal-title" style="color:#fff;">Are you sure?</h4>
            
            </div>

            <form action="/cancel-my-order/{{$order->id}}" method="post">
                    
                @csrf

                @method('DELETE')

                <div class="modal-footer">                       

                    <button class="btn btn-sm btn-black" type="submit">Cancel Order</button>

                </div>

            </form>

        </div>

    </div>

</div>