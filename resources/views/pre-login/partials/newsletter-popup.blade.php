<section id="nlpopup" data-expires="20" data-delay="10">

    <a href="javascript:void(0)" class="nlpopup_close nlpopup_close_icon">
        <img src="img/close-icon-white.png" alt="Newsletter Close" /></a>

    <h3 class="mb-40">Join Our Mailing List </h3>
    <p class="black mb-20">
        But I must explain to you how all this mistaken<br />
        idea of denouncing pleasure pain.
    </p>
    <form action="/join-newsletter" method="POST" onsubmit="event.preventDefault(); return Newsletter(this)">
        <input class="input-md" name="email" title="Enter Email Address.." placeholder="example@domain.com" type="email">
        <button type="submit" class="btn btn-md btn-color btn-subscribe">Subscribe</button>
    </form>
    <label class="mt-20">
        Sign up For Exclusive Updates, New Arrivals<br />
        And Insider-Only Discount.</label>
    <a class="nlpopup_close btn-sm btn-black mt-40">&#10006; Close</a>
</section>

<div id="nlpopup_overlay"></div>
