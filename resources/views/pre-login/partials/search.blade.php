<section class="search-overlay-menu">
    <a href="javascript:void(0)" class="search-overlay-close"></a>
    <div class="container">
    
        <form action="/search" method="get">
            
            <div class="search-icon-lg">
                <img src="{{URL::asset('img/search-icon-lg.png')}}" alt="" />
            </div>
            <select class="input search-categories nice-select-box" name="category" id="search-category">
                <option value="0">{{__('app.all_categories')}}</option>
                <?php
                $categories = App\Category::latest()->get();
                ?>
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->category_name}}</option>
                @endforeach
            </select>   
            <input name="query" autocomplete="off" id="search-products" type="search" placeholder="{{__('app.search')}}">
            <button class="search-btn" type="submit">
                <img src="{{URL::asset('img/search-lg-go-icon.png')}}" alt="" />
            </button>
        </form>

    </div>
</section>