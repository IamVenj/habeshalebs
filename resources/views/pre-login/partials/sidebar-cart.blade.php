
<section id="sidebar-right" class="sidebar-menu sidebar-right">
    <div class="cart-sidebar-wrap">

        <div class="cart-widget-heading">
            <h4>{{__('app.ShoppingCart')}}</h4>
            <a href="javascript:void(0)" id="sidebar_close_icon" class="close-icon-white"></a>
        </div>


        <div class="cart-widget-content">
            <div class="cart-widget-product ">

                @if(Cart::instance('default')->count() == 0)

                <div class="cart-empty">
                    <label>{{__('app.empty_added_cart')}}</label>
                </div>
                
                @endif
                
                <ul class="cart-product-item">
                    <div id="cartItem"></div>
                </ul>


            </div>
        </div>

        <div class="cart-widget-footer">
            <div class="cart-footer-inner">

                @if(Cart::instance('default')->count() > 0)
                <label class="cart-total-hedding normal" style="font-family: 'Pacifico', cursive;"><span>{{__('app.total')}} :</span><span class="cart-total-price"></span></label>
                @else
                <label class="cart-total-hedding normal" style="font-family: 'Pacifico', cursive; font-size: 16px;"><span>{{__('app.total')}} :</span><span class="cart-total-price">$0</span></label>
                @endif

                <div class="cart-action-buttons">
                    <a href="/cart" class="view-cart btn btn-md btn-gray">{{__('app.view_cart')}}</a>
                    <a href="/checkout" class="checkout btn btn-md btn-color">{{__('app.checkout')}}</a>
                </div>

            </div>
        </div>
    </div>
</section>
<div class="sidebar_overlay"></div>