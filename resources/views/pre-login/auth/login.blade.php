@extends('pre-login.index.index')

@section('content')

<section class="breadcrumb">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb-link">
                    <a href="#">{{__('app.home')}}</a>
                    <span>{{__('app.login')}}</span>
                </nav>
            </div>
        </div>
    </div>

</section>

<section class="content-page">

    <div class="container mb-80">

        <div class="row">

            <div class="col-md-12">

                <article class="post-8">

                    <p class="checkout-info">

                        {{__('app.Not_member')}} <strong><a href="/register">{{__('app.click_here')}}</a></strong>

                    </p>

                    <form class="product-checkout mt-45" style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">

                        <div class="row">

                            <div class="col-md-12">

                                <h3>{{__('app.login')}}</h3>

								<input type="hidden" name="csrf-token" value="{{csrf_token()}}"></div>

								<div class="form-field-wrapper form-center col-sm-12">

                                    <label for="email" class="left">

                                        {{__('app.email')}}

                                        <abbr class="form-required" title="required">*</abbr></label>
                                    <input class="input-md form-full-width" name="login-email" title="{{__('app.email')}}" value="" placeholder="{{__('app.email')}}" type="email" required="" aria-required="true">

                                </div>

                                <div class="form-field-wrapper form-center col-sm-12">

                                    <label for="password" class="left">

                                        {{__('app.password')}}

                                        <abbr class="form-required" title="required">*</abbr></label>

                                    <input class="input-md form-full-width" name="password" title="{{__('app.password')}}" value="" placeholder="{{__('app.password')}}" type="password" required="" aria-required="true">

                                </div>

                                <div class="form-field-wrapper col-sm-6">

                                	<button type="submit" class="btn btn-sm btn-black btn-login">
                                	{{__('app.login')}}</button>

                            	</div>

                            	<div class="form-field-wrapper col-sm-6 text-right"><a href="/forgot-password">Forgot your password?</a></div>

                            </div>

                        </div>

                    </form>

                </article>

            </div>

        </div>

    </div>

</section>

<div class="message"></div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">

	var token = $("input[name=csrf-token]");
	
	$('.btn-login').click(function(e){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': token.val()
			}
		});


		e.preventDefault();

		var email = $("input[name='login-email']").val();
		var password = $("input[name=password]").val();

		$.ajax({

			type: 'POST',
			url: window.location.href,
			data: {email: email, password: password},

			beforeSend: function(){
	            $(".btn-login").attr('disabled', "disabled");
          	},

			success: function(response){

				$(".btn-login").removeAttr('disabled', "disabled");

				if(response.status == 'login_failed')
				{

					var message = $('.message').fadeIn('fast');
					var errorMessage = '<div class="form-group mt-4">';
		    			errorMessage += '<div class="alert golden">';
		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ response.message + '</p>';
		      			errorMessage +=  '</div></div>';
		      			$(message).html(errorMessage).delay(2500).fadeOut('slow');
				}

				else

				{

					if(response.role_type == "admin") {
						location.replace('/dashboard');
					} else if(response.role_type == "seller" && response.activation == 1) {
						location.replace('/unactivated');
					} else if(response.role_type == "seller" && response.activation == 0) {
						location.replace('/dashboard');
					} else if(response.role_type == "customer") {
						location.replace('/');
					}

				}

			},
			error: function(error){

				console.log(error.responseJson);

			}

		});

	});

</script>

@endsection