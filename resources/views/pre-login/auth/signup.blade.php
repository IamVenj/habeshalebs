@extends('pre-login.index.index')

@section('content')


<section class="breadcrumb">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <nav class="breadcrumb-link">

                    <a href="#">{{__('app.home')}}</a>

                    <span>{{__('app.Register')}}</span>

                </nav>

            </div>

        </div>

    </div>

</section>

<section class="content-page">

    <div class="container">
        <ul class="product-filter nav" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#latest" role="tab" data-toggle="tab" style="font-size: 28px; font-weight: bold;">{{__('app.customer')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#best-sellar" role="tab" data-toggle="tab" style="font-size: 28px; font-weight: bold;">{{__('app.Vendor')}}</a>
            </li>
        </ul>
        
        <div class="tab-content">
            <!-- Tab1 - Latest Product -->
            <div id="latest" role="tabpanel" class="tab-pane fade in active">
            	<form class="product-checkout mt-45" style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">
                    <div class="row">
                        <div class="col-md-12">
			                <article class="post-8">
			                    <p class="checkout-info">
			                        <p>{{__('app.already_user')}} <a href="/login">{{__('app.login')}}</a></p>
			                    </p>
			                </article>
                        	<h3>{{__('app.Register')}}</h3>
			                <input class="input-md form-full-width mb-10" name="firstname" title="{{__('app.firstname')}} " value="" placeholder="{{__('app.firstname')}} " type="text">
			                <input class="input-md form-full-width mb-10" name="lastname" title="{{__('app.lastname')}} " value="" placeholder="{{__('app.lastname')}} " type="text">
			                <input class="input-md form-full-width mb-10" name="customer-email" title="{{__('app.email')}}" value="" placeholder="{{__('app.email')}}" type="email" required="">
			                <input class="input-md form-full-width mb-10" name="customer-password" title="{{__('app.password')}}" value="" placeholder="{{__('app.password')}}" type="password" required="">
			                <input class="input-md form-full-width mb-30" type="password" title="{{__('app.c_password')}}" name="customer-password_confirmation" placeholder="{{__('app.c_password')}}" required="">
                        	<button type="submit" class="btn btn-sm btn-black btn-register-customer">{{__('app.Register')}}</button>

			            </div>
			        </div>
			    </form>
            </div>

            <!-- Tab2 - Best Sellar -->
            <div id="best-sellar" role="tabpanel" class="tab-pane fade">
            	<form class="product-checkout mt-45" style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">
                    <div class="row">
                        <div class="col-md-12">
                        	<article class="post-8">
			                    <p class="checkout-info">
			                        <p>{{__('app.already_user')}} <a href="/login">{{__('app.login')}}</a></p>
			                    </p>
			                </article>
                            <h3>{{__('app.Register')}}</h3>
			                <input class="input-md form-full-width mb-10" name="shopname" title="{{__('app.shop_name')}}" value="" placeholder="{{__('app.shop_name')}}" type="text">
			                <input class="input-md form-full-width mb-10" name="seller-email" title="{{__('app.email')}}" value="" placeholder="{{__('app.email')}}" type="email" required="">
			                <input class="input-md form-full-width mb-10" name="password" title="{{__('app.password')}}" value="" placeholder="{{__('app.password')}}" type="password" required="">
			                <input class="input-md form-full-width mb-30" type="password" title="{{__('app.c_password')}}" name="password_confirmation" placeholder="{{__('app.c_password')}}" required="">	
                        	<button type="submit" class="btn btn-sm btn-black btn-register-seller">{{__('app.Register')}}</button>

			            </div>
			        </div>
			    </form>
            </div>
        </div>
    </div>
</section>

<div class="message"></div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>

<script type="text/javascript">
	
	var token = $("meta[name=csrf-token]").attr("content");

	$('.btn-register-customer').click(function(e){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': token
			}
		});

		e.preventDefault();

		var email = $("input[name=customer-email]").val();
		var firstname = $("input[name=firstname]").val();
		var lastname = $("input[name=lastname]").val();
		var password = $("input[name=customer-password]").val();
		var password_confirmation = $("input[name=customer-password_confirmation]").val();

		$.ajax({

			type: 'POST',
			url: '/customer/signup',
			data: {firstname: firstname, lastname:lastname, email: email, password: password, password_confirmation:password_confirmation},
			success: function(data){

				var message = $('.message').fadeIn('fast');

	      		if(data.status == "success")
	      		{

	      			$("input[name=firstname]").val("");
					$("input[name=lastname]").val("");
					$("input[name=customer-email]").val("");
		      		$("input[name=customer-password]").val("");
		      		$("input[name=customer-password_confirmation]").val("");

					var successMessage = '<div class="form-group mt-4">';
		    			successMessage += '<div class="alert golden2">';
		    			successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ data.message + '</p>';
		      			successMessage +=  '</div></div>';
	      				$(message).html(successMessage).delay(2500).fadeOut('slow');      			
	      		}


	      		if(data.status == "error")
	      		{
	      			var errorMessage = '<div class="form-group mt-4">';
		    			errorMessage += '<div class="alert golden">';
		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ data.message + '</p>';
		      			errorMessage +=  '</div></div>';
      					$(message).html(errorMessage).delay(2500).fadeOut('slow');
	      		}

	      		if(data.role_type == "admin" || data.role_type == "seller") {
					location.replace('/dashboard');
				} else if(data.role_type == "customer") {
					location.replace('/');
				}

			},

			error: function(error){
				console.log(error);
				var message = $('.message').fadeIn('fast');
				if(error.responseJSON['errors'] != null)
				{
					var errorMessage = '<div class="form-group mt-4">';
		    			errorMessage += '<div class="alert golden">';
		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			$.each(error.responseJSON['errors'], function(key, errors)
						{
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ errors + '</p>';
		    			});
		      			errorMessage +=  '</div></div>';
      					$(message).html(errorMessage).delay(2500).fadeOut('slow');
		      			
				}

				else
				{
					$(message).html('').fadeIn('slow');
				}
			}
		});
	});


	$('.btn-register-seller').click(function(e){

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': token
			}
		});

		e.preventDefault();

		var email = $("input[name=seller-email]").val();
		var shop_name = $("input[name=shopname]").val();
		var password = $("input[name=password]").val();
		var password_confirmation = $("input[name=password_confirmation]").val();

		$.ajax({

			type: 'POST',
			url: '/seller/signup',
			data: {shop_name:shop_name, email: email, password: password, password_confirmation:password_confirmation},
			success: function(data){
				var message = $('.message').fadeIn('fast');
	      		if(data.status == "success")
	      		{
					$("input[name=shopname]").val("");
					$("input[name=seller-email]").val("");
		      		$("input[name=password]").val("");
		      		$("input[name=password_confirmation]").val("");

					var successMessage = '<div class="form-group mt-4">';
		    			successMessage += '<div class="alert golden2">';
		    			successMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			successMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ data.message + '</p>';
		      			successMessage +=  '</div></div>';
	      				$(message).html(successMessage).delay(2500).fadeOut('slow');      			
	      		}


	      		if(data.status == "error")
	      		{
	      			var errorMessage = '<div class="form-group mt-4">';
		    			errorMessage += '<div class="alert golden">';
		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ data.message + '</p>';
		      			errorMessage +=  '</div></div>';
      					$(message).html(errorMessage).delay(2500).fadeOut('slow');
	      		}

	      		if(data.role_type == "admin") {
					location.replace('/dashboard');
				} else if(data.role_type == "seller" && data.activation == 1) {
					location.replace('/unactivated');
				} else if(data.role_type == "seller" && data.activation == 0) {
					location.replace('/dashboard');
				} else if(data.role_type == "customer") {
					location.replace('/');
				}

			},

			error: function(error){
				console.log(error);
				var message = $('.message').fadeIn('fast');
				if(error.responseJSON['errors'] != null)
				{
					var errorMessage = '<div class="form-group mt-4">';
		    			errorMessage += '<div class="alert golden">';
		    			errorMessage += '<small  class="close ml-3" data-dismiss="alert" area-hidden="true" style="cursor: pointer;"><strong><i class="fa fa-times fa"></i></strong></small>';
		    			$.each(error.responseJSON['errors'], function(key, errors)
						{
		    			errorMessage += '<p class="mr-5 ml-5" style="font-family: Nunito; color: #fff; font-size: 16px; cursor: default;">'
		    			+ errors + '</p>';
		    			});
		      			errorMessage +=  '</div></div>';
      					$(message).html(errorMessage).delay(2500).fadeOut('slow');
				}

				else
				{
					$(message).html('').fadeIn('slow');
				}
			}
		});
	});

</script>

@endsection