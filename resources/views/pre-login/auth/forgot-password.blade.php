@extends('pre-login.index.index')

@section('content')

<section class="breadcrumb">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <nav class="breadcrumb-link">

                    <a href="#">{{__('app.home')}}</a>

                    <span>{{__('app.forgot_pass')}}</span>

                </nav>

            </div>

        </div>

    </div>

</section>

<section class="content-page">

    <div class="container mb-80">

        <div class="row">

            <div class="col-md-12">

                <article class="post-8">

                    <form action="/forgot-password" method="POST"  class="product-checkout mt-45" style="background: #ffffff40; padding-top: 50px; padding-bottom: 50px; padding-left: 40px; padding-right: 40px; box-shadow: 0px 10px 20px #00000010;">

                        <div class="row">

                            <div class="col-md-12">

                                <h3>{{__('app.forgot_pass')}}</h3>

								<div class="form-field-wrapper form-center col-sm-12">

                                    <label for="email" class="left">

                                        {{__('app.email')}}

                                        <abbr class="form-required" title="required">*</abbr></label>
                                    <input class="input-md form-full-width" name="forgot-email" title="{{__('app.email')}}" value="" placeholder="{{__('app.email')}}" type="email" required="" aria-required="true">

                                </div>

                                
                                <div class="form-field-wrapper col-sm-6">

                                	<button type="submit" class="btn btn-sm btn-black btn-login">
                                	{{__('app.Submit')}}</button>

                            	</div>

                            </div>

                        </div>

                    </form>

                </article>

            </div>

        </div>

    </div>

</section>


@endsection