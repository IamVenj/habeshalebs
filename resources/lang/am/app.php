<?php

return [

	'amh' => 'አማርኛ',

	'eng' => 'እንግሊዘኛ',

	'short_amh' => 'አማ',

	'short_eng' => 'እንግ',

	'welcome' => 'እንኳን በደህና መጡ',

	'filter_price' => 'በዋጋ ያጣሩ',

	'filter_color' => 'በቀለም ያጣሩ',

	'filter_size' => 'በመጠን አጣራ',

	'filter_vendor' => 'በአቅራቢው ያጣሩ',

	'all_categories' => 'ሁሉም ምድቦች',

	'categories' => 'ምድቦች',

	'search' => 'ቃልዎን ያስገቡ',

	'my_account' => 'አካውንቴ',

	'login' => 'ይግቡ',

	'my_cart' => 'ዘምቢሌ',

	'view_cart' => 'ዘምቢልዎን ይመልከቱ',

	'join' => 'ይመዝገቡ',

	'my_wishlist' => 'የምኞት ዝርዝር',

	'my_address' => 'የእኔ አድራሻ',

	'my_orders' => 'የእኔ ትዕዛዞች',

	'logout' => 'ውጣ',

	'cust_service' => 'የደንበኞች ግልጋሎት',

	'about_us' => 'ስለ እኛ',

	'ship_return' => 'መላክ እና መመለስ',

	'ship_guide' => 'የመላኪያ መመሪያ',
 
	'faq' => 'ተደጋግሞ የሚነሱ ጥያቄዎች',

	'stay_connected' => 'እንደተገናኙ ይቆዩ',

	'enter_email' => 'ኢሜል አድራሻ ያስገቡ',

	'join_letter' => 'በራሪ ጽሑፍን ይቀላቀሉ',

	'special_offers' => 'ልዩ ቅናሾች',

	'rights' => 'መብቱ በህግ የተጠበቀ ነው',

	'copyright' => 'የቅጂ መብት',

	'developer_note' => 'የተቀየሰ እና የተነደፈ ከ',

	'by' => 'በ',

	'developer_name' => 'ጄ-ዲቭ',

	'best_seller' => 'ምርጥ የሽያጭ ምርቶች',

	'quick_view' => 'በፍጥነት ይመልከቱ',

	'popular_categories' => 'ታዋቂ ምድቦች',

	'latest_products' => 'የቅርብ ጊዜ ምርቶች',

	'home' => 'መነሻ ገጽ',

	'cart' => 'ዘምቢል',

	'empty_cart' => 'ዘምቢልዎ ባዶ ነው',

	'empty_added_cart' => 'ወደ ዘምቢልዎ ምንም ነገር አላከሉም',

	'order_review' => 'የትዕዛዝ ክለሳ',

	'product' => 'ምርት',

	'price' => 'ዋጋ',

	'quantity' => 'ብዛት',

	'include_delivery' => 'ማቅረቢያ ጨምር',

	'total' => 'ጠቅላላ',

	'birr' => 'ብር',

	'SUBTOTAL' => 'ንዑስ ድምር',

	'DELIVERY' => 'ማድረስ',

	'no_delivery' => 'ማቅረቢያ የለም',

	'DISCOUNT' => 'ቅናሽ',

	'payment_methods' => 'የክፍያ መንገዶች',

	'Yenepay' => 'የኔፔይ',

	'cash_delivery' => 'ሲቀበሉ መክፈል',

	'place_order' => 'ይዘዙ',

	'left' => 'ቀረ',

	'Availability' => 'ተገኝነት',

	'Brand' => 'መለያ',

	'Size' => 'መጠን', 

	'Color' => 'ቀለም',

	'add_cart' => 'ወደ ግዢው ቅርጫት ጨምር',

	'Description' => 'መግለጫ',

	'Reviews' => 'ግምገማዎች',

	'Review' => 'ግምገማ',

	'Location' => 'አካባቢ',

	'Edit' => 'ያርትዑ',

	'your_rating' => 'የእርስዎ ደረጃ',

	'your_review' => 'የእርስዎ ግምገማ',

	'Update' => 'አዘምን',

	'Delete' => 'ሰርዝ',

	'write_review' => 'ግምገማዎን ይጻፉ',

	'logging_required' => 'በመለያ መግባት ያስፈልጋል',

	'emailWillNotBePublished' => 'የእርስዎ ኢሜይል አድራሻ አይታተምም',

	'Submit' => 'ያስገቡ',

	'Related' => 'ተዛማጅ',

	'sold_by' => 'የተሸጠው በ',

	'ItemDeliveredState' => 'እቃው ደርሷል?',

	'changeDeliveryState' => 'የመላኪያ ሁኔታን ይቀይሩ',

	'yes' => 'አዎ',

	'no' => 'አይ',

	'Products' => 'ምርቶች',

	'sort_by' => 'ቅደምተከተሉ የተስተካከለው',

	'Latest' => 'የቅርብ ጊዜ',

	'Price_High_Low' => 'ዋጋ - ከከፍተኛ እስከ ዝቅተኛ',

	'Price_Low_High' => 'ዋጋ - ከዝቅተኛ እስከ ከፍተኛ',

	'Show' => 'አሳይ',

	'search_results' => 'የፍለጋ ውጤቶች',

	'have_account' => 'መለያ አለዎት?',

	'Register' => 'ይመዝገቡ',

	'forgot_pass' => 'መክፈቻ ቁልፉን ረሱ',

	'reset' => 'የይለፍ ቃልዎን ዳግም ያስጀምሩ',

	'already_user' => 'ቀድሞውኑ ተጠቃሚ ነዎት?',

	'seller_acct' => 'የሻጭ መለያ',

	'password' => 'የይለፍ ቃል',

	'email' => 'ኢሜይል',

	'c_password' => 'የይለፍ ቃል አረጋግጥ',

	'fill_profile' => 'መሰረታዊ መገለጫውን ይሙሉ',

	'profile' => 'መገለጫ',

	'address' => 'አድራሻ',

	'finish' => 'ጨርስ',

	'profile_image' => 'የመገለጫ ምስል',

	'choose_image' => 'ምስል ይምረጡ',

	'full_name' => 'ሙሉ ስም',

	'phone_number' => 'ስልክ ቁጥር',

	'revieve_news' => 'በራሪ ወረቀቶችን መቀበል እፈልጋለሁ',

	'terms_conditions' => 'በአገልግሎት ውሉ እስማማለሁ',

	'shop_name' => 'የሱቅ ስም',

	'about_shop' => 'ስለ ሱቅዎ',

	'add_more' => 'ተጨማሪ ያክሉ',

	'select_address' => 'የአድራሻ ግብዓቶችን ይምረጡ',

	'mer_code' => 'የነጋዴ ኮድ (የሻጭ ኮድ)',

	'pdt_token' => 'PDT (የክፍያ መረጃ ማስተላለፍ) ማስመሰያ',

	'optional' => 'ከተፈለገ',

	'customer_delivery' => 'ማንኛውንም ቁሳቁስ ለደንበኞች እናደርሳለን',

	'yene_setup' => 'የየኔፔይ ቅንጅቶች',

	'Dashboard' => 'ዳሽቦርድ',

    'customize_pages' => 'ገጾችን ያብጁ',

    'home_carousel' => 'የፉት ገጽ ተንቀሳቃሽ ምስል',

    'special_offers' => 'ልዩ ቅናሾች',

    'Category' => 'ምድብ',

    'Vendor_Reviews' => 'ግምገማዎች ለአቅራቢ',

    'Users' => 'ተጠቃሚዎች',

    'Debts' => 'እዳዎች',

    'Newsletter' => 'በራሪ ጽሑፍ',

    'Orders' => 'ትዕዛዞች',

    'My_Debt' => 'የእኔ ዕዳ',

    'Create_Product' => 'ምርት ይፍጠሩ',

    'View_Products' => 'ምርቶችን ይመልከቱ',

    'SIDEBAR_SKINS' => 'የጎን መልክ',

    'Light' => 'ብርሃን',

    'Dark' => 'ጨለማ',

    'HEADER_SKINS' => 'የራስ መልክ',

    'Settings' => 'ቅንጅቶች',

    'developer_note2' => 'በእጅ የተሰራ እና የተሰራዉ በ',

    'Create_Category' => 'ምድብ ይፍጠሩ',

    'category_desc' => 'ምድብ እና ንዑስ ምድብ ይፍጠሩ',

    'Category_Name' => 'የምድብ ስም',

    'Select_Category' => 'ምድብ ይምረጡ',

    'Category_Image' => 'የምድብ ምስል',

    'Create' => 'ይፍጠሩ',

    'Main_Categories' => 'ዋና ምድቦች', 

    'File_upload' => 'ፋይል ይስቀሉ',

    'Manage' => 'ያስተዳድሩ',

    'Update_Image' => 'ምስል አዘምን',

    'Welcome_back' => 'እንኳን በደህና መጡ',

    'analytics_statement' => 'ትንታኔዎችዎ ዳሽቦርድ',

    'Analytics' => 'ትንታኔዎች',

    'Cash_sales' => 'በጥሬ ገንዘብ ሽያጮች',

    'Sales_Comparison' => 'የሽያጭ ማወዳደር',

    'Sales_last_month' => 'ሽያጮች ባለፈው ወር',

    'Gross_sales_of' => 'የጠቅላላ ሽያጮች የ',

    'Previous' => 'ቀዳሚ',

    'Next' => 'ቀጣይ',

    'View_Your_Monthly_income' => 'ወርሃዊ ገቢዎን ይመልከቱ',

    'Monthly_income' => 'ወርሃዊ ገቢ',

    'Sales_income' => 'የሽያጭ ገቢ',

    'Purchases' => 'ግዢዎች',

    'Yearly_sales' => 'ዓመታዊ ሽያጮች',

    'View_Your_Yearly_sales' => 'ዓመታዊ ሽያጮችን ይመልከቱ',

    'View_your_Daily_deposits' => 'ዕለታዊ ተቀማጭዎን ይመልከቱ',

    'Daily_deposits' => 'በየቀኑ ተቀማጭ ገንዘብ',

    'Total_sales' => 'ጠቅላላ ሽያጮች',

    'Gross_sales_over_the_years' => 'ባለፉት ዓመታት አጠቃላይ ሽያጮች',

    'Vendors' => 'ሻጮች',

    'Vendor' => 'አቅራቢ',

    'This_is_the_list_of_total_customers_for' => 'ይህ የጠቅላላ ደንበኞች ዝርዝር ይህ ነው ለ',

    'customers' => 'ደንበኞች',

    'customer' => 'ደንበኛ',
    
    'Debt' => 'እዳ',

    'Order' => 'ትእዛዝ',

    'Item_SKU' => 'ንጥል SKU',

    'Debt_Status' => 'የዕዳ ሁኔታ',

    'Total_Debt' => 'ጠቅላላ ዕዳ',

    'Unpaid' => 'ያልተከፈለ',

    'Paid' => 'የተከፈለ',

    'check_debt_status' => 'ዕዳው ሙሉ በሙሉ ከተሸፈነ እባክዎን ከዚህ በታች ያለውን የቼክ ቁልፍን ጠቅ ያድርጉ',

    'Create_Newsletter' => 'በራሪ ጽሑፍ ይፍጠሩ',

    'Title' => 'ርዕስ',

    'Slug' => 'መግለጫ',

    'Send_Newsletter' => 'በራሪ ጽሑፍ ይላኩ',

    'order_date' => 'የታዘዘበት ቀን',

    'Delivery_status' => 'የማቅረብ ሁኔታ',

    'payment_method' => 'የክፍያ ዘዴ',

    'Payment_status' => 'የክፍያ ሁኔታ',

    'Sent_Date' => 'የተላከበት ቀን',

    'Pickup' => 'ማንሳት',

    'New' => 'አዲስ',

    'Delivered' => 'ደርሷል',

    'View_Orders' => 'ትዕዛዞችን ይመልከቱ',

    'Customer_Information' => 'የደንበኛ መረጃ',

    'Delivery_Information' => 'ማቅረቢያ መረጃ',

    'Order_Detail' => 'የትዕዛዝ ዝርዝር',

    'Product_Information' => 'የምርት መረጃ',

    'Payment_Information' => 'የክፍያ መረጃ',

    'customers_wants_pickup' => 'ደንበኛው እቃውን እወስዳለሁ ይላል',

    'customer_wants_delivery' => 'ደንበኛው ዕቃውን በአድራሻቸው እንዲደርስ ይፈልጋል',

    'NotDeliveredYet' => 'አልተለቀቀም ገና',

    'available' => 'ይገኛል',

    'Pending' => 'በመጠባበቅ ላይ',

    'Not_Paid' => 'አልተከፈለም',

    'Total_Price' => 'ጠቅላላ ዋጋ',

    'cash_delivery_pickup' => 'ዕቃውን በማቅረብ ላይ / ሲወስዱ በጥሬ ገንዘብ',

    'PaymentTransactionID' => 'የክፍያ ግብይት መታወቂያ',

    'Product_Name' => 'የምርት ስም',

    'unique_code' => 'የምርት ልዩ ኮድ (SKU)',

    'Product_Description' => 'የምርት ማብራሪያ',

    'Upload_Images' => 'ምስሎችን ጫን',

    'show_images' => 'የተመረጡ ምስሎችን አሳይ',

    'select_pColor' => 'የምርት ቀለም ይምረጡ',

    'Select_Colors' => 'ቀለሞችን ይምረጡ',

    'Product_Brand' => 'የምርት መለያ',

    'old_price' => "የምርቱ 'የድሮ / የመሠረት ዋጋ",

    'current_price' => "የምርቱ ወቅታዊ ዋጋ",

    'Price_in_birr' => 'ዋጋዉን በዶላር',

    'how_many_available' => 'ስንቶቹ ይገኛሉ',

    'Number_of_stock' => 'የአክሲዮን ብዛት',

    'ProductsLocation' => "የምርት ቦታ",

    'Select_Location' => 'አካባቢን ይምረጡ',

    'All' => 'ሁሉም',

    'Add_Products' => 'ምርቶችን ያክሉ',

    'Stores' => 'መደብሮች',

    'Store' => 'መደብር',

    'Manage_Images' => 'ምስሎችን ያቀናብሩ',

    'Product_Reviews' => 'የምርት ግምገማዎች',

    'Manage_Colors' => 'ቀለሞችን ያስተዳድሩ',

    'Replace_Color' => 'ቀለም ይተኩ',

    'Replace_Image' => 'ምስል ይተኩ',

    'Type_of_Users' => 'የተጠቃሚዎች ዓይነት',

    'Rating' => 'ደረጃ መስጠት',

    'View_Reviews' => 'ግምገማዎች ይመልከቱ',

    'Company_Name' => 'የድርጅት ስም',

    'company_slug' => 'ስለ ኩባንያው በጣም አጭር መግለጫ',

    'charge_per_percent' => 'ኩባንያው ምን ያህል መቶኛ ያስከፍላል',

    'discount_product' => 'ለቅናሽ ዋጋ የሚገዛው ምርት ስንት ነው',

    'amount_of_products' => 'ምርቶች ብዛት',

    'discount_for' => 'ቅናሽ ለ',

    'products_per_percent' => 'ምርቶች መቶኛ',

    'Discount_per_percent' => 'ቅናሽ በአንድ በመቶ',

    'facebook' => 'ፌስቡክ',

    'twitter' => 'ትዊተር',

    'Google_plus' => 'ጉግል +',

    'linked_in' => 'ሊንክድ ኢን',

    'Logo' => 'አርማ',

    'Upload_Logo' => 'አርማ ጫን',

    'Select_Logo' => 'አርማ ይምረጡ',

    'Current_Logo' => 'የአሁኑ አርማ',

    'Favicon' => 'ፋቪኮን',

    'Upload_Favicon' => 'ፋቪኮንን ይስቀሉ',

    'Select_Favicon' => 'ፋቪኮንን ይምረጡ',

    'Current_Favicon' => 'የአሁኑ ፋቪኮን',

    'select_vendor' => 'አቅራቢውን ይምረጡ',

    'change_pic' => 'የመገለጫ ስዕል ቀይር',

    'change_delivery_option' => 'የአቅርቦት አማራጭን ይቀይሩ',

    'Change_Password' => 'የሚስጥር ቁልፍ ይቀይሩ',

    'Name' => 'ስም',

    'UpdateProfilePicture' => 'የመገለጫ ስዕል አዘምን',

    'Save' => 'አስቀምጥ',

    'UpdateDeliveryOption' => 'የአቅርቦት አማራጭን አዘምን',

    'Close' => 'ይዝጉት',

    'Current_Password' => 'የአሁኑ ሚስጥራዊ ማለፊያ ቁልፍ',

    'NewPassword' => 'አዲስ የይለፍ ቃል',

    'Edit_Address' => 'አርትዕ-አድራሻ',

    'Add_Address' => 'አድራሻ ያክሉ',

    'Add' => 'ያክሉ',

    'Remove_Map' => 'ካርታ ያስወግዱ',

    'Remove' => 'ያስወግዱ',

    'Edit_Category' => 'ምድብ አርትዕ',

    'UpdateImageCategory' => 'የምስል ምድብ አዘምን',

    'Upload_Image' => 'ምስል ይስቀሉ',

    'Delete_Category' => 'ምድብ ሰርዝ',

    'DeleteOffer' => 'ቅናሽ ሰርዝ',

    'Add_colors' => 'ቀለሞችን ያክሉ',

    'OK' => 'እሺ',

    'Selected_Images' => 'የተመረጡ ምስሎች',

    'AddImage'=> 'ምስል ያክሉ',

    'Edit_Carousel' => 'ተንቀሳቃሽ ምስሉን ያርትዑ',

    'Delete_Carousel' => 'ተንቀሳቃሽ ምስሉን ይሰርዙ',

    'Send' => 'ይላኩ',

    'Edit_Newsletter' => 'በራሪ ጽሑፍን ያርትዑ',

    'Delete_Newsletter' => 'በራሪ ጽሑፍን ይሰርዙ',

    'Save_Changes' => 'ለውጦችን አስቀምጥ',

    'no_items' => 'ምንም ምርቶች የሉም',

    'firstname' => 'የመጀመሪያ ስም',

    'lastname' => 'የአያት ሥም',

    'delivery_price' => 'ጠፍጣፋ ፍጥነት መላኪያ ዋጋ',

    'weight_in_kg' => 'ክብደት በኪ.ግ.',

    'washcare' => 'መታጠብ ጥንቃቄ',

    'composition' => 'ጥንቅር',

    'lining_composition' => 'ሽፋን ጥንቅር',

    'washcare_example' => 'ደረቅ ጽዳት',

    'composition_example' => '100% ፖሊስተር',

    'Product_styling' => 'የምርት ንድፍ',

    'items_resulting_in_increase_of_flat_rate' => 'ጠፍጣፋ ተመን እንዲጨምር የሚያስችሉ የእቃዎች ብዛት',

    'order' => 'ትዕዛዝ',

    'item_name' => 'የንጥል ስም',

    'vendors_phone_number' => 'የሻጭ ስልክ ቁጥር',

    'shipping' => 'ማጓጓዣ',

    'sorry_items' => '',

    'sorry_offers' => 'አዝናለሁ! እዚህ ምንም ነገሮች የሉም',

    'shop_category' => 'በምድቦች ይሸምቱ',

    'sorry_category' => 'አዝናለሁ! እዚህ ምንም ምድቦች የሉም',

    'subscribe_newsletter' => 'ለጋዜጣችን ደንበኛ ይሁኑ',

    'exclusiveText' => 'ለአዳዲሰ ምርቶች እና ዜናዎች ይመዝገቡ',

    'SignUp' => 'ይመዝገቡ',

    'SeeMore' => 'ተጨማሪ ይመልከቱ',

    'Delivery' => 'ማድረስ',

    'Exchange_or_Return' => 'ልውውጥ ወይም ተመላሽ አድርግ',

    'Support' => 'ድጋፍ',

    'TopInteresting' => 'ከፍተኛ ፍላጎት',

    'NewProduct' => 'አዲስ ምርት',

    'welcome_to' => 'እንኳን በደህና መጡ',

    'About' => 'ስለ',

    'filter_design' => 'በዲዛይን አጣራ',

    'contact' => 'እውቂያ',

    'related' => 'ተዛማጅ ምርቶች',

    'add_review' => 'ግምገማ ያክሉ',

    'Item_Location' => 'የንጥል ቦታ',

    'design' => 'ዲዛይን',

    'Additional_Information' => 'ተጭማሪ መረጃ',

    'ShoppingCart' => 'ዘምቢል',

    'checkout' => 'ጨርሰህ ውጣ',

    'sorry' => 'አዝናለሁ',

    'country' => 'ሀገር',

    'State' => 'ግዛት',

    'Province' => 'ክፍለ ሀገር',

    'City' => 'ከተማ',

    'Postcode' => 'የፖስታ ኮድ',

    'ZIP' => 'ዚፕ',

    'Credit_or_debit_card' => 'የክሬዲት ወይም የዴቢት ካርድ',

    'Billing_details' => 'የክፍያ መጠየቂያ ዝርዝሮች',

    'YourOrder' => 'ትእዛዝዎት',

    'PaymentDetails' => 'የክፍያ ዝርዝሮች',

    'product_name' => 'የምርት ስም',

    'CustomerSupport' => 'የደንበኛ ድጋፍ',

    'OrderHistory' => 'ትእዛዝ',

    'SubscribeNewsletter' => 'በራሪ ጽሑፍ ይመዝገቡ',

    'createAccount' => 'መለያ ይፍጠሩ',

    'Not_member' => 'አባል አይደሉም?',

    'click_here' => 'ለመመዝገብ እዚህ ጠቅ ያድርጉ'

];  